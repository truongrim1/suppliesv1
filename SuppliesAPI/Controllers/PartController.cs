﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Supplies.Business.Part;
using Supplies.Common;
using Supplies.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies.Controllers
{
    [ApiController]
    [Route("api/v1/parts")]
    [ApiExplorerSettings(GroupName = "Part")]
    public class PartController : ControllerBase
    {
        private readonly IPartHandler _partHendler;
        public PartController(IPartHandler partHendler)
        {
            _partHendler = partHendler;

        }
        /// <summary>
        /// lấy toàn bộ phòng ban , tìm phòng ban theo filter
        /// </summary>
        /// <param name="size"></param>
        /// <param name="page"></param>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponsePagination<PartModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync(int size = 20, int page = 1, string filter = "{}", string sort = "+CreatedOnDate")
        {

            var filterObject = JsonConvert.DeserializeObject<PartQueryModel>(filter);

            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;

            var result = await _partHendler.GetPageAsync(filterObject);

            return Helper.TransformData(result);
        }
        /// <summary>
        /// tìm phòng ban theo id
        /// </summary>
        /// <param name="partId"></param>
        /// <returns></returns>
        [HttpGet,Route("{id}")]
        public async Task<IActionResult> GetPartId(Guid id)
        {

            var albumFromRepob = await _partHendler.GetPart(id);

            return Helper.TransformData(albumFromRepob);
        }
        /// <summary>
        /// thêm phòng ban
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreatePart( PartCreateModel model)
        {
            var addpart = await _partHendler.CreatePart(model);

            return Helper.TransformData(addpart);

        }
        /// <summary>
        /// xoá phòng ban
        /// </summary>
        /// <param name="partId"></param>
        /// <returns></returns>
        [HttpDelete,Route("{id}")]
        public async Task<IActionResult> DeletePart(Guid id)
        {
            var deletePart = await _partHendler.DeletePart(id);

            return Helper.TransformData(deletePart);

        }
        /// <summary>
        /// import
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost, Route("import")]
        public async Task<IActionResult> ImportExcel(IFormFile file)
        {
            var inport = await _partHendler.ImportExcel(file);

            return Helper.TransformData(inport);

        }

    }
}
