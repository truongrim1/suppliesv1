﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Supplies.Business.Staff;
using Supplies.Common;
using Supplies.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuppliesAPI.Controllers
{
    [ApiController]
    [Route("api/v1/staffs")]
    [ApiExplorerSettings(GroupName = "Staff")]
    public class StaffController
    {
        private readonly IStaffHandler _staffHendler;
        public StaffController(IStaffHandler staffHendler)
        {
            _staffHendler = staffHendler;

        }
        /// <summary>
        /// lấy nhân viên theo filter
        /// </summary>
        /// <param name="size"></param>
        /// <param name="page"></param>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponsePagination<StaffModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync(int size = 20, int page = 1, string filter = "{}", string sort = "+CreatedOnDate")
        {

            var filterObject = JsonConvert.DeserializeObject<StaffQueryModel>(filter);

            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;

            var result = await _staffHendler.GetListAsync(filterObject);

            return Helper.TransformData(result);
        }
        /// <summary>
        /// lấy nhân viên theo id
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        [HttpGet,Route("{id}")]
        public async Task<IActionResult> GetStaffId(Guid id)
        {

            var staffFromRepob = await _staffHendler.GetStaff(id);

            return Helper.TransformData(staffFromRepob);
        }
        /// <summary>
        /// thêm nhân viên
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateStaff( StaffCreateModel model)
        {
            var addstaff = await _staffHendler.CreateStaff(model);

            return Helper.TransformData(addstaff);

        }
        /// <summary>
        /// xoá nhân viên
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        [HttpDelete,Route("{id}")]
        public async Task<IActionResult> DeleteStaff(Guid id)
        {
            var deleteStaff = await _staffHendler.DeleteStaff(id);

                return Helper.TransformData(deleteStaff);

        }
        /// <summary>
        /// thay đổi thông tin nhân viên 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="staffId"></param>
        /// <returns></returns>
        [HttpPut,Route("{id}")]
        public async Task<IActionResult> UpdateStaff(StaffUpdateModel model, Guid id)
        {
            var updateStaff = await _staffHendler.UpdateStaff(model, id);

            return Helper.TransformData(updateStaff);
        }
    }
}
