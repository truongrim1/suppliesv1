﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Supplies.Business;
using Supplies.Business.Part;
using Supplies.Common;
using Supplies.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies.Controllers
{
    [ApiController]
    [Route("api/v1/products")]
    [ApiExplorerSettings(GroupName = "Product")]
    public class ProductController : ControllerBase
    {
        private readonly IProductHandler _productHendler;
        public ProductController(IProductHandler productHendler)
        {
            _productHendler = productHendler;

        }
        /// <summary>
        /// lấy toàn bộ sản phẩm, get theo filter
        /// </summary>
        /// <param name="size"></param>
        /// <param name="page"></param>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponsePagination<ProductModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync(int size = 20, int page = 1, string filter = "{}", string sort = "+CreatedOnDate")
        {

            var filterObject = JsonConvert.DeserializeObject<ProductQueryModel>(filter);

            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;

            var result = await _productHendler.GetlistProduct(filterObject);
            
            return Helper.TransformData(result);
        }   
        /// <summary>
        /// tìm sản phẩm theo id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet,Route("{id}")]
        public async Task<IActionResult> GetProductId(Guid id)
        {

            var productFromRepob = await _productHendler.GeytByIdProduct(id);

            return Helper.TransformData(productFromRepob);
        }
        /// <summary>
        /// thêm sản phẩm
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateProduct(ProductCreateModel model)
        {
            var addproduct = await _productHendler.CreateProduct(model);

            return Helper.TransformData(addproduct);

        }
        /// <summary>
        /// xoá sản phẩm
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpDelete, Route("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var deleteProduct = await _productHendler.DeleteProduct(id);

            return Helper.TransformData(deleteProduct);

        }

    }
}
