﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Supplies.Business.Statistical;
using Supplies.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuppliesAPI.Controllers
{
    [ApiController]
    [Route("api/v1/statisticals")]
    [ApiExplorerSettings(GroupName = "Statistical")]
    public class StatisticalController
    {
     
        private readonly IStatisticalHandler _statisticalHendler;
        public StatisticalController(IStatisticalHandler statisticalHendler)
        {
            _statisticalHendler = statisticalHendler;

           

        }
        /// <summary>
        /// lấy tổng số nhân viên trong 1 phòng ban
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("get-staff")]
        public async Task<IActionResult> GetStaffByPart()
        {
            var data = await _statisticalHendler.GetStaffByPart();

            return Helper.TransformData(data);

        }
        /// <summary>
        /// thống kê nhân viên theo quý
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("quarterly-employee-statistic")]
        public async Task<IActionResult> QuarterlyEmployeeStatistic()
        {
            var data = await _statisticalHendler.QuarterlyEmployeeStatistic();

            return Helper.TransformData(data);

        }
        /// <summary>
        /// sản phẩm ưa order nhiều nhất
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("product-order-max")]
        public async Task<IActionResult> GetProductMax()
        {
            var data = await _statisticalHendler.GetProductMax();

            return Helper.TransformData(data);

        }
        /// <summary>
        /// lấy số lượng đăng ký của từng phòng ban
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("order-part")]
        public async Task<IActionResult> OrderPart()
        {
            var result = await _statisticalHendler.GetOrderOfPart();
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Phòng ban đăng kí nhiều sản phẩm nhất
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("order-max-part")]
        public async Task<IActionResult> OrderMaxPart()
        {
            var result = await _statisticalHendler.GetPartMaxOrderProduct();
            return Helper.TransformData(result);
        }
        /// <summary>
        /// tháng nào thuộc năm nào có số lượng sản phẩm 
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("product-max-monthly")]
        public async Task<IActionResult> GetProductMaxOfMonth()
        {
            var result = await _statisticalHendler.GetProductMaxOfMonth();
            return Helper.TransformData(result);
        }
        /// <summary>
        /// tháng nào thuộc năm nào có số lượng nhân viên đăng ký nhiều nhất
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet, Route("monthly-form")]
        public async Task<IActionResult> GetFormByMonth()
        {
            var result = await _statisticalHendler.GetNewStaffOfMonthYear();
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Thống kê số lượng đăng kí theo quý
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("quarterly-order-statistic")]
        public async Task<IActionResult> QuarterlOrderStatistic()
        {
           
            var data = await _statisticalHendler.QuarterlyOrderStatistic();
            return Helper.TransformData(data);

        }
    }
}
