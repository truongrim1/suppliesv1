﻿using Microsoft.AspNetCore.Mvc;
using Supplies.Business.Users;
using Supplies.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuppliesAPI.Controllers
{
    [ApiController]
    [Route("api/v1/users")]
    [ApiExplorerSettings(GroupName ="User")]
    public class UserController
    {
        private readonly IUserHendler _userHendler;
        public UserController(IUserHendler userHendler)
        {
            _userHendler = userHendler;
        }
        /// <summary>
        /// đăng nhập
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("login")]
        public async Task<IActionResult> Login(UserModel model)
        {

            var login = await _userHendler.Login(model);

            return Helper.TransformData(login);
        }
        /// <summary>
        /// đăng ký
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, Route("register")]
        public async Task<IActionResult> Register(UserCreateModel model)
        {
            var register = await _userHendler.Register(model);
            return Helper.TransformData(register);
        }
        /// <summary>
        /// dailyaccess
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("daily-access")]
        public async Task<IActionResult> StatisticalUserDaily()
        {
            var result = await _userHendler.StatisticalAccessDaily();
            return Helper.TransformData(result);
        }
        /// <summary>
        /// monthlyaccess
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("monthly-access")]
        public async Task<IActionResult> StatisticalAccessMonthly()
        {
            var result = await _userHendler.StatisticalAccessMonthly();
            return Helper.TransformData(result);
        }
        /// <summary>
        /// yearlyaccess
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("yearly-access")]
        public async Task<IActionResult> StatisticalAccessYearly()
        {
            var result = await _userHendler.StatisticalAccessYearly();
            return Helper.TransformData(result);
        }
        /// <summary>
        /// create or update access time
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("yearly-accessa")]
        public async Task<IActionResult> CreateAccess()
        {
            var result = await _userHendler.CreateAccess();
            return Helper.TransformData(result);
        }
    }
}
