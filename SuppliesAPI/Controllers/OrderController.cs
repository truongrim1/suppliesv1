﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Supplies.Business;

using Supplies.Business.Staff;
using Supplies.Common;
using Supplies.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuppliesAPI.Controllers
{
    [ApiController]
    [Route("api/v1/orders")]
   // [Authorize]
    [ApiExplorerSettings(GroupName = "Order")]
    public class OrderController
    {
        private readonly IOrderHandler _orderHendler;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderHendler"></param>
        public OrderController(IOrderHandler orderHendler)
        {
            _orderHendler = orderHendler;

        }
        /// <summary>
        /// get toàn bộ order, tìm order theo filter
        /// </summary>
        /// <param name="size"></param>
        /// <param name="page"></param>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponsePagination<OrderModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int size = 20, [FromQuery] int page = 1, [FromQuery] string filter = "{}", [FromQuery] string sort = "+CreatedOnDate")
        {
            var filterObject = JsonConvert.DeserializeObject<OrderQueryModel>(filter);

            filterObject.Sort = sort != null ? sort : filterObject.Sort;
            filterObject.Size = size;
            filterObject.Page = page;

            var result = await _orderHendler.GetListAsync(filterObject);
            return Helper.TransformData(result);
        }
        /// <summary>
        /// tìm order theo id
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpGet,Route("{id}")]
        public async Task<IActionResult> GetOrderId(Guid id)
        {
            var orderromRepob = await _orderHendler.GetOrder(id);
            return Helper.TransformData(orderromRepob);
        }
        /// <summary>
        /// thêm mới order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateOrder(OrderCreateModel model)
        {
            var addorder = await _orderHendler.CreateOrder(model);
            return Helper.TransformData(addorder);

        }
        /// <summary>
        /// xoá order
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpDelete,Route("{id}")]
        public async Task<IActionResult> DeleteOrder(Guid id)
        {
            var deleteOrder = await _orderHendler.DeleteOrder(id);
            return Helper.TransformData(deleteOrder);
        }
        /// <summary>
        /// sửa order
        /// </summary>
        /// <param name="model"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpPut,Route("{id}")]
        public async Task<IActionResult> UpdateOrder(OrderUpdateModel model, Guid id)
        {
            var updateOrder = await _orderHendler.UpdateOrder( model, id);
            return Helper.TransformData(updateOrder);

        }


    }
}

