﻿using Castle.Core.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SuppliesAPI.SeedData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuppliesAPI.Controllers
{

    [ApiController]
    [ApiExplorerSettings(GroupName = "_Seed")]
    public class SeedController : ControllerBase
    {
        // public IConfiguration _config;
        
        public Seeding handler;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="handler"></param>
        public SeedController(Seeding handler)
        {
           // _config = config;
            this.handler = handler;
        }
        /// <summary>
        /// seed data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("all")]
        public async Task<string> SeedAll()
        {
            await handler.SeedDataPart();

            await handler.SeedDataStaff();

            await handler.SeedDataOrder();

            await handler.SeedDataProduct();

            return "OK";
        }
    }
}
