﻿using AutoMapper;
using Supplies.Business;
using Supplies.Business.Part;
using Supplies.Business.Staff;
using Supplies.Business.Statistical;
using Supplies.Business.Users;
using Supplies.Common;
using Supplies.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            #region Mapper Part
            CreateMap<Pagination<Part>, Pagination<PartModel>>().ReverseMap();

            CreateMap<Part, PartModel>().ReverseMap();

            CreateMap<PartCreateModel, Part>();

            CreateMap<Part, ViewPart>()
                .ForMember(dest => dest.CreateDateYearsAgo, opt => opt.MapFrom(src => $"{src.CreateDate.ToString("yyyy")} ({src.CreateDate.GetYearsAgo()}) YEAR AGO  "));

            CreateMap<Part, PartModel>()
                .ForMember(dest => dest.ViewStaff, opt => opt.MapFrom(src => src.Staffs))
                .ForMember(dest => dest.CreateDateYearsAgo,opt=>opt.MapFrom(src=> $"{src.CreateDate.ToString("yyyy")} ({src.CreateDate.GetYearsAgo()}) YEAR AGO  "));
            #endregion

            #region Mapper Staff
            CreateMap<Staff, StaffModel>().ForMember(dest => dest.CreateDateYearAgo, opt => opt.MapFrom(src => $"{src.CreateDate.ToString("yyyy")} ({src.CreateDate.GetYearsAgo()}) YEAR AGO  "));

            CreateMap<StaffUpdateModel, Staff>().ReverseMap();

            CreateMap<Pagination<Staff>, Pagination<StaffModel>>();

            CreateMap<StaffCreateModel, Staff>();

            CreateMap<Staff, GetViewPart>().ForMember(
                dest => dest.ViewPart,  
                opt => opt.MapFrom(src => src.Part))
                .ForMember(dest => dest.CreateDateYearAgo, opt => opt.MapFrom(src => $"{src.CreateDate.ToString("yyyy")} ({src.CreateDate.GetYearsAgo()}) YEAR AGO  "));
            #endregion

            #region Mapper Product
            CreateMap<Product, ProductModel>().ReverseMap();
            CreateMap<ProductCreateModel, Product>();
            CreateMap<Pagination<Product>,Pagination<ProductModel>>();

            #endregion

            #region Mapper Order
            CreateMap<Order, OrderModel>().ReverseMap();

            CreateMap<Pagination<Order>, Pagination<OrderModel>>();

            CreateMap<OrderCreateUpdateDetailModel, OrderDetail>();

            CreateMap<OrderDetail,OrderDetailModel>();

            CreateMap<OrderCreateModel, Order>().ForMember(dest=>dest.Products,opt=>opt.MapFrom(src=>src.ProductCreates));

            #endregion

            #region statistical
            CreateMap<Statistical, UserIpModel>().ReverseMap();
            #endregion
        }
    }
    
}
