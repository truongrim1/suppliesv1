﻿using Supplies.Business;
using Supplies.Business.Part;
using Supplies.Business.Staff;
using Supplies.Data;
using Supplies.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuppliesAPI.SeedData
{
    public class Seeding
    {
        private IPartHandler _partHandler;
        private IStaffHandler _staffHandler;
        private IOrderHandler _orderHandler;
        private IProductHandler _productHandler;
        

        private SuppliesContext context;

        public Seeding(IPartHandler partHandler, SuppliesContext context, IStaffHandler staffHandler,IOrderHandler orderHandler,IProductHandler productHandler)
        {
           _partHandler = partHandler;
            this.context = context;
           _staffHandler = staffHandler;    
           _orderHandler = orderHandler;
            _productHandler = productHandler;
        }
        public async Task SeedDataPart()
        {
            await _partHandler.CreatePart(new PartCreateModel
            {
                Name = "Phong dao tao",
                NameLeader = "cuahdajkgda",
                CreateDate = new DateTime(2000 - 1 - 1)
            });
        }
        public async Task SeedDataStaff()
        {
            await _staffHandler.CreateStaff(new StaffCreateModel
            {
                Name = "huy",
                Adress = "122 tran hung dao",
                Phone = "111111111",
                PartId= Guid.Parse("3a2b44b6-76b3-47dc-d722-08da8a295942"),
                CreateDate=new DateTime(2000-01-02)
                
            });
        }
        public async Task SeedDataOrder()
        {
            await _orderHandler.CreateOrder(new OrderCreateModel
            {
                StaffId=Guid.Parse("32e84975-0e4c-d7f3-da55-130a5dc16880"),
           
                ProductCreates = new List<OrderCreateUpdateDetailModel>
                {
                    new OrderCreateUpdateDetailModel { Amount=10,productId= Guid.Parse("930181e9-dfe8-47c8-3744-08da8a296a3f")}
                }
            });
        }
        public async Task SeedDataProduct()
        {
            await _productHandler.CreateProduct(new ProductCreateModel
            {
                Price=11,
                Name = "banh trang",
            }); ;
        }

    }
}
