﻿using Quartz;
using Serilog;
using Supplies.Business.Users;
using Supplies.Data;
using Supplies.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SuppliesAPI.BackgroundTasks
{
    public class StatisticalJob : IJob
    {
        private readonly SuppliesContext _context;
        private IUserHendler _userHendler;
        private Timer _time;
        
        public StatisticalJob(SuppliesContext context, IUserHendler hendler, Timer timer)
        {
            _context = context;
            _userHendler = hendler;
            _time = timer;
        }
        
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                DateTime today = DateTime.Now;
               
                if (today.Hour == 0)
                {
                    var date = CallUserStatisticalDate();
                }
                if (today.Day == 1)
                {
                    var month = CallUserStatisticalMonth();
                }
                if (today.Month == 1 && today.Day== 1)
                {
                    var year = CallUserStatisticalYear();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CurrencyJob: Exception - " + ex.Message);
            }

        }

        private async Task CallUserStatisticalDate( )
        {
            try
            {
                Statistical modelDate = new Statistical()
                {   
                    TotalDate = 0 
                };
                _context.Add(modelDate);

            }

            catch (Exception ex)
            {
                Log.Error("StatisticalJob Exception: " + ex.ToString());
            }
        }
        private async Task CallUserStatisticalMonth()
        {
            try
            {
                Statistical modelMonth = new Statistical()
                {
                    TotalMonth = 0
                };
                _context.Add(modelMonth);
            }
            catch (Exception ex)
            {
                Log.Error("StatisticalJob Exception: " + ex.ToString());
            }
        }
        private async Task CallUserStatisticalYear()
        {
            try
            {
                Statistical modelYear = new Statistical()
                {
                    TotalYear = 0
                };
                _context.Add(modelYear);
            }
            catch (Exception ex)
            {
                Log.Error("StatisticalJob Exception: " + ex.ToString());
            }
        }
    }
}

