﻿using FluentAssertions;
using Supplies.Business.Staff;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Supplie.IntegrationTest
{
   public class StaffControllerTest :IntegrationTest
    {
        [Fact]
        public async Task UpdateStaff()
        {
           
                var model = new StaffCreateModel() { Name = "12dassa3", Adress = "hihihihi", Phone= "21354654",PartId=Guid.Parse("c8357dc2-8c20-f678-18ae-6acf0804a81a") , CreateDate = new DateTime(22 / 12 / 2000) };
                var id = "501e5bf1-0a44-9d1d-481c-455371c5c8b1";
                var response = await TestClient.PutAsJsonAsync($"/api/v1/staffs/{id}", model);
                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var data = await response.Content.ReadAsAsync<Response>();
                await Task.CompletedTask;
            
        }
    }
}
