﻿using Azure;
using Microsoft.AspNetCore.Mvc.Testing;
using Supplies.Business.Part;
using SuppliesAPI;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Supplie.IntegrationTest
{
   public class IntegrationTest
    {
        protected readonly HttpClient TestClient;

        protected IntegrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>();
             
            
            TestClient = appFactory.CreateClient();
            TestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBY2Nlc3NUb2tlbiIsImp0aSI6IjRhMTYyZGExLWMwZDktNDEyYi1iNjgyLWEzOTc1ZDhiOWQ3NyIsImlhdCI6IjE2LzkvMjAyMiAyOjQ3OjE3IEFNIiwiVXNlck5hbWUiOiJUUlVPTkcgQ09ORyBWSSIsIkVtYWlsIjoidmlAZ21haWwuY29tIiwiSWQiOiJlY2IwZjNmYy0wYmVhLTQxNmEtYTI5NC1mNTAzZGI2Mjg4ZDgiLCJleHAiOjE2NjMzODI4MzcsImlzcyI6IkdFTkVBVCBDT1JQIiwiYXVkIjoiSW52ZXRvcnlBdXRoZW50aWNhdGlvbiJ9.ilCYD42N-gm_HS4QV42Nmae6r2ptKQKQ6LLEKyJxiA8");
        }
        /*protected async Task<Response> CreateDepartmentAsync(PartCreateModel request)
        {
            var response = await TestClient.PostAsJsonAsync("/api/v1/parts", request);
            return await response.Content.ReadAsAsync<Response>();
        }*/
        
        
    }
}
