﻿
using FluentAssertions;
using Supplies.Business;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Supplie.IntegrationTest
{
   public class ProductController : IntegrationTest
    {
        [Fact]
        public async Task TaskOne()
        {
            var model = new ProductTest() { Name = "Cần sa11234", Price = 120 };
            var response = await TestClient.PostAsJsonAsync<ProductTest>("/api/v1/products", model);
            var result = await response.Content.ReadAsAsync<Response<ProductTest>>();
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            response = await TestClient.GetAsync($"/api/v1/products/{result.Data.Id}");
            (await response.Content.ReadAsAsync<Response<ProductTest>>()).Should().NotBeNull();

            response = await TestClient.DeleteAsync($"/api/v1/products/{result.Data.Id}");
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            response = await TestClient.GetAsync($"/api/v1/products/{result.Data.Id}");
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
        [Fact]
        public async Task TaskTwo()
        {
            var model = new ProductTest() { Name = "Cần sa chuẩn12333", Price = 1002 };
            var response = await TestClient.PostAsJsonAsync<ProductTest>("api/v1/products", model);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = await response.Content.ReadAsAsync<Response<ProductTest>>();

            response = await TestClient.GetAsync("api/v1/products?size=100000");
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var data = await response.Content.ReadAsAsync<Response<Pagination<ProductTest>>>();
            data.Data.Content.FirstOrDefault(d => d.Id == result.Data.Id).Should().NotBeNull();
        }

        

    }
}
