﻿
using FluentAssertions;
using Supplies.Business.Part;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Supplie.IntegrationTest
{
    public class PartControllerTest :IntegrationTest
    {
        [Fact]
        public async Task GetAll()
        {
            var response = await TestClient.GetAsync("/api/v1/parts?size=20&page=1&filter={}");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            (await response.Content.ReadAsAsync<Pagination<PartModel>>()).Should().NotBeNull();
        }
       
    }
}
