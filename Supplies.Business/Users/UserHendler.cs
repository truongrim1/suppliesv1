﻿using AutoMapper;
using Castle.Core.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Code = System.Net.HttpStatusCode;


namespace Supplies.Business.Users
{
    public class UserHendler : IUserHendler
    {
        private readonly SuppliesContext _context;
        private IHttpContextAccessor _accessor;
        private IMapper _mapper;

        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuration;

        public UserHendler(SuppliesContext context, Microsoft.Extensions.Configuration.IConfiguration config, IHttpContextAccessor accessor, IMapper mapper)
        {
            _context = context;
            _accessor = accessor;
            _configuration = config;
            _mapper = mapper;

        }
        public async Task<Response> Login(UserModel model)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
            if (user == null)
            {
                return new ResponseError(Code.NotFound, "không tìm thấy Email");
            }

            if (!BCrypt.Net.BCrypt.Verify(model.Password, user.Password))
                return new ResponseError(Code.BadRequest, "Mật khẩu không đúng");

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),//sub người dùng
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),// jti chỉ định danh
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),//iat thời điểm JWT ĐƯỢC BAN HÀNH có thể sử dụng để xác định tuổi
                new Claim("UserName", user.UserName),
                new Claim("Email", user.Email),
                new Claim("Id", user.Id.ToString())
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:key"]));
            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);
            return new ResponseObject<string>(new JwtSecurityTokenHandler().WriteToken(token));
        }
        public async Task<Response> Register(UserCreateModel model)
        {
            try
            {
                if (_context.Users.Any(u => u.Email == model.Email))
                    return new ResponseError(Code.BadRequest, "Email đã được đăng ký");
                var entity = new User()
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    Password = BCrypt.Net.BCrypt.HashPassword(model.Password),
                    CreateDate = DateTime.Now
                };

                _context.Users.Add(entity);
                var status = await _context.SaveChangesAsync();
                if (status > 0)
                    return new Response(Code.OK, "Đăng ký thành công");
                return new ResponseError(Code.NotFound, "Không thành công");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        public async Task<Response> StatisticalAccessDaily()
        {
            try
            {
                var data = await _context.Statisticals.ToListAsync();


                var fromDailyAsess = data.Min(a => a.AccessTime.Day);
                var toDailyAsess = data.Max(a => a.AccessTime.Day);
                var fromMonth = data.Min(a => a.AccessTime.Month);
                var toMonth = data.Max(a => a.AccessTime.Month);
                var fromYear = data.Min(a => a.AccessTime.Year);
                var toYear = data.Max(a => a.AccessTime.Year);

                List<StatisticalUserDaily> daily = new List<StatisticalUserDaily>();
                for (int k = fromYear; k <= toYear; k++)
                {
                    for (int i = fromMonth; i <= toMonth; i++)
                    {
                        for (int j = fromDailyAsess; j <= toDailyAsess; j++)
                        {
                            var dailyAssess = data.Where(a => a.AccessTime.Year == k)
                                .Where(a => a.AccessTime.Month == i)
                                .Where(a => a.AccessTime.Day == j)
                                .OrderBy(a => a.AccessTime.Day)
                                .GroupBy(a => a.AccessTime.Day)
                                .Select(a => new StatisticalUserDaily
                                {
                                    Year = k,
                                    Month = i,
                                    DailyAccess = a.Key,
                                    ViewCount = a.Count()
                                });

                            foreach (var item in dailyAssess)
                            {
                                daily.Add(new StatisticalUserDaily
                                {
                                    Year = item.Year,
                                    Month = item.Month,
                                    DailyAccess = item.DailyAccess,
                                    ViewCount = item.ViewCount

                                });

                            }
                        }

                    }
                }


                return new ResponseList<StatisticalUserDaily>(daily);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        public async Task<Response> StatisticalAccessMonthly()
        {
            try
            {
                var data = await _context.Statisticals.ToListAsync();

                var fromMonth = data.Min(a => a.AccessTime.Month);
                var toMonth = data.Max(a => a.AccessTime.Month);
                var fromYear = data.Min(a => a.AccessTime.Year);
                var toYear = data.Max(a => a.AccessTime.Year);

                List<StatisticalUserMonthly> monthly = new List<StatisticalUserMonthly>();
                for (int k = fromYear; k <= toYear; k++)
                {
                    for (int i = fromMonth; i <= toMonth; i++)
                    {

                        var dailyAssess = data.Where(a => a.AccessTime.Year == k)
                            .Where(a => a.AccessTime.Month == i)
                            .OrderBy(a => a.AccessTime.Month)
                            .GroupBy(a => a.AccessTime.Month)
                            .Select(a => new StatisticalUserMonthly
                            {
                                Year = k,
                                MonthlyAccess = a.Key,
                                ViewCount = a.Count()
                            });

                        foreach (var item in dailyAssess)
                        {
                            monthly.Add(new StatisticalUserMonthly
                            {
                                Year = item.Year,

                                MonthlyAccess = item.MonthlyAccess,
                                ViewCount = item.ViewCount

                            });
                        }
                    }
                }

                return new ResponseList<StatisticalUserMonthly>(monthly);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        public async Task<Response> StatisticalAccessYearly()
        {
            try
            {
                var data = await _context.Statisticals.ToListAsync();


                var fromYear = data.Min(a => a.AccessTime.Year);
                var toYear = data.Max(a => a.AccessTime.Year);

                List<StatisticalUserYearly> yearly = new List<StatisticalUserYearly>();
                for (int k = fromYear; k <= toYear; k++)
                {
                    var dailyAssess = data.Where(a => a.AccessTime.Year == k)
                        .OrderBy(a => a.AccessTime.Year)
                        .GroupBy(a => a.AccessTime.Year)
                        .Select(a => new StatisticalUserYearly
                        {

                            YearlyAccess = a.Key,
                            ViewCount = a.Count()
                        });

                    foreach (var item in dailyAssess)
                    {
                        yearly.Add(new StatisticalUserYearly
                        {

                            YearlyAccess = item.YearlyAccess,
                            ViewCount = item.ViewCount

                        });
                    }
                }
                return new ResponseList<StatisticalUserYearly>(yearly);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// kiễm tra ip có trùng không, trùng update LastModifiedOnDate,không thì thêm mới
        /// </summary>
        /// <returns></returns>
        public async Task<Response> CreateAccess()
        {
            try
            {
                var ip = _accessor.HttpContext.Connection.RemoteIpAddress.ToString(); // nếu đây là get ip từ header
                var data = await _context.Statisticals.Where(a => a.IpAdress == ip).FirstOrDefaultAsync();
             
                DateTime today = DateTime.Now;

                if (data == null)
                {
                    Data.Entity.Statistical entity = new Data.Entity.Statistical();

                    entity.Id = Guid.NewGuid();
                    entity.AccessTime = DateTime.Now;
                    entity.IpAdress = ip;
                    entity.ViewCount++;
                    _context.Add(entity);
                }
                else
                {
                    if (today > data.AccessTime.AddSeconds(10))
                    {
                        
                        data.AccessTime = DateTime.Now;
                        data.ViewCount++;
                    }
                }
                _context.SaveChanges();
                var a = _mapper.Map<UserIpModel>(data);

                return new Response<UserIpModel>(a);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }
    }
}