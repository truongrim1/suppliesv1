﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Supplies.Business.Users
{
  public  class UserModel
    {

        public string Email { get; set; }
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
    public class UserCreateModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }

    public class StatisticalUserDaily
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int ViewCount { get; set; }

        public int DailyAccess { get; set; }

     
    }

    public class UserIpModel
    {
        public Guid Id { get; set; }
        public DateTime AccessTime { get; set; }
        public string IpAdress { get; set; }

        public int ViewCount { get; set; }
    }

    public class StatisticalUserMonthly
    {
        public int Year { get; set; }
        public int ViewCount { get; set; }

        public int MonthlyAccess { get; set; }


    }
    public class StatisticalUserYearly
    {
        public int ViewCount { get; set; }
        public int YearlyAccess { get; set; }

    }
}
