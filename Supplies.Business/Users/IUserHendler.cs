﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business.Users
{
   public interface IUserHendler
    {
        Task<Response> Login(UserModel model);
        Task<Response> Register(UserCreateModel model);
        Task<Response> StatisticalAccessDaily();
        Task<Response> StatisticalAccessMonthly();
        Task<Response> StatisticalAccessYearly();
        Task<Response> CreateAccess();

    }
}
