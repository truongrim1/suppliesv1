﻿using Abp.Linq.Expressions;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Code = System.Net.HttpStatusCode;

namespace Supplies.Business.Staff
{
    public class StaffHandler : IStaffHandler
    {
        private readonly SuppliesContext _context;
        private readonly IMapper _mapper;

        public StaffHandler(SuppliesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// them
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Response> CreateStaff(StaffCreateModel model)
        {
            try
            {
               
                var staff = await _context.Staffs.Where(s =>s.Name.ToLower() == model.Name.ToLower()&& s.Adress==model.Adress&& s.Phone==model.Phone ).FirstOrDefaultAsync();
                if (staff != null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "dữ liệu đã tồn tại");
                }

                var staffEntity = _mapper.Map<Data.Entity.Staff>(model);
              
                _context.Staffs.Add(staffEntity);
                 await _context.SaveChangesAsync();
                var result = _mapper.Map<StaffModel>(staffEntity);

                return new Response<StaffModel>(result);
                

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// xoa
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteStaff(Guid staffId)
        {
            try
            {
                var result = await _context.Staffs.Where(s => s.Id == staffId).FirstOrDefaultAsync();
                
                if (result != null)
                {
                    _context.Remove(result);

                    var status = await _context.SaveChangesAsync();
                    if (status > 0)
                    {
                        return new ResponseDelete(staffId, "Xoa thanh Cong");
                    }
                    return new ResponseError(Code.BadRequest, "Xoa that bai");
                }
                return new ResponseError(Code.BadRequest, "Không tìm thấy Id");
            }
            catch (Exception ex )
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// get page/ query theo buildQuery
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<Response> GetListAsync(StaffQueryModel query)
        {
            try
            {
                var predicate = BuildQuery(query);

                var queryResult = _context.Staffs.Include(s => s.Part).Where(predicate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<Data.Entity.Staff>, Pagination<StaffModel>>(data);

                if (result != null)
                {
                    return new ResponsePagination<StaffModel>(result);
                }
                return new ResponseError(HttpStatusCode.BadRequest, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// get theo Id
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public async Task<Response> GetStaff(Guid staffId)
        {
            try
            {
                var result = await _context.Staffs.Include(s => s.Part).Where(s=> s.Id == staffId).FirstOrDefaultAsync();
                if (result == null)
                {
                    return new ResponseError(Code.NotFound, "album không tồn tại");
                }
                var albumFromRepo = _mapper.Map<GetViewPart>(result);
                return new ResponseObject<GetViewPart>(albumFromRepo);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.NotFound, ex.Message);
            }
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="model"></param>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public async Task<Response> UpdateStaff(StaffUpdateModel model, Guid staffId)
        {
            try
            {
                var result = await _context.Staffs.Where(s => s.Id == staffId).FirstOrDefaultAsync();
                if (result == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy dữ liệu");
                }
                result.Id = staffId;
                result.PartId = model.PartId;
                result.Name = model.Name;
                result.Adress = model.Adress;
                result.Phone = model.Phone;
                await _context.SaveChangesAsync();
                var data = _mapper.Map<StaffModel>(result);

                return new ResponseObject<StaffModel>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        private Expression<Func<Data.Entity.Staff, bool>> BuildQuery(StaffQueryModel query)
        {
            var predicate = PredicateBuilder.New<Data.Entity.Staff>(true);
            if (query.Filter!="{}")
            {
                predicate.And(s => s.Name.ToLower() == query.Name.ToLower());
            }
            if (!string.IsNullOrWhiteSpace(query.NamePart))
            {
                predicate.And(s => s.NamePart.Contains(query.NamePart));
            }
            if (!string.IsNullOrWhiteSpace(query.Phone))
            {
                predicate.And(s => s.Phone.Contains(query.Phone));
            }
            if (!string.IsNullOrWhiteSpace(query.Adress))
            {
                predicate.And(s => s.Adress.Contains(query.Adress));
            }
            
            return predicate;
        }

    }
}
