﻿using Supplies.Business.Part;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Supplies.Business.Staff
{
    public class StaffModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

      

        public string Adress { get; set; }

        public string Phone { get; set; }

        public string CreateDateYearAgo { get; set; }

        public Guid partId { get; set; }
    }

    public class GetViewPart
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string NamePart { get; set; }

        public string Adress { get; set; }

        public string Phone { get; set; }

        public string CreateDateYearAgo { get; set; }

        public ViewPart ViewPart { get; set; }
    }
    public class StaffQueryModel : PaginationRequest
    {
        public string Name { get; set; }

        public string NamePart { get; set; }

        public string Adress { get; set; }

        public string Phone { get; set; }
    }
    public class StaffCreateModel
    {
        public string Name { get; set; }

        public string Adress { get; set; }
       
        public string Phone { get; set; }

        public DateTime CreateDate { get; set; }
      
        public Guid? PartId { get; set; }
    }
    public class StaffUpdateModel
    {
        public string Name { get; set; }

        public string NamePart { get; set; }

        public string Adress { get; set; }
     
        public string Phone { get; set; }
        
        public string CreateDate { get; set; }
      
        public Guid PartId { get; set; }
    }
}

