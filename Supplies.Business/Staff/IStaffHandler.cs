﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business.Staff
{
  public interface IStaffHandler
    {
        Task<Response> GetStaff(Guid staffId);

        Task<Response> GetListAsync(StaffQueryModel query);

        Task<Response> CreateStaff(StaffCreateModel model);

        Task<Response> DeleteStaff(Guid staffId);

        Task<Response> UpdateStaff(StaffUpdateModel model, Guid staffId);
    }
}
