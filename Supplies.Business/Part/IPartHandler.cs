﻿
using Microsoft.AspNetCore.Http;

using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business.Part
{
  public  interface IPartHandler
    {
        Task<Response> GetPart(Guid partId);

        Task<Response> GetPageAsync(PartQueryModel query);

        Task<Response> CreatePart(PartCreateModel model);

        Task<Response> DeletePart(Guid partId);

        Task<Response> ImportExcel(IFormFile file);

    }
}
