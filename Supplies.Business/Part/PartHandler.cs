﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Code = System.Net.HttpStatusCode;
using Supplies.Common;
using Supplies.Data;
using System.Linq.Expressions;
using Abp.Linq.Expressions;
using Microsoft.Extensions.FileProviders;
using System.IO;
using OfficeOpenXml;
using Microsoft.AspNetCore.Http;

namespace Supplies.Business.Part
{
    public class PartHandler : IPartHandler
    {
        private readonly SuppliesContext _context;
        private readonly IMapper _mapper;

        public PartHandler(SuppliesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// createPart 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Response> CreatePart(PartCreateModel model)
        {
            try
            {
                var part = await _context.Parts.Where(p => p.Name.ToLower() == model.Name.ToLower() && p.NameLeader.ToLower() == model.NameLeader.ToLower()).FirstOrDefaultAsync();
                if (part != null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, " bộ phận này đã tồn tại");
                }
                var partEntity = _mapper.Map<Data.Entity.Part>(model);
                _context.Parts.Add(partEntity);
                await _context.SaveChangesAsync();
                var result = _mapper.Map<PartModel>(partEntity);
                return new ResponseObject<PartModel>(result);


            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// DeletePart theo id
        /// </summary>
        /// <param name="partId"></param>
        /// <returns></returns>
        public async Task<Response> DeletePart(Guid id)
        {
            try
            {
                var result = await _context.Parts.Where(p => p.Id == id).FirstOrDefaultAsync();
                if (result != null)
                {
                    _context.Parts.Remove(result);

                    var status = await _context.SaveChangesAsync();
                    if (status > 0)
                    {
                        return new ResponseDelete(id, "Xoá thành công ");
                    }
                    return new ResponseError(HttpStatusCode.BadRequest, "Xoá thất bại");
                }
                return new ResponseError(HttpStatusCode.BadRequest, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        public async Task<Response> GetPageAsync(PartQueryModel query)
        {
            try
            {
                var pradicate = BuildQuery(query);
                var queryResult = _context.Parts.Include(p => p.Staffs).Where(pradicate);
                var data = await queryResult.GetPageAsync(query);
                var result = _mapper.Map<Pagination<Data.Entity.Part>, Pagination<PartModel>>(data);

                if (result != null)
                {
                    return new ResponsePagination<PartModel>(result);
                }
                return new ResponseError(HttpStatusCode.BadRequest, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// lay theo Id
        /// </summary>
        /// <param name="partId"></param>
        /// <returns></returns>
        public async Task<Response> GetPart(Guid partId)
        {
            try
            {
                var result = await _context.Parts.Include(P => P.Staffs).Where(p => p.Id == partId).FirstOrDefaultAsync();
                if (result != null)
                {
                    var resultToReturn = _mapper.Map<PartModel>(result);
                    return new ResponseObject<PartModel>(resultToReturn);
                }
                return new ResponseError(HttpStatusCode.BadRequest, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        public async Task<Response> ImportExcel(IFormFile file)
        {
            try
            {
               
                var department = new List<ImportModel>();
                using (var stream = new MemoryStream())
                {
                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                       
                        var noOfRow = worksheet.Dimension.Rows;
                        for (int rowInterator = 2; rowInterator < noOfRow; rowInterator++)
                        {
                            var model = new ImportModel()
                            {
                                Id = Guid.Parse(worksheet.Cells[rowInterator, 1].Value.ToString()),
                                Name = Convert.ToString(worksheet.Cells[rowInterator, 2].Value),
                                NameLeader = Convert.ToString(worksheet.Cells[rowInterator, 3].Value),
                                CreateDate = Convert.ToDateTime(worksheet.Cells[rowInterator, 4].Value)
                            };
                            department.Add(model);
                        }
                    }
                }
                return new ResponseList<ImportModel>(department);
            }
            catch (Exception ex )
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);

            }

        }

        private Expression<Func<Data.Entity.Part, bool>> BuildQuery(PartQueryModel query)
        {
            var predicate = PredicateBuilder.New<Data.Entity.Part>(true);
            if (!string.IsNullOrWhiteSpace(query.Name))
            {
                predicate.And(p => p.Name.Contains(query.Name));
            }
            if (!string.IsNullOrWhiteSpace(query.NameLeader))
            {
                predicate.And(p => p.NameLeader.Contains(query.NameLeader));
            }

            return predicate;
        }
    }
}
