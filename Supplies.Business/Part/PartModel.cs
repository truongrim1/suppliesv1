﻿
using System;
using System.Collections.Generic;
using System.Text;
using Supplies.Business.Staff;
using Supplies.Common;


namespace Supplies.Business.Part
{
    public class PartModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string NameLeader { get; set; }

        public string CreateDateYearsAgo { get; set; }

        public ICollection<StaffModel> ViewStaff { get; set; }
    }
    public class ImportModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string NameLeader { get; set; }

        public DateTime CreateDate { get; set; }
    }
    public class ViewPart
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string NameLeader { get; set; }

        public string CreateDateYearsAgo { get; set; }

    }
    public class PartCreateModel
    {
        public string Name { get; set; }

        public string NameLeader { get; set; }

        public DateTime CreateDate { get; set; }
    }
   

    public class PartQueryModel : PaginationRequest
    {
        public string Name { get; set; }

        public string NameLeader { get; set; }

    }

    public static class YearAgo
    {
        public static int GetYearsAgo(this DateTime dateTime)
        {
            var currentDate = DateTime.Now;
            int yearsAgo = currentDate.Year - dateTime.Year;

            return yearsAgo;
        }
    }


}
