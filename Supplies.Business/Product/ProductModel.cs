﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Supplies.Business
{
    public class ProductModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }
        public int ViewCount { get; set; }

    }
    public class ProductQueryModel : PaginationRequest
    {
        public string Name { get; set; }

        public decimal Price { get; set; }
    }
    public class ProductCreateModel
    {
        
        public string Name { get; set; }

        public decimal Price { get; set; }
    }
    public  class ProductTest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public decimal Price { get; set; }
    }
    public class ProducViewCount
    {
        public int ViewCount { get; set; }
    }


}
