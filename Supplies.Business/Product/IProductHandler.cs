﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public interface IProductHandler
    {
        Task<Response> GetlistProduct(ProductQueryModel query);
        Task<Response> GeytByIdProduct(Guid productId);
        Task<Response> CreateProduct(ProductCreateModel model);
        Task<Response> DeleteProduct(Guid productId);
       


    }
}
