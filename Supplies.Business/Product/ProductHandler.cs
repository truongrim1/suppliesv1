﻿using AutoMapper;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Supplies.Data.Entity;
using System.Threading.Tasks;
using Abp.Linq.Expressions;
using Abp.Extensions;
using Supplies.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using Serilog;

namespace Supplies.Business
{
    public class ProductHandler : IProductHandler
    {
        private readonly SuppliesContext _context;
        private readonly IMapper _mapper;

        public ProductHandler(SuppliesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Response> CreateProduct(ProductCreateModel model)
        {
            try
            {
                {
                    var order = await _context.Products.Where(p => p.Name.ToLower() == model.Name.ToLower() && p.Price == model.Price).FirstOrDefaultAsync();
                    if (order != null)
                    {
                        return new ResponseError(HttpStatusCode.BadRequest, "dữ liệu đã tồn tại");
                    }
                    var productEntity = _mapper.Map<Product>(model);

                    _context.Products.Add(productEntity);
                    await _context.SaveChangesAsync();
                    var result = _mapper.Map<ProductModel>(productEntity);
                    return new ResponseObject<ProductModel>(result);

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        public async Task<Response> DeleteProduct(Guid id)
        {
            try
            {
                var result = await _context.Products.Where(p => p.Id == id).FirstOrDefaultAsync();

                if (result != null)
                {
                    _context.Remove(result);

                    var status = await _context.SaveChangesAsync();
                    if (status > 0)
                    {
                        return new ResponseDelete(id, "Xoa thanh Cong");
                    }
                    return new ResponseError(HttpStatusCode.BadRequest, "Xoa that bai");
                }
                return new ResponseError(HttpStatusCode.BadRequest, "Không  tìm thấy Id");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// getList/  tìm theo BuildQuery
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<Response> GetlistProduct(ProductQueryModel query)
        {
            try
            {
                var predicate = BuildQuery(query);

                var queryResult = _context.Products.Where(predicate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<Product>, Pagination<ProductModel>>(data);

                if (result != null)
                {
                    return new ResponsePagination<ProductModel>(result);
                }
                return new ResponseError(HttpStatusCode.BadRequest, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// tìm theo Id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<Response> GeytByIdProduct(Guid id)
        {
            try
            {
                var result = await _context.Products.Where(p => p.Id == id).FirstOrDefaultAsync();
                if (result == null)
                {
                    return new ResponseError(HttpStatusCode.NotFound, "Id không tồn tại");
                }

                 result.ViewCount ++ ;

                await _context.SaveChangesAsync();

                var product = _mapper.Map<ProductModel>(result);

                return new ResponseObject<ProductModel>(product);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        
       

        private Expression<Func<Product, bool>> BuildQuery(ProductQueryModel query)
        {
            var predicate = PredicateBuilder.New<Product>(true);
            if (!string.IsNullOrWhiteSpace(query.Name))
            {
                //var fullTextSearch = query.Name.Trim();
                predicate.And(s => s.Name.Contains(query.Name));
            }
            if (query.Price != 0)
            {
                var fullTextSearch = query.Price;
                predicate.And(s => s.Price == fullTextSearch);
            }
            return predicate;
        }
    }

}
