﻿using Abp.Linq.Expressions;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Supplies.Common.Utils;
using Code = System.Net.HttpStatusCode;
using System.Management;

namespace Supplies.Business.Statistical
{
    public class StatisticalHandler : IStatisticalHandler
    {
        private readonly SuppliesContext _context;
        private readonly IMapper _mapper;


        public StatisticalHandler(SuppliesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Thống kê số lượng nhân viên theo phòng ban
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetStaffByPart()
        {
            try
            {
                var data = await _context.Parts.Select(s => new GetStaffByPart
                {
                    Id = s.Id,
                    Name = s.Name,
                    NameLeader = s.NameLeader,
                    CreateDate = s.CreateDate,
                    TotalStaff = s.Staffs.Count()
                }).ToListAsync();
                return new ResponseList<GetStaffByPart>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// Thống kê số lượng đăng kí theo quý
        /// </summary>
        /// <returns></returns>
        public async Task<Response> QuarterlyOrderStatistic()
        {
            try
            {
                var data = await _context.Orders.ToListAsync();
                int fromYear = data.Min(a => a.CreateDate.Year);
                int toYear = data.Max(a => a.CreateDate.Year);

                List<QuarterlyOrderStatistic> list = new List<QuarterlyOrderStatistic>();

                for (int i = fromYear; i <= toYear; i++)
                {

                    var dataQuy = data.Where(c => c.CreateDate.Year == i)
                    .Select(g => new
                    {
                        Year = g.CreateDate.Year,
                        Quy = (g.CreateDate.Month - 1) / 3 + 1
                    });
                    var listTam = dataQuy.OrderBy(p => p.Quy).GroupBy(p => p.Quy).Select(p => new QuarterlyOrderStatistic
                    {
                        Year = i,
                        Quy = p.Key,
                        TotalOrder = p.Count()
                    });
                    bool[] a = { false, false, false, false };
                    foreach (var loc in listTam)
                    {
                        a[loc.Quy - 1] = true;
                    }
                    for (int j = 0; j < a.Length; j++)
                    {
                        if (a[j] == false)
                        {
                            list.Insert(j, new QuarterlyOrderStatistic()
                            {

                                Year = i,
                                Quy = j + 1,
                                TotalOrder = 0
                            });
                        }
                    }
                    foreach (var item in listTam)
                    {
                        list.Add(new QuarterlyOrderStatistic()
                        {
                            Year = item.Year,
                            Quy = item.Quy,
                            TotalOrder = item.TotalOrder
                        });
                    }

                }
                return new ResponseList<QuarterlyOrderStatistic>(list);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Thống kê số lượng nhân viên mới theo quý
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<Response> QuarterlyEmployeeStatistic()
        {
            try
            {
                var data = await _context.Staffs.ToListAsync();
                int fromYear = data.Min(a => a.CreateDate.Year);
                int toYear = data.Max(a => a.CreateDate.Year);


                List<QuarterlyEmployeeStatistic> list = new List<QuarterlyEmployeeStatistic>();
                for (int i = fromYear; i <= toYear; i++)
                {
                    var dataQuy = data.Where(c => c.CreateDate.Year == i)
                     .Select(g => new
                     {
                         Year = g.CreateDate.Year,
                         Quy = (g.CreateDate.Month - 1) / 3 + 1
                     });
                    var listTam = dataQuy.OrderBy(p => p.Quy).GroupBy(p => p.Quy).Select(p => new QuarterlyEmployeeStatistic
                    {
                        Year = i,
                        Quy = p.Key,
                        TotalStaff = p.Count()
                    }).ToList();
                    bool[] a = { false, false, false, false };
                    foreach (var loc in listTam)
                    {
                        a[loc.Quy - 1] = true;

                    }
                    for (int j = 0; j < a.Length; j++)
                    {
                        if (a[j] == false)
                        {
                            listTam.Insert(j, new QuarterlyEmployeeStatistic()
                            {

                                Year = i,
                                Quy = j + 1,
                                TotalStaff = 0

                            });
                        }
                    }
                    foreach (var item in listTam)
                    {
                        list.Add(new QuarterlyEmployeeStatistic()
                        {
                            Year = item.Year,
                            Quy = item.Quy,
                            TotalStaff = item.TotalStaff
                        });
                    }
                }

                return new ResponseList<QuarterlyEmployeeStatistic>(list);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }

        }

        /// <summary>
        /// sản phẩm được order nhiều nhất
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetProductMax()
        {
            try
            {
                var data = await _context.OrderDetails.GroupBy(a => new { a.ProductId, a.Product.Name })
                                                 .Select(g => new SummaryProductViewModel
                                                 {
                                                     Id = g.Key.ProductId,
                                                     Name = g.Key.Name,
                                                     TotalAmount = g.Sum(g => g.Amount)
                                                 }).ToListAsync();
                var max = data.Max(x => x.TotalAmount);
                var result = data.FindAll(g => g.TotalAmount == max);
                return new ResponseList<SummaryProductViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// thống kê  số lượng dăng ký theo từng phòng ban
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetOrderOfPart()
        {
            try
            {
                var data = await _context.Orders.GroupBy(a => new { a.Staff.Part.Id, a.StaffId })
                                                        .OrderByDescending(g => g.Count())
                                                        .Select(g => new OrderInMonth()
                                                        {
                                                            PartId = g.Key.Id,
                                                            TotalOrder = g.Count()
                                                        }).ToListAsync();
                var result = _mapper.Map<List<OrderInMonth>>(data);
                return new ResponseList<OrderInMonth>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        ///  Phòng ban đăng kí nhiều sản phẩm nhất
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetPartMaxOrderProduct()
        {
            try
            {
                var data = await _context.OrderDetails.Include(a => a.Order)
                                                        .ThenInclude(b => b.Staff)
                                                        .GroupBy(a => new { a.Order.Staff.PartId, a.Order.Staff.NamePart, a.Amount })
                                                        .Select(g => new PartOrderProductMax()
                                                        {
                                                            PartId = g.Key.PartId,
                                                            NamePart = g.Key.NamePart,
                                                            TotalProductOrder = g.Sum(g => g.Amount)
                                                        }).ToListAsync();
                var max = data.Max(x => x.TotalProductOrder);
                List<PartOrderProductMax> result = data.FindAll(g => g.TotalProductOrder == max);

                return new ResponseList<PartOrderProductMax>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// Tháng nào thuộc năm nào có số lượng sản phẩm được đăng kí nhiều nhất
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetProductMaxOfMonth()
        {
            try
            {
                var data = await _context.OrderDetails
                .GroupBy(a => new { a.Order.CreateDate.Year, a.Order.CreateDate.Month })
                .Select(g => new ProductMaxOfMonth
                {
                    Year = g.Key.Year,
                    Month = g.Key.Month,
                    TotalProduct = g.Count()
                }).ToListAsync();

                var max = data.Max(g => g.TotalProduct);
                List<ProductMaxOfMonth> result = data.FindAll(g => g.TotalProduct == max);
                return new ResponseList<ProductMaxOfMonth>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// tháng nào thuộc năm nào có số lượng nhân viên đăng ký nhiều nhất
        /// </summary>
        /// <returns></returns>
        public async Task<Response> GetNewStaffOfMonthYear()
        {
            try
            {
                var data = await _context.Staffs
                .GroupBy(a => new { a.CreateDate.Year, a.CreateDate.Month })
                .Select(g => new SummaryStaffViewModel
                {
                    Year = g.Key.Year,
                    Month = g.Key.Month,
                    TotalStaff = g.Count()
                }).ToListAsync();
                var max = data.Max(g => g.TotalStaff);
                List<SummaryStaffViewModel> result = data.FindAll(g => g.TotalStaff == max);
                return new ResponseList<SummaryStaffViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

    }
}



