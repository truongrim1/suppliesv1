﻿        using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business.Statistical
{
    public interface IStatisticalHandler
    {
        Task<Response> GetStaffByPart();
        Task<Response> QuarterlyEmployeeStatistic();
        Task<Response> GetProductMax();
        Task<Response> GetOrderOfPart();
        Task<Response> GetPartMaxOrderProduct();
        Task<Response> GetProductMaxOfMonth();
        Task<Response> GetNewStaffOfMonthYear( );
        Task<Response> QuarterlyOrderStatistic();

       /* Task<Response> GetIpUser();*/

    }
}
