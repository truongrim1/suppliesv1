﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Supplies.Business.Statistical
{
    public class StatisticalModel
    {
        public int Year { get; set; }
        public int Month { get; set; }
    }
    public class GetStaffByPart
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string NameLeader { get; set; }
        public DateTime? CreateDate { get; set; }
        public int TotalStaff { get; set; }

    }
    public class QuarterlyEmployeeStatistic
    {
        public int Year { get; set; }
        public int Quy { get; set; }
        public int TotalStaff { get; set; }
        //  public int Month { get; set; }
    }
    public class SummaryStaffViewModel : StatisticalModel
    {
        public int TotalStaff { get; set; }
    }
    public class SummaryProductViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int TotalAmount { get; set; }
    }
    public class OrderInMonth
    {
        public Guid PartId { get; set; }
        public int TotalOrder { get; set; }

    }
    public class PartOrderProductMax
    {
        public Guid PartId { get; set; }
        public string NamePart { get; set; }
        public int TotalProductOrder { get; set; }
    }
    public class ProductMaxOfMonth : StatisticalModel
    {
        public int TotalProduct { get; set; }
    }
    public class StatiscalQueryModel : PaginationRequest
    {
        public DateTime FromMonthOfYear { get; set; } = new DateTime(1970, 1, 1);
        public DateTime ToMonthOfYear { get; set; } = DateTime.Today;
    }
    public class QuarterlyOrderStatistic
    {
        public int Year { get; set; }
        public int Quy { get; set; }
        public int TotalOrder { get; set; }
        //  public int Month { get; set; }
    }
    public class ProductStatiscal :StatisticalModel
    {
        public int Date { get; set; }
    }
}
