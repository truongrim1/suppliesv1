using Abp.Linq.Expressions;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using Supplies.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Code = System.Net.HttpStatusCode;

namespace Supplies.Business
{

    public class OrderHandler : IOrderHandler
    {
        private readonly SuppliesContext _context;
        private readonly IMapper _mapper;

        public OrderHandler(SuppliesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// them
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Response> CreateOrder(OrderCreateModel model)
        {
            try
            {
                var orderEntity = _mapper.Map<Order>(model);

                _context.Orders.Add(orderEntity);
               
                 await _context.SaveChangesAsync();
                var result = _mapper.Map<OrderModel>(orderEntity);
                return new ResponseObject<OrderModel>(result);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }   
        /// <summary>
        /// xoa
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public async Task<Response> DeleteOrder(Guid orderId)
        {
            try
            {
                var result = await _context.Orders.Include(o => o.Products).
                    Where(o => o.Id == orderId).FirstOrDefaultAsync();

                if (result != null)
                {
                    _context.Remove(result);

                    var status = await _context.SaveChangesAsync();
                    if (status > 0)
                    {
                        return new ResponseDelete(orderId, "Xoa thanh Cong");
                    }
                    return new ResponseError(Code.BadRequest, "Xoa that bai");
                }
                return new ResponseError(Code.BadRequest, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// get page/ query theo buildQuery
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<Response> GetListAsync(OrderQueryModel query)
        {
            try
            {
                var predicate = BuildQuery(query);

                var queryResult = _context.Orders.Include(o => o.Products).Include(s => s.Staff).Where(predicate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<Order>, Pagination<OrderModel>>(data);

                if (result != null)
                {
                    return new ResponsePagination<OrderModel>(result);
                }
                return new ResponseError(HttpStatusCode.BadRequest, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// get theo Id
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public async Task<Response> GetOrder(Guid orderId)
        {
            try
            {
                var result = await _context.Orders.Include(s => s.Products).Include(s => s.Staff).FirstOrDefaultAsync(o => o.Id == orderId);

                if (result == null)
                {
                    return new ResponseError(Code.BadRequest, "orderId không tồn tại");
                }
                var OrderFromRepo = _mapper.Map<OrderModel>(result);

                return new ResponseObject<OrderModel>(OrderFromRepo);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        public async Task<Response> UpdateOrder(OrderUpdateModel model, Guid orderId)
        {
            try
            {
                var result = await _context.Orders.Include(p => p.Products).FirstOrDefaultAsync(o => o.Id == orderId);
                if (result == null)
                {
                    return new ResponseError(Code.BadRequest, "Không tìm thấy dữ liệu");
                }
                result.Id = orderId;
                result.StaffId = model.StaffId;
                result.Products = _mapper.Map<ICollection<OrderDetail>>(model.Products);
                result.NamePart = model.NamePart;

                  await _context.SaveChangesAsync();
                
                var data = _mapper.Map<OrderModel>(result);

                return new ResponseObject<OrderModel>(data);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        private Expression<Func<Order, bool>> BuildQuery(OrderQueryModel query)
        {
            var predicate = PredicateBuilder.New<Order>(true);
            if (!string.IsNullOrWhiteSpace(query.NamePart))
            {
                //var fullTextSearch = query.NamePart.Trim();
                predicate.And(s => s.NamePart.Contains(query.NamePart) );
            }
            if (!string.IsNullOrWhiteSpace(query.NameStaff))
            {
               // var fullTextSearch = query.NameStaff.Trim();
                predicate.And(s => s.NameStaff.Contains(query.NameStaff));
            }
            return predicate;
        }

    }

}
