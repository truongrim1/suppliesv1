﻿using Newtonsoft.Json;
using Supplies.Business.Staff;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Supplies.Business
{
    public class OrderModel
    {

        public Guid Id { get; set; }

        public Guid StaffId { get; set; }

        public List<OrderDetailModel> Products { get; set; }
    }
    public class OrderDetailModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public int Amount { get; set; }
    }

    public class OrderCreateModel
    {

        public Guid? StaffId { get; set; }

        public List<OrderCreateUpdateDetailModel> ProductCreates { get; set; }
    }
    public class OrderCreateUpdateDetailModel
    {
        public int Amount { get; set; }
        public Guid productId { get; set; }
    }
    public class OrderUpdateModel
    {
        public Guid Id { get; set; }

        public string NamePart { get; set; }

        public Guid StaffId { get; set; }

        public List<OrderCreateUpdateDetailModel> Products { get; set; }

    }
    public class OrderQueryModel : PaginationRequest
    {
        public string NamePart { get; set; }

        public string NameStaff { get; set; }
    }
    
}
