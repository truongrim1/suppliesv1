using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public interface IOrderHandler
    {
        Task<Response> GetOrder(Guid orderId);

        Task<Response> GetListAsync(OrderQueryModel query);

        Task<Response> CreateOrder(OrderCreateModel model);

        Task<Response> DeleteOrder(Guid orderId);

        Task<Response> UpdateOrder(OrderUpdateModel model, Guid orderId);

    }
}
