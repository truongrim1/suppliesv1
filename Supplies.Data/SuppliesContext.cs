﻿
using Bogus;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Supplies.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Supplies.Data
{
    public class SuppliesContext : DbContext
    {
        public SuppliesContext(DbContextOptions<SuppliesContext> options) : base(options)
        {
        }
        public DbSet<Part> Parts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<User> Users { get; set; }

        public DbSet<Statistical> Statisticals { get; set; }
       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*//fakedb part
            Faker<Part> partFaker = new Faker<Part>()
                .RuleFor(p => p.Id, f => f.Random.Guid())
                .RuleFor(p => p.Name, f => f.Internet.UserName())
                .RuleFor(p => p.NameLeader, f => f.Internet.UserName())
                .RuleFor(p => p.CreateDate, f => f.Date.Between(DateTime.Today.AddMonths(-1), DateTime.Today));

            var parts = partFaker.Generate(50);

            modelBuilder.Entity<Part>().HasData(parts);

            //fakedb staff
            Faker<Staff> staffFaker = new Faker<Staff>()
               .RuleFor(p => p.Id, f => f.Random.Guid())
               .RuleFor(p => p.Name, f => f.Name.FullName())
               .RuleFor(p => p.NamePart, f => f.Name.FullName())
               .RuleFor(p => p.Adress, f => f.Address.StreetAddress())
               .RuleFor(p => p.Phone, f => f.Phone.PhoneNumber())
               .RuleFor(p => p.CreateDate, f => f.Date.Between(DateTime.Today.AddMonths(-2), DateTime.Today))
               .RuleFor(p => p.PartId, f => f.PickRandom(parts).Id);

            var staffs = staffFaker.Generate(50);
            modelBuilder.Entity<Staff>().HasData(staffs);

            //fakedb order
            Faker<Order> orderFaker = new Faker<Order>()
                .RuleFor(p => p.Id, f => f.Random.Guid())
                .RuleFor(p => p.NamePart, f => f.PickRandom(parts).Name)
                .RuleFor(p => p.NameStaff, f => f.PickRandom(staffs).Name)
                .RuleFor(p => p.StaffId, f => f.PickRandom(staffs).Id);

            var orders = orderFaker.Generate(50);
            modelBuilder.Entity<Order>().HasData(orders);

            //fakedb Product
            Faker<Product> productFaker = new Faker<Product>()
               .RuleFor(p => p.Id, f => f.Random.Guid())
               .RuleFor(p => p.Name, f => f.Name.FullName())
               .RuleFor(p => p.Price, f => f.Random.Decimal(5, 12));

            var products = productFaker.Generate(50);
            modelBuilder.Entity<Product>().HasData(products);

            //fakedb orderDetail

            Random rd = new Random();
            int rand = rd.Next(1, 100);
            Faker<OrderDetail> orderDetailFaker = new Faker<OrderDetail>()
               .RuleFor(p => p.Id, f => f.Random.Guid())
               .RuleFor(p => p.OrderId, f => f.PickRandom(orders).Id)
                .RuleFor(p => p.Amount, f => f.PickRandom(rand))
               .RuleFor(p => p.ProductId, f => f.PickRandom(products).Id);


            var orderDetail = orderDetailFaker.Generate(50);
            modelBuilder.Entity<OrderDetail>().HasData(orderDetail);

            //fakedb user
            Faker<User> userFaker = new Faker<User>()
               .RuleFor(p => p.Id, f => f.Random.Guid())
               .RuleFor(p => p.Email, f => f.Internet.Email())
               .RuleFor(p => p.Password, f => f.Internet.Password())
               .RuleFor(p => p.UserName, f => f.Internet.UserName());

            var users = userFaker.Generate(20);
            modelBuilder.Entity<User>().HasData(users);*/

            base.OnModelCreating(modelBuilder);
        }
        
    }
}
