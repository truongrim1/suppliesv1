﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Supplies.Data.Migrations
{
    public partial class updatestatistical : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ListAccess",
                table: "Statisticals");

            migrationBuilder.DropColumn(
                name: "Time",
                table: "Statisticals");

            migrationBuilder.DropColumn(
                name: "TimeSaved",
                table: "Products");

            migrationBuilder.AddColumn<DateTime>(
                name: "AccessTime",
                table: "Statisticals",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "IpAdress",
                table: "Statisticals",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MacId",
                table: "Statisticals",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ViewCount",
                table: "Statisticals",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccessTime",
                table: "Statisticals");

            migrationBuilder.DropColumn(
                name: "IpAdress",
                table: "Statisticals");

            migrationBuilder.DropColumn(
                name: "MacId",
                table: "Statisticals");

            migrationBuilder.DropColumn(
                name: "ViewCount",
                table: "Statisticals");

            migrationBuilder.AddColumn<int>(
                name: "ListAccess",
                table: "Statisticals",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "Time",
                table: "Statisticals",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeSaved",
                table: "Products",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
