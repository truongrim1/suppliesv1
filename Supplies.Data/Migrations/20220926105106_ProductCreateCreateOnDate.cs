﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Supplies.Data.Migrations
{
    public partial class ProductCreateCreateOnDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreateOnDate",
                table: "Products",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateOnDate",
                table: "Products");
        }
    }
}
