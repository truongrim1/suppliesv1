﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Supplies.Data.Migrations
{
    public partial class updatestatistical1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MacId",
                table: "Statisticals");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MacId",
                table: "Statisticals",
                type: "text",
                nullable: true);
        }
    }
}
