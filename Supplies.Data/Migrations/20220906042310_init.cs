﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Supplies.Data.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Parts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    NameLeader = table.Column<string>(maxLength: 50, nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Staffs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    NamePart = table.Column<string>(maxLength: 50, nullable: true),
                    Adress = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    PartId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staffs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Staffs_Parts_PartId",
                        column: x => x.PartId,
                        principalTable: "Parts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    NamePart = table.Column<string>(maxLength: 50, nullable: true),
                    NameStaff = table.Column<string>(maxLength: 50, nullable: true),
                    StaffId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Staffs_StaffId",
                        column: x => x.StaffId,
                        principalTable: "Staffs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    OrderId = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Parts",
                columns: new[] { "Id", "CreateDate", "Name", "NameLeader" },
                values: new object[,]
                {
                    { new Guid("8d1a5116-082e-786d-c51a-004842a4f743"), new DateTime(2022, 8, 22, 23, 37, 14, 90, DateTimeKind.Local).AddTicks(9728), "Zena6", "Elyssa55" },
                    { new Guid("45d697c4-dd92-c266-249d-7b2522d95405"), new DateTime(2022, 8, 12, 9, 30, 47, 44, DateTimeKind.Local).AddTicks(9965), "Shyanne_Legros", "Madison_Haley" },
                    { new Guid("802c4c29-235d-1382-a9dd-e6c2dc754817"), new DateTime(2022, 8, 7, 14, 31, 10, 667, DateTimeKind.Local).AddTicks(9841), "Dwight.Haag1", "Mose.Gerlach" },
                    { new Guid("60a2f32b-01ee-c7eb-6890-3a42c679f812"), new DateTime(2022, 8, 6, 6, 21, 20, 745, DateTimeKind.Local).AddTicks(7896), "Eugenia14", "Imelda.Hamill37" },
                    { new Guid("e5593121-ceb7-1f00-f78e-7148efe4d715"), new DateTime(2022, 8, 18, 5, 3, 33, 810, DateTimeKind.Local).AddTicks(7310), "Mohamed.Reinger", "Frankie_Reinger" },
                    { new Guid("f53df199-020a-5341-a3e0-b96d83d96b41"), new DateTime(2022, 8, 13, 5, 46, 14, 747, DateTimeKind.Local).AddTicks(5155), "Fannie.Corkery90", "Jose.Gerhold59" },
                    { new Guid("2787ed82-4670-1d19-ddc3-29ec8b2ff54b"), new DateTime(2022, 8, 23, 22, 0, 12, 402, DateTimeKind.Local).AddTicks(3300), "Leann.Kub", "Jayme.Adams" },
                    { new Guid("8d9a7156-d169-f7c2-c9bf-661cb8482769"), new DateTime(2022, 8, 15, 4, 48, 25, 984, DateTimeKind.Local).AddTicks(1117), "Jana.Maggio", "Clemmie77" },
                    { new Guid("8a8b4e83-105a-6b0e-b01a-d57f9073f167"), new DateTime(2022, 8, 6, 11, 15, 46, 62, DateTimeKind.Local).AddTicks(6538), "Mae_Bernier0", "Easter_Lynch" },
                    { new Guid("e29e5fe3-fe78-cb5e-e583-376c6eae57b0"), new DateTime(2022, 8, 22, 15, 3, 50, 633, DateTimeKind.Local).AddTicks(7773), "Therese_Kerluke", "Rogelio_Moore" },
                    { new Guid("92ac021c-7937-6a0a-372d-aecccea2d620"), new DateTime(2022, 8, 17, 3, 26, 27, 317, DateTimeKind.Local).AddTicks(8495), "Osvaldo.Lang66", "Beaulah.Wolf" },
                    { new Guid("2dafe3c0-a279-987e-2107-257e24ae1034"), new DateTime(2022, 8, 16, 15, 16, 20, 882, DateTimeKind.Local).AddTicks(3335), "Percival_Dickinson18", "Trisha5" },
                    { new Guid("c916e51d-7541-f313-d3dc-353f64db448b"), new DateTime(2022, 8, 16, 20, 7, 3, 842, DateTimeKind.Local).AddTicks(2865), "Alvis.Feil", "Hyman.Kulas87" },
                    { new Guid("abda5ba0-e1b8-ad90-02e6-79e8b928cbad"), new DateTime(2022, 8, 27, 22, 42, 19, 164, DateTimeKind.Local).AddTicks(7272), "Lauren.Murazik", "Tierra_Huel38" },
                    { new Guid("2224bab3-fbc1-d35b-120f-b791fdc09be5"), new DateTime(2022, 8, 28, 1, 21, 8, 307, DateTimeKind.Local).AddTicks(2515), "Maiya.Abernathy", "Assunta23" },
                    { new Guid("934ffda7-2981-8675-882e-d688e0257d8c"), new DateTime(2022, 9, 5, 1, 4, 40, 32, DateTimeKind.Local).AddTicks(7885), "Zita15", "Kacey_Daugherty" },
                    { new Guid("52b8db66-8bb3-d9b9-2c19-fd0c4091e542"), new DateTime(2022, 8, 25, 15, 28, 26, 33, DateTimeKind.Local).AddTicks(5806), "Oren.Walter", "Zachary.Ledner" },
                    { new Guid("54441296-7f5f-8b1d-c61a-4dbc33d4a21c"), new DateTime(2022, 8, 13, 19, 25, 28, 23, DateTimeKind.Local).AddTicks(5557), "Abbey_Schoen71", "Kelvin.Zieme75" },
                    { new Guid("8e6ffe4d-89ab-1d0d-7974-85cd99e0807a"), new DateTime(2022, 8, 28, 4, 38, 58, 616, DateTimeKind.Local).AddTicks(1972), "Porter.Kling", "Alberto_Jaskolski" },
                    { new Guid("f0c4af24-6d8e-d39f-cccf-8d568ca23226"), new DateTime(2022, 8, 19, 2, 45, 56, 46, DateTimeKind.Local).AddTicks(6664), "Garry.Larkin", "Macy_Flatley" },
                    { new Guid("b42e3f0d-5d13-c155-6986-da482ceb06e5"), new DateTime(2022, 8, 26, 2, 19, 17, 339, DateTimeKind.Local).AddTicks(3443), "Mortimer.Hagenes82", "Elyse23" },
                    { new Guid("3e84ffc7-0fb2-c5c4-812e-711fb9713894"), new DateTime(2022, 9, 3, 13, 42, 58, 127, DateTimeKind.Local).AddTicks(5264), "Zena_Willms", "Gaylord16" },
                    { new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"), new DateTime(2022, 8, 15, 19, 26, 42, 916, DateTimeKind.Local).AddTicks(9736), "Simone52", "Marlen.Cartwright49" },
                    { new Guid("190a9a8e-6ab3-3c92-b5a2-2b93ba3c25bb"), new DateTime(2022, 8, 11, 20, 36, 0, 243, DateTimeKind.Local).AddTicks(4101), "Otto67", "Elmira74" },
                    { new Guid("4698230e-e5c9-2b80-539b-7bd29e469c5f"), new DateTime(2022, 8, 27, 22, 34, 24, 9, DateTimeKind.Local).AddTicks(8309), "Hildegard17", "Faye.Abbott45" },
                    { new Guid("f8342ea6-d3c8-4288-5cfc-b139ae092c02"), new DateTime(2022, 8, 9, 5, 8, 12, 894, DateTimeKind.Local).AddTicks(3914), "Darius_Murazik91", "Darrion5" },
                    { new Guid("67c24ff5-7a90-8e66-2d76-c71dbb821837"), new DateTime(2022, 8, 16, 21, 29, 50, 77, DateTimeKind.Local).AddTicks(7134), "Easton98", "Ethel51" },
                    { new Guid("bbeade3d-2f8e-be2a-c4ee-94915f9d03ea"), new DateTime(2022, 8, 21, 8, 13, 59, 733, DateTimeKind.Local).AddTicks(7545), "Shanie_Casper", "Herbert.Kuhic58" },
                    { new Guid("18ad7730-abae-2ff3-291c-4d098d623787"), new DateTime(2022, 8, 6, 13, 4, 35, 870, DateTimeKind.Local).AddTicks(7310), "Rosario_Dicki98", "Jaydon.Tremblay" },
                    { new Guid("f29200bd-2d91-1ef1-fd90-c060176f0d18"), new DateTime(2022, 8, 20, 11, 36, 59, 99, DateTimeKind.Local).AddTicks(4025), "Dax80", "Bernice.Daugherty" },
                    { new Guid("2deeb484-ae82-07ac-87ee-f29d378b1f18"), new DateTime(2022, 8, 13, 11, 52, 58, 425, DateTimeKind.Local).AddTicks(6821), "Amos_Corwin61", "Merle30" },
                    { new Guid("cdd65641-25c3-1c01-0d79-02f17be21358"), new DateTime(2022, 8, 7, 17, 15, 23, 538, DateTimeKind.Local).AddTicks(3040), "Garett24", "Sid95" },
                    { new Guid("30ca3495-b39b-05c1-eb6d-5631b92acd9b"), new DateTime(2022, 8, 16, 1, 31, 51, 496, DateTimeKind.Local).AddTicks(5614), "Gardner_Langworth", "Pansy_Kiehn" },
                    { new Guid("ad117f58-44b5-b5ca-e32e-f037ec72c640"), new DateTime(2022, 8, 15, 12, 7, 5, 748, DateTimeKind.Local).AddTicks(1033), "Adele.Thiel", "Neoma95" },
                    { new Guid("f54e5991-8125-c329-cdee-c5e5ab778280"), new DateTime(2022, 8, 10, 18, 26, 26, 223, DateTimeKind.Local).AddTicks(9759), "Vicky96", "Esther.Schumm" },
                    { new Guid("715901aa-987a-0673-33da-522121bec10a"), new DateTime(2022, 8, 28, 13, 31, 46, 625, DateTimeKind.Local).AddTicks(6363), "Alaina_Kemmer", "Bryana_Upton78" },
                    { new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"), new DateTime(2022, 8, 15, 17, 7, 8, 530, DateTimeKind.Local).AddTicks(9706), "Emmitt.Bosco", "Carrie11" },
                    { new Guid("8cea1109-a4d9-15df-3571-97b9eddc511f"), new DateTime(2022, 8, 17, 15, 23, 24, 806, DateTimeKind.Local).AddTicks(854), "Jamel.Barton", "Leilani79" },
                    { new Guid("0ae57fc6-ab6c-1770-e443-34af5836288b"), new DateTime(2022, 8, 18, 6, 44, 29, 800, DateTimeKind.Local).AddTicks(9395), "Aurelie.Heidenreich", "Martina.Friesen" },
                    { new Guid("bfb1409a-1b30-1ba9-2b65-fb8b469511f6"), new DateTime(2022, 8, 13, 11, 33, 28, 682, DateTimeKind.Local).AddTicks(4790), "Hobart_Herzog57", "Mavis21" },
                    { new Guid("d20a4b2a-d920-80c4-e396-19ef9327f24c"), new DateTime(2022, 8, 7, 7, 16, 23, 339, DateTimeKind.Local).AddTicks(7466), "Velda14", "Sam_Herman" },
                    { new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), new DateTime(2022, 8, 24, 21, 23, 3, 157, DateTimeKind.Local).AddTicks(477), "Adolf.Tillman", "Tiara.Hartmann46" },
                    { new Guid("6178377b-00c3-b8ad-1483-23b0e32b5c48"), new DateTime(2022, 9, 1, 10, 35, 24, 457, DateTimeKind.Local).AddTicks(7630), "Dameon1", "Jammie.Rohan" },
                    { new Guid("ec39f26f-c3ba-926e-27ef-852e77817ae9"), new DateTime(2022, 8, 9, 9, 58, 11, 348, DateTimeKind.Local).AddTicks(2893), "Hudson67", "Theodore_Metz47" },
                    { new Guid("1388baaf-6ce5-dfc4-530e-74cb59bfb41d"), new DateTime(2022, 8, 24, 13, 15, 18, 622, DateTimeKind.Local).AddTicks(3044), "Paige13", "Kiera_Gerhold3" },
                    { new Guid("5a106410-72c4-2306-13b3-0a1409186a97"), new DateTime(2022, 8, 31, 3, 41, 11, 900, DateTimeKind.Local).AddTicks(7003), "Darwin50", "Genesis43" },
                    { new Guid("54cbeff1-bc4e-b8bc-085c-5fcef84901c5"), new DateTime(2022, 8, 6, 16, 12, 43, 750, DateTimeKind.Local).AddTicks(7696), "Assunta.Bruen", "Hillard_Franecki81" },
                    { new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), new DateTime(2022, 8, 13, 11, 32, 57, 485, DateTimeKind.Local).AddTicks(5852), "Alek_Okuneva51", "Camron28" },
                    { new Guid("fc49c059-de46-c66f-a1f4-ee1737a1dbef"), new DateTime(2022, 8, 24, 21, 46, 9, 889, DateTimeKind.Local).AddTicks(962), "Mariela_Fritsch74", "Josh_Maggio69" },
                    { new Guid("6ffde2a4-e3e3-c753-2e7d-5013521f25aa"), new DateTime(2022, 8, 28, 7, 34, 51, 323, DateTimeKind.Local).AddTicks(1560), "Samara.Lehner", "Orland85" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("25c1263a-d432-5147-b5b6-fd2ee76f8c5b"), "Candice Haag", 11.974104023992133m },
                    { new Guid("2161ea44-bda9-09c5-626f-ffd1c3377ed7"), "Bartholome Gislason", 6.854459464016586m },
                    { new Guid("cc981adb-bedb-0273-335d-6f9af93d14d3"), "Kip Ledner", 5.782784355703175m },
                    { new Guid("e516289b-2834-3265-b7ec-aef411012bdb"), "Elody Reichert", 5.5724533351941285m },
                    { new Guid("23ade046-012d-7f1c-4aa0-73562214b462"), "Brycen Waters", 9.11960815038514m },
                    { new Guid("6fa27318-76f8-bc4f-e774-5e0a0b72d8f1"), "Brendan Hintz", 10.170824643769687m },
                    { new Guid("9e946255-a266-9c1c-c43d-c35e86e51871"), "Lempi Hirthe", 10.301515736757555m },
                    { new Guid("d4e80b03-af95-918b-2308-fae38ce2c309"), "Genevieve Kreiger", 11.801525263954666m },
                    { new Guid("b5160fee-9310-8c33-ac62-7b663cd0e930"), "Johnnie Kohler", 7.255580472413254m },
                    { new Guid("13793282-54c6-a960-73fe-74712bac9cd7"), "Kira Considine", 11.943449931658548m },
                    { new Guid("6b614d2f-5210-4165-1861-37dc095ddf14"), "Megane Gibson", 8.042435658184083m },
                    { new Guid("5e01695b-cfe4-8bdc-8007-d57c539f9494"), "Cooper Smitham", 8.811768222512572m },
                    { new Guid("51aa6206-2a1b-8574-7109-5cbc9d0cb5d2"), "Robyn Wintheiser", 10.194327846725626m },
                    { new Guid("02d67cc5-e4a4-221a-d105-59123387a71d"), "Celestine Schimmel", 11.829264494976616m },
                    { new Guid("69261de2-46c8-a164-6032-b0047c1aa7c7"), "Sophia Ernser", 8.515269352363083m },
                    { new Guid("07016d05-80a3-f718-2935-f6eea5cdf14e"), "Therese Brakus", 11.131832661168571m },
                    { new Guid("4240bc0e-f0b9-0020-dcf7-8cbf7fedafff"), "Jaquan Konopelski", 6.802938533855109m },
                    { new Guid("4fafe883-6c3a-446a-0a37-8bcbc0281522"), "Ima Witting", 11.837328693288065m },
                    { new Guid("80821748-ff3b-759d-7600-f6cd1e6ed01b"), "Fleta Collins", 10.575152990210405m },
                    { new Guid("ac4fd606-c816-2c51-cb97-6acb0c3ba898"), "Aubree Rice", 5.02070508525739661m },
                    { new Guid("f79c4f74-43f4-6a14-6c5d-ef0ceac436be"), "Tad Steuber", 10.578348150280465m },
                    { new Guid("d3eab76c-ddd1-350b-ea92-75d5d11ac2e9"), "Aubree Swift", 10.779473792658874m },
                    { new Guid("6383759e-1eb1-42b0-8ff3-e65177c1b02c"), "Scarlett Collier", 6.948101698396777m },
                    { new Guid("48fe92b0-ef9b-5039-c05d-fd3e65d7af4b"), "Stanton Ferry", 5.800622163713267m },
                    { new Guid("2ab5cdca-1799-a324-71e3-9bcaab100e57"), "Ashlee Boyle", 10.763853995950827m },
                    { new Guid("c742eab8-7988-a282-91de-90534583c6ea"), "Marcelina Hauck", 7.16908034922978m },
                    { new Guid("3b718030-4722-6868-a6f4-11a170b5b4c2"), "Forrest Brown", 5.2659659577840783m },
                    { new Guid("bf50bdb0-921b-0544-4ea7-424f95f450fc"), "Adella Will", 7.356237646823858m },
                    { new Guid("94b44eeb-7b80-be4c-507f-ebcfc4c0a9e9"), "Salma Kub", 6.134790584507769m },
                    { new Guid("f8dfce2a-c6d2-e49f-b749-f6b182ae8a64"), "Carson Considine", 7.887421412341025m },
                    { new Guid("d3ce43d0-86f4-d863-7d8f-eb0afca5b631"), "Hermann Hermiston", 11.549059447622419m },
                    { new Guid("1f8a4088-c807-d1da-71e4-62af9eed2a86"), "Emmie Lueilwitz", 5.248110795509122m },
                    { new Guid("7bc8922f-cd77-ec58-7807-c3b9d048d473"), "Brent Hodkiewicz", 7.003957866692894m },
                    { new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598"), "Ramiro Ritchie", 10.678763792700488m },
                    { new Guid("dcaf5213-c961-2bae-9266-ca7a4140818b"), "Erick Miller", 7.313395625591925m },
                    { new Guid("ed5c88d6-cf0b-e7a6-b1da-6aaf30b22431"), "Rey Harber", 9.66494587700113m },
                    { new Guid("e1bb0da4-51fd-b627-99c7-1c0faf74f05d"), "Jacquelyn West", 7.324054987320703m },
                    { new Guid("2c8873d3-0915-8721-252c-6ac8e77a6c72"), "Madie Balistreri", 11.460922270296571m },
                    { new Guid("1823ff21-532b-5cf5-44f6-75ad79ea6111"), "Kurtis Barrows", 8.871999637629837m },
                    { new Guid("cadcd8a6-8a01-ff2f-3594-4201ce60e94e"), "Reggie White", 9.044022539651031m },
                    { new Guid("c3617645-dae9-cf74-e1df-5425d4567ce4"), "Millie Grant", 10.277579801752034m },
                    { new Guid("14eddb72-921e-0b40-3645-e2984f8dfb07"), "Darien Bechtelar", 8.344893156245767m },
                    { new Guid("845097ca-477a-c4a3-703f-98acf1d2314a"), "Lori Bednar", 9.189967535990278m },
                    { new Guid("22ec42e5-d991-d18e-d9b8-271713e2c2c2"), "Kian Sipes", 5.984066877506706m },
                    { new Guid("3d754e21-32d6-bcfa-3123-945fc99919a4"), "Garnet Bahringer", 6.525232125783912m },
                    { new Guid("604863e4-0320-0b09-817c-07d7f1b8df30"), "Shanel Klein", 5.4586785074596662m },
                    { new Guid("8330229c-8d98-fdc9-c7aa-8c09b9e49d12"), "Lizzie Crona", 11.040450718738352m },
                    { new Guid("dbba385f-3b3f-dd9e-86c5-ea073272417d"), "Heidi Pfannerstill", 11.163618724869389m },
                    { new Guid("39a0519e-a73f-06d1-9672-6b967a564156"), "Emely McClure", 6.639064277354189m },
                    { new Guid("caa1fe61-404b-6aab-3824-39d8b25191cd"), "Rosie Howe", 7.646243567413761m }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreateDate", "Email", "Password", "UserName" },
                values: new object[,]
                {
                    { new Guid("62bacc24-92af-2457-cfc1-8901035b8d54"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jon57@hotmail.com", "Piy5B63n_h", "Johnnie.Willms74" },
                    { new Guid("97d69928-dae5-c4ff-46b6-493847444096"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rosalee.Effertz85@hotmail.com", "pgH_2MsLS0", "Dixie.Hand20" },
                    { new Guid("419b6b26-8c6d-1ccc-fbe7-3cf93e594ade"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kip41@gmail.com", "su_aCT5NW8", "Haley71" },
                    { new Guid("570ba0d7-214c-9d3d-601a-7c9141c09ee5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alison_Cartwright@yahoo.com", "KqgNJGQyk_", "Jazmyn_Bergstrom80" },
                    { new Guid("9ee19294-5777-af49-8bd4-5338ce965509"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Piper_Kuhic28@yahoo.com", "Ig6INx6Jny", "Devin.Glover47" },
                    { new Guid("f5a1ac76-08c4-4b03-0aa3-08c4fbbafff5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alejandra_White81@gmail.com", "JTjivdEqiE", "Haley.Osinski18" },
                    { new Guid("f5b58a27-6b3b-1d35-5cf0-c5c70a1d0d84"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Vance7@hotmail.com", "fL1kvxnhBL", "Everett_Flatley48" },
                    { new Guid("4e38f433-caac-573e-4646-7df396ed495f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ulices_Rowe@yahoo.com", "sOJ0RnA4xU", "Quinton_Fay59" },
                    { new Guid("9063a2f2-b2e8-fb0c-eafb-1ea697155f37"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Herta_Johnston@hotmail.com", "i1fG9qgwX1", "Kianna91" },
                    { new Guid("edb64a32-5259-abac-d0e3-fcc82b2e920f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Justen55@yahoo.com", "H8RqEHEF9F", "Hilma61" },
                    { new Guid("8f1cab08-e6d7-7fba-260e-6ae892375b70"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Saul.Spinka@hotmail.com", "0mYq90Kc7t", "Vida.Hudson65" },
                    { new Guid("68fdcb8a-8c50-a950-38dc-2ec486f5ec36"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alize.Block@gmail.com", "bLkumYrR8l", "Rodrick_Lesch" },
                    { new Guid("b6fcb092-ac8c-f934-e8db-d25e9ab4d170"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Anabel7@hotmail.com", "_47SUX6dju", "Tatyana.Gorczany19" },
                    { new Guid("677c530a-3d2d-f2f7-47c8-e1a01d801fc6"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ola_Upton22@gmail.com", "dsmzUsSSCB", "Arno_Osinski36" },
                    { new Guid("db3323c8-0d80-f637-2bae-93f36458eaea"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Merle94@gmail.com", "sLGavRKGnq", "Orlando_Rowe66" },
                    { new Guid("efcca2ee-7acf-09f6-5dcc-8fe04200d997"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Michel.Jacobs35@yahoo.com", "jStwoKEbQn", "Cielo.Toy" },
                    { new Guid("de7a58f1-b785-ec87-b1ea-2d1bd1b12a22"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Regan.Howe@yahoo.com", "cIKks77FbV", "Helga71" },
                    { new Guid("689e82f2-b006-3f1f-ac57-d17dbfc75b5b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bill.Bauch5@gmail.com", "Nrcf6QwCh7", "Judson.Emmerich" },
                    { new Guid("477576f6-398d-2e8d-60ad-8724cb1e355c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Camille.Rau@hotmail.com", "ZeGQLdjzDy", "Carli60" },
                    { new Guid("eb0cfd23-c79b-a4dc-7517-bf0a0892d4e4"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ora_Rempel84@yahoo.com", "W_1eZtOY47", "Allison_Tillman70" }
                });

            migrationBuilder.InsertData(
                table: "Staffs",
                columns: new[] { "Id", "Adress", "CreateDate", "Name", "NamePart", "PartId", "Phone" },
                values: new object[,]
                {
                    { new Guid("eece8354-25ec-4cd2-c00d-f56eed3b0225"), "59156 Heathcote Roads", new DateTime(2022, 8, 20, 16, 57, 55, 763, DateTimeKind.Local).AddTicks(4194), "Frank Treutel", "Felipe Volkman", new Guid("2deeb484-ae82-07ac-87ee-f29d378b1f18"), "944-772-7795" },
                    { new Guid("737a5e3b-fc31-080b-d386-725d2e47f4c9"), "342 Harvey Knoll", new DateTime(2022, 7, 30, 13, 45, 37, 246, DateTimeKind.Local).AddTicks(4381), "Katheryn Kshlerin", "Mandy Lesch", new Guid("802c4c29-235d-1382-a9dd-e6c2dc754817"), "(917) 362-1543" },
                    { new Guid("9038bc44-07b5-7d03-f25e-24555f29aff4"), "962 Cortney Landing", new DateTime(2022, 7, 25, 0, 30, 6, 660, DateTimeKind.Local).AddTicks(2075), "Maia Botsford", "Alexandrea Kiehn", new Guid("60a2f32b-01ee-c7eb-6890-3a42c679f812"), "1-809-387-8877 x8145" },
                    { new Guid("dc92c1ac-22b7-9fd3-2696-61fd96d2801b"), "620 Lavonne Ramp", new DateTime(2022, 8, 15, 2, 5, 53, 967, DateTimeKind.Local).AddTicks(771), "Mathias Botsford", "Sabryna Pagac", new Guid("e5593121-ceb7-1f00-f78e-7148efe4d715"), "1-802-321-3435 x5900" },
                    { new Guid("596e114a-dcf4-28b6-d40e-60f7ecd2c3e8"), "530 Domenick Knoll", new DateTime(2022, 7, 13, 23, 25, 53, 958, DateTimeKind.Local).AddTicks(3912), "Guido Champlin", "Destinee Jacobson", new Guid("e5593121-ceb7-1f00-f78e-7148efe4d715"), "(604) 436-9000" },
                    { new Guid("6585182d-2c5a-11bb-a70d-22318d15b950"), "4172 Jane Flats", new DateTime(2022, 8, 20, 0, 57, 17, 491, DateTimeKind.Local).AddTicks(326), "Harmon Waelchi", "Remington Wolf", new Guid("f53df199-020a-5341-a3e0-b96d83d96b41"), "581-953-0660" },
                    { new Guid("9b50b98c-25b6-12bb-5cd4-fd740442f74f"), "428 Claudine Stream", new DateTime(2022, 7, 27, 2, 15, 54, 480, DateTimeKind.Local).AddTicks(2957), "Ulices Rowe", "Adell Collier", new Guid("2787ed82-4670-1d19-ddc3-29ec8b2ff54b"), "(852) 284-7078" },
                    { new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d"), "08312 Gianni Island", new DateTime(2022, 8, 10, 20, 29, 58, 748, DateTimeKind.Local).AddTicks(3681), "Arvid Rowe", "August Sauer", new Guid("e29e5fe3-fe78-cb5e-e583-376c6eae57b0"), "368.773.8560" },
                    { new Guid("6148b7c1-313d-0b71-99ee-aed730fb76ee"), "574 Weber Street", new DateTime(2022, 7, 26, 19, 18, 18, 671, DateTimeKind.Local).AddTicks(3077), "Janis Bergnaum", "Norris Jacobs", new Guid("e29e5fe3-fe78-cb5e-e583-376c6eae57b0"), "1-576-308-2617 x011" },
                    { new Guid("e7b7328a-7a58-2d20-e6e9-ae675adfc3f9"), "4034 Cassin Meadows", new DateTime(2022, 7, 31, 7, 33, 43, 902, DateTimeKind.Local).AddTicks(9221), "Yesenia D'Amore", "Marilou Buckridge", new Guid("92ac021c-7937-6a0a-372d-aecccea2d620"), "(465) 765-8008 x0200" },
                    { new Guid("03c19da2-646d-e11e-cdd1-5e329bb32962"), "8219 Ratke Heights", new DateTime(2022, 7, 21, 20, 48, 39, 769, DateTimeKind.Local).AddTicks(4457), "Monty Harvey", "Macie Bashirian", new Guid("f8342ea6-d3c8-4288-5cfc-b139ae092c02"), "470.424.4715" },
                    { new Guid("1a8b10a0-df5e-4b1e-fa34-ddb35be0eb8b"), "74955 Konopelski Vista", new DateTime(2022, 7, 29, 19, 22, 54, 410, DateTimeKind.Local).AddTicks(6692), "Bartholome Mitchell", "Mateo Kuhn", new Guid("c916e51d-7541-f313-d3dc-353f64db448b"), "297-930-2705 x89770" },
                    { new Guid("a0fdb218-16c6-056f-cc8c-cf32a6134249"), "2287 Madie Grove", new DateTime(2022, 8, 22, 4, 29, 54, 262, DateTimeKind.Local).AddTicks(8832), "Cesar Stamm", "Pearline Jacobson", new Guid("c916e51d-7541-f313-d3dc-353f64db448b"), "724.977.2551" },
                    { new Guid("66dcd09b-a68e-fa1a-e5f5-242fe8f40fbe"), "54247 Urban Stravenue", new DateTime(2022, 8, 5, 3, 19, 53, 909, DateTimeKind.Local).AddTicks(2007), "Sydnee Miller", "Milford Orn", new Guid("2224bab3-fbc1-d35b-120f-b791fdc09be5"), "(894) 344-5366" },
                    { new Guid("0e02ac36-74c6-9f0b-0d15-005d5d84dfbe"), "7409 Howell Manor", new DateTime(2022, 8, 21, 9, 59, 48, 544, DateTimeKind.Local).AddTicks(2216), "Emie Champlin", "Rosemarie Crist", new Guid("2224bab3-fbc1-d35b-120f-b791fdc09be5"), "(900) 529-3046" },
                    { new Guid("d918a0f1-d04a-4402-5035-c1b95e4e8a48"), "08912 Terry Squares", new DateTime(2022, 8, 30, 18, 23, 56, 579, DateTimeKind.Local).AddTicks(6055), "Leonor Gutmann", "Lilliana McLaughlin", new Guid("52b8db66-8bb3-d9b9-2c19-fd0c4091e542"), "1-334-549-6542 x6760" },
                    { new Guid("97a802c2-c94f-bed8-925d-31478f9b50e8"), "78824 Moen Lakes", new DateTime(2022, 9, 5, 10, 56, 22, 71, DateTimeKind.Local).AddTicks(7026), "Ebony Welch", "Alfreda O'Reilly", new Guid("54441296-7f5f-8b1d-c61a-4dbc33d4a21c"), "(416) 384-5186" },
                    { new Guid("00e54572-9329-cc20-8146-45548f7f4fcc"), "99191 Stefan Path", new DateTime(2022, 9, 5, 17, 34, 14, 568, DateTimeKind.Local).AddTicks(9028), "Kariane Bergstrom", "Johnny Shanahan", new Guid("54441296-7f5f-8b1d-c61a-4dbc33d4a21c"), "(741) 414-3429 x5427" },
                    { new Guid("8b3f7c2d-70e0-90b6-5263-889b49b96b64"), "98469 Janelle Lodge", new DateTime(2022, 8, 3, 5, 27, 59, 265, DateTimeKind.Local).AddTicks(8689), "Marco DuBuque", "Sienna McGlynn", new Guid("8e6ffe4d-89ab-1d0d-7974-85cd99e0807a"), "839.515.7620 x718" },
                    { new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf"), "0646 Micaela Courts", new DateTime(2022, 7, 7, 8, 20, 10, 59, DateTimeKind.Local).AddTicks(7685), "Kasandra Hartmann", "Rubye Ryan", new Guid("8e6ffe4d-89ab-1d0d-7974-85cd99e0807a"), "1-945-978-3247 x978" },
                    { new Guid("fdd2cb5d-621f-f1cf-4a18-78301b15a936"), "0993 Marvin Land", new DateTime(2022, 8, 24, 3, 45, 15, 952, DateTimeKind.Local).AddTicks(1628), "Jewell Bednar", "Telly Hackett", new Guid("b42e3f0d-5d13-c155-6986-da482ceb06e5"), "272-243-7732" },
                    { new Guid("08644456-3ebe-f008-7ce7-c6e9cb0c334a"), "6911 Sarah Stream", new DateTime(2022, 8, 24, 11, 9, 4, 567, DateTimeKind.Local).AddTicks(240), "Lizzie Yost", "Brandi Paucek", new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"), "(846) 947-2651" },
                    { new Guid("a32b4e75-4941-9679-163a-1a57f6dd702a"), "6839 Sylvia Burgs", new DateTime(2022, 7, 21, 15, 39, 37, 903, DateTimeKind.Local).AddTicks(8051), "Casey Beahan", "Alayna King", new Guid("4698230e-e5c9-2b80-539b-7bd29e469c5f"), "1-672-765-8797" },
                    { new Guid("2a738e5d-dca9-82bd-17e9-9ef7db1b7d98"), "2933 Gottlieb Turnpike", new DateTime(2022, 7, 6, 9, 4, 36, 439, DateTimeKind.Local).AddTicks(4733), "Ramon Emmerich", "Tracey Howe", new Guid("bbeade3d-2f8e-be2a-c4ee-94915f9d03ea"), "1-358-374-1837 x4299" },
                    { new Guid("292d1131-2381-b342-3a05-72ccf4e3d147"), "527 Alda Parkway", new DateTime(2022, 7, 20, 21, 45, 53, 96, DateTimeKind.Local).AddTicks(5902), "Emily Bins", "Alessia Rolfson", new Guid("bbeade3d-2f8e-be2a-c4ee-94915f9d03ea"), "1-765-751-1384" },
                    { new Guid("4c4d04a1-b098-8ce0-652c-d96d36dbbe44"), "8266 Oren Shoal", new DateTime(2022, 8, 5, 14, 49, 45, 979, DateTimeKind.Local).AddTicks(7786), "Germaine Langworth", "Elena Corkery", new Guid("67c24ff5-7a90-8e66-2d76-c71dbb821837"), "(877) 284-2723 x1855" },
                    { new Guid("dc304c87-09f7-8fab-b2f9-bd99b6da2aea"), "528 Gorczany Extensions", new DateTime(2022, 7, 23, 5, 40, 31, 0, DateTimeKind.Local).AddTicks(738), "Dayton Thiel", "Ahmad Torp", new Guid("cdd65641-25c3-1c01-0d79-02f17be21358"), "815.804.3337" },
                    { new Guid("ecc1fb02-1a2e-902c-309d-8f348ae34023"), "424 Kerluke Locks", new DateTime(2022, 9, 2, 2, 52, 1, 366, DateTimeKind.Local).AddTicks(4312), "Keith Walker", "Abbey Crona", new Guid("30ca3495-b39b-05c1-eb6d-5631b92acd9b"), "980.835.7259 x801" },
                    { new Guid("64735d54-7642-72c8-659b-7616a68a3338"), "62904 Gerson Turnpike", new DateTime(2022, 8, 30, 2, 25, 6, 219, DateTimeKind.Local).AddTicks(8098), "Ervin Waelchi", "Gordon Goyette", new Guid("30ca3495-b39b-05c1-eb6d-5631b92acd9b"), "1-623-440-4795 x60624" },
                    { new Guid("363bc0cb-083e-5630-71ba-dabbacd706dd"), "81802 Schimmel Lights", new DateTime(2022, 8, 18, 16, 55, 55, 258, DateTimeKind.Local).AddTicks(9030), "Virginia Kassulke", "Gardner Schulist", new Guid("ad117f58-44b5-b5ca-e32e-f037ec72c640"), "(777) 790-4182 x931" },
                    { new Guid("17a6f792-66c6-b278-b806-19485cacee54"), "52518 Mann Shores", new DateTime(2022, 7, 21, 12, 41, 27, 652, DateTimeKind.Local).AddTicks(6315), "Lindsey Bauch", "Marta Hills", new Guid("715901aa-987a-0673-33da-522121bec10a"), "(309) 861-1482" },
                    { new Guid("2ef232d7-920f-4a24-11a3-4e576177ccfa"), "62359 Wendell Rest", new DateTime(2022, 8, 19, 17, 36, 36, 224, DateTimeKind.Local).AddTicks(6624), "Mayra Nicolas", "Hattie Miller", new Guid("715901aa-987a-0673-33da-522121bec10a"), "482.946.0825" },
                    { new Guid("428ac028-9d86-4ced-8c8a-a85d4b9c1e3d"), "384 Stroman Garden", new DateTime(2022, 8, 9, 11, 20, 56, 751, DateTimeKind.Local).AddTicks(8156), "Lauriane Oberbrunner", "Jasper McCullough", new Guid("715901aa-987a-0673-33da-522121bec10a"), "1-225-713-6813 x587" },
                    { new Guid("3cbf7731-ffcf-0075-6676-bde35fddb8d1"), "11648 Boehm Mill", new DateTime(2022, 8, 4, 2, 31, 34, 954, DateTimeKind.Local).AddTicks(1328), "Sandy Kunde", "Christiana Hodkiewicz", new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"), "1-294-424-7611" },
                    { new Guid("d8035464-71c8-ac5e-234a-9b805a11a569"), "77065 Jerad Knoll", new DateTime(2022, 8, 19, 4, 52, 53, 768, DateTimeKind.Local).AddTicks(6179), "Xzavier Haag", "Aletha Reinger", new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"), "960.852.3408 x505" },
                    { new Guid("37323f33-86c8-3f82-842a-a4d548beabb8"), "860 Morissette Fort", new DateTime(2022, 8, 27, 21, 57, 43, 924, DateTimeKind.Local).AddTicks(7607), "Patricia Metz", "Tamara Medhurst", new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"), "630.223.1136 x64585" },
                    { new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b"), "4921 Casimir Flat", new DateTime(2022, 9, 2, 20, 16, 3, 424, DateTimeKind.Local).AddTicks(5139), "Dorthy Jacobs", "Keagan Stark", new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"), "970-270-8912 x91242" },
                    { new Guid("cd4ac6da-0caa-def4-be85-c2d738cf916f"), "461 Abbott Freeway", new DateTime(2022, 8, 3, 8, 56, 35, 759, DateTimeKind.Local).AddTicks(4813), "Kevon Sawayn", "Eddie Bernhard", new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), "(355) 482-1510 x342" },
                    { new Guid("a417997b-6a8d-1047-8b38-49481bd044cb"), "585 Fiona Causeway", new DateTime(2022, 7, 13, 6, 32, 24, 870, DateTimeKind.Local).AddTicks(2907), "April Grady", "Orin Gusikowski", new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), "367-556-3595" },
                    { new Guid("69f34ae5-db6d-2a23-2ac4-8bc2d0a17470"), "337 Jaskolski Bypass", new DateTime(2022, 7, 19, 3, 10, 40, 118, DateTimeKind.Local).AddTicks(5611), "Gretchen Bernhard", "Torrance Kling", new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), "(486) 606-8555 x3487" },
                    { new Guid("707740bf-6987-5dc0-87db-230cd35bed46"), "7131 Ryan Views", new DateTime(2022, 7, 21, 8, 39, 30, 773, DateTimeKind.Local).AddTicks(7308), "Destini Luettgen", "Carolyne Price", new Guid("6178377b-00c3-b8ad-1483-23b0e32b5c48"), "281.843.5762" },
                    { new Guid("23d0f4ff-61f4-cf86-f9c3-c9d01567ba63"), "6898 McDermott Walk", new DateTime(2022, 8, 5, 23, 44, 40, 562, DateTimeKind.Local).AddTicks(8221), "Nicolette Lesch", "Efrain Romaguera", new Guid("1388baaf-6ce5-dfc4-530e-74cb59bfb41d"), "(971) 801-7153 x142" },
                    { new Guid("f777c83e-2104-c344-3f79-db4a3b634b4b"), "4632 Tierra Dale", new DateTime(2022, 8, 7, 13, 58, 13, 656, DateTimeKind.Local).AddTicks(9806), "Edward Orn", "Jacklyn Lindgren", new Guid("1388baaf-6ce5-dfc4-530e-74cb59bfb41d"), "(371) 553-9230 x71280" },
                    { new Guid("5ed3fbd8-5d8a-5141-0d80-b6bd5ee3961b"), "586 Rusty Meadow", new DateTime(2022, 8, 25, 14, 7, 13, 239, DateTimeKind.Local).AddTicks(1678), "Leonard Hammes", "Graham Emmerich", new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), "891-756-6737" },
                    { new Guid("19298057-71d1-e643-d8d2-75a075a133ce"), "635 Christy Mission", new DateTime(2022, 7, 22, 2, 21, 5, 25, DateTimeKind.Local).AddTicks(1011), "Elnora Predovic", "Dianna Kunze", new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), "764.797.7742" },
                    { new Guid("70176971-a6cc-265a-0259-d18af3babc70"), "57259 Batz Greens", new DateTime(2022, 7, 15, 23, 32, 12, 110, DateTimeKind.Local).AddTicks(1121), "Eleazar Kuhlman", "Berneice Raynor", new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), "1-568-513-6957" },
                    { new Guid("c757d324-c682-9d14-f213-adffd86f17fc"), "4309 Dayton Court", new DateTime(2022, 8, 4, 8, 33, 58, 871, DateTimeKind.Local).AddTicks(449), "Frida Daniel", "Abigail Volkman", new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), "208.870.5789" },
                    { new Guid("2c850a60-85fc-7d04-b9f7-32159895f8e5"), "515 Burnice Lake", new DateTime(2022, 7, 31, 15, 8, 38, 963, DateTimeKind.Local).AddTicks(6559), "Jaron Morissette", "Lance Muller", new Guid("6ffde2a4-e3e3-c753-2e7d-5013521f25aa"), "236.529.2910 x659" },
                    { new Guid("13208737-53bc-80c8-483b-8e63d9d042c8"), "8565 Halvorson Lakes", new DateTime(2022, 9, 4, 22, 36, 10, 84, DateTimeKind.Local).AddTicks(9346), "Rollin Kozey", "Chasity Marks", new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), "905-580-8532 x353" },
                    { new Guid("3cdf8fd0-1f92-3e5b-f04d-c9f45530bcec"), "596 Lynch Court", new DateTime(2022, 9, 3, 14, 48, 35, 111, DateTimeKind.Local).AddTicks(1628), "Tavares Price", "Tanya Sporer", new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"), "701-575-2154" }
                });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "NamePart", "NameStaff", "StaffId" },
                values: new object[,]
                {
                    { new Guid("d961269f-e5d1-1541-26ed-f97767e65a6c"), "Dameon1", "Guido Champlin", new Guid("eece8354-25ec-4cd2-c00d-f56eed3b0225") },
                    { new Guid("a090ff59-c55b-00a2-aeb1-9e6f8e7437f1"), "Otto67", "Dayton Thiel", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("5f9fa83f-6cbd-c5b8-05e1-4ee1b7c3c701"), "Shyanne_Legros", "Janis Bergnaum", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"), "Maiya.Abernathy", "Janis Bergnaum", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("4edec551-e708-3c41-5a2e-54b4f90c978d"), "Easton98", "Sandy Kunde", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("6c54248f-902e-27b7-25a9-1628050db7bd"), "Darwin50", "Guido Champlin", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("425dd829-9b9e-201e-bb22-87b28388a9a6"), "Hildegard17", "Ulices Rowe", new Guid("6148b7c1-313d-0b71-99ee-aed730fb76ee") },
                    { new Guid("257238c6-a427-b0ed-12ca-d989926219e1"), "Hudson67", "Kevon Sawayn", new Guid("6148b7c1-313d-0b71-99ee-aed730fb76ee") },
                    { new Guid("babc03e8-adfe-ab67-cccd-3d43ae9f1a47"), "Hobart_Herzog57", "Maia Botsford", new Guid("03c19da2-646d-e11e-cdd1-5e329bb32962") },
                    { new Guid("8fae5f78-cf7d-4cc2-ceb0-31b87178284d"), "Darius_Murazik91", "Emily Bins", new Guid("03c19da2-646d-e11e-cdd1-5e329bb32962") },
                    { new Guid("4a3110a4-0c2f-34de-523e-e0b900790540"), "Zena6", "Lauriane Oberbrunner", new Guid("66dcd09b-a68e-fa1a-e5f5-242fe8f40fbe") },
                    { new Guid("24cba960-243f-8eed-f954-388350f7a42e"), "Hobart_Herzog57", "Jewell Bednar", new Guid("66dcd09b-a68e-fa1a-e5f5-242fe8f40fbe") },
                    { new Guid("78ac5e27-766b-cf6d-c472-46f74a23448f"), "Dameon1", "Germaine Langworth", new Guid("0e02ac36-74c6-9f0b-0d15-005d5d84dfbe") },
                    { new Guid("7b4c3eaf-b74f-164b-c658-902665aafbeb"), "Hildegard17", "Sydnee Miller", new Guid("d918a0f1-d04a-4402-5035-c1b95e4e8a48") },
                    { new Guid("f30dd851-b816-40fd-e9df-97223f7668da"), "Eugenia14", "Cesar Stamm", new Guid("d918a0f1-d04a-4402-5035-c1b95e4e8a48") },
                    { new Guid("7243e63d-9bfc-0972-fa64-5c9468bfc3d1"), "Maiya.Abernathy", "Arvid Rowe", new Guid("97a802c2-c94f-bed8-925d-31478f9b50e8") },
                    { new Guid("6bf84cec-7d37-7772-db15-54951c7b8b5f"), "Hobart_Herzog57", "Maia Botsford", new Guid("00e54572-9329-cc20-8146-45548f7f4fcc") },
                    { new Guid("058753b3-3837-9aea-f0f7-0511c2d29d4b"), "Dax80", "Jewell Bednar", new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf") },
                    { new Guid("1c644315-1e5b-46c1-cd53-c75e7d46f2bc"), "Percival_Dickinson18", "Xzavier Haag", new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf") },
                    { new Guid("b6d1a298-a50a-157c-7422-0dfaed645f2b"), "Adolf.Tillman", "Guido Champlin", new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf") },
                    { new Guid("4ec824fd-fc0f-b47c-44eb-e0622c8d4d06"), "Emmitt.Bosco", "Leonor Gutmann", new Guid("fdd2cb5d-621f-f1cf-4a18-78301b15a936") },
                    { new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), "Porter.Kling", "Emily Bins", new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b") },
                    { new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"), "Zena_Willms", "April Grady", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("6a5205f3-feb9-d175-962f-22a9ed849b66"), "Mariela_Fritsch74", "Lindsey Bauch", new Guid("9b50b98c-25b6-12bb-5cd4-fd740442f74f") },
                    { new Guid("e580e679-ec05-fecd-c5ea-0ca425b9776d"), "Abbey_Schoen71", "Gretchen Bernhard", new Guid("6585182d-2c5a-11bb-a70d-22318d15b950") },
                    { new Guid("109571f6-d42a-79e4-b7d3-aed716fb7ae2"), "Eugenia14", "Tavares Price", new Guid("596e114a-dcf4-28b6-d40e-60f7ecd2c3e8") },
                    { new Guid("b0130de5-0167-51f9-9652-76057f1adc7a"), "Amos_Corwin61", "Edward Orn", new Guid("eece8354-25ec-4cd2-c00d-f56eed3b0225") },
                    { new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"), "Alvis.Feil", "Virginia Kassulke", new Guid("ecc1fb02-1a2e-902c-309d-8f348ae34023") },
                    { new Guid("2c3e6a90-25dc-0f65-d57a-4000c4b96044"), "Shyanne_Legros", "Sydnee Miller", new Guid("ecc1fb02-1a2e-902c-309d-8f348ae34023") },
                    { new Guid("923ca8a6-1d68-fb7a-37ab-d9eac6a9193d"), "Dax80", "Lindsey Bauch", new Guid("64735d54-7642-72c8-659b-7616a68a3338") },
                    { new Guid("329a58e6-03ae-1f52-cf69-6f0bc15af361"), "Assunta.Bruen", "Ulices Rowe", new Guid("363bc0cb-083e-5630-71ba-dabbacd706dd") },
                    { new Guid("161d9192-c183-9e3c-bd02-4935c8b84557"), "Mariela_Fritsch74", "Elnora Predovic", new Guid("17a6f792-66c6-b278-b806-19485cacee54") },
                    { new Guid("822b7a9e-e1c5-1495-ca67-b2aaae095cbc"), "Fannie.Corkery90", "Jaron Morissette", new Guid("cd4ac6da-0caa-def4-be85-c2d738cf916f") },
                    { new Guid("2ff2b15a-2163-a874-8980-e3b467a12a14"), "Otto67", "Katheryn Kshlerin", new Guid("cd4ac6da-0caa-def4-be85-c2d738cf916f") },
                    { new Guid("a53c05f1-dee7-fe26-1ae6-e4ce7e13bb49"), "Velda14", "Jewell Bednar", new Guid("707740bf-6987-5dc0-87db-230cd35bed46") },
                    { new Guid("43856184-28f8-18f8-ce8f-30a4c88688e4"), "Vicky96", "Xzavier Haag", new Guid("f777c83e-2104-c344-3f79-db4a3b634b4b") },
                    { new Guid("39692b63-4874-aae8-4bd7-4c9ee31c61b3"), "Hobart_Herzog57", "Mayra Nicolas", new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b") },
                    { new Guid("4feda6f9-d3fc-5c87-7c3a-39b6ab33f4d8"), "Samara.Lehner", "Gretchen Bernhard", new Guid("5ed3fbd8-5d8a-5141-0d80-b6bd5ee3961b") },
                    { new Guid("a45dbb54-3345-1d1a-a0dc-6524630df8c6"), "Otto67", "Lizzie Yost", new Guid("19298057-71d1-e643-d8d2-75a075a133ce") },
                    { new Guid("7d680ada-8fdb-c6f1-d85f-9aa4bd8fc81b"), "Vicky96", "Katheryn Kshlerin", new Guid("19298057-71d1-e643-d8d2-75a075a133ce") },
                    { new Guid("0b66a772-f8ba-2a8f-7cff-611e863dff14"), "Alaina_Kemmer", "Monty Harvey", new Guid("c757d324-c682-9d14-f213-adffd86f17fc") },
                    { new Guid("df2d60b1-6e5a-e140-735c-92b88fc72a7c"), "Garett24", "Rollin Kozey", new Guid("4c4d04a1-b098-8ce0-652c-d96d36dbbe44") },
                    { new Guid("b0b20bef-ee68-9076-8e33-83226a277b8f"), "Hudson67", "Kasandra Hartmann", new Guid("2a738e5d-dca9-82bd-17e9-9ef7db1b7d98") },
                    { new Guid("5919fa5c-f41a-e47a-2bd2-355a426c0d49"), "Aurelie.Heidenreich", "Harmon Waelchi", new Guid("2a738e5d-dca9-82bd-17e9-9ef7db1b7d98") },
                    { new Guid("0f358ba3-8a3c-2bcb-a2ad-c3eae579db79"), "Amos_Corwin61", "Rollin Kozey", new Guid("737a5e3b-fc31-080b-d386-725d2e47f4c9") },
                    { new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"), "Velda14", "Lauriane Oberbrunner", new Guid("dc92c1ac-22b7-9fd3-2696-61fd96d2801b") },
                    { new Guid("ee544476-56a0-2e56-5df0-8c40fad4d07e"), "Jamel.Barton", "Germaine Langworth", new Guid("dc92c1ac-22b7-9fd3-2696-61fd96d2801b") },
                    { new Guid("8b847645-fafd-0e79-daf8-b34e6bcf2b8a"), "Fannie.Corkery90", "Janis Bergnaum", new Guid("596e114a-dcf4-28b6-d40e-60f7ecd2c3e8") },
                    { new Guid("c79beded-a660-c08a-7f24-8228409e7cdb"), "Amos_Corwin61", "Bartholome Mitchell", new Guid("19298057-71d1-e643-d8d2-75a075a133ce") },
                    { new Guid("efaa2963-c4a9-6831-acfb-445bd3c1c265"), "Adele.Thiel", "Xzavier Haag", new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b") }
                });

            migrationBuilder.InsertData(
                table: "OrderDetails",
                columns: new[] { "Id", "Amount", "OrderId", "ProductId" },
                values: new object[,]
                {
                    { new Guid("c5e39430-006c-b382-bd2d-0d179530843f"), 55, new Guid("d961269f-e5d1-1541-26ed-f97767e65a6c"), new Guid("d3ce43d0-86f4-d863-7d8f-eb0afca5b631") },
                    { new Guid("2451a1ea-2ca6-989a-1619-5dbfa600a5e6"), 55, new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"), new Guid("1823ff21-532b-5cf5-44f6-75ad79ea6111") },
                    { new Guid("9311a9c1-74fb-04b9-d2f8-54cb72b86c25"), 55, new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"), new Guid("e516289b-2834-3265-b7ec-aef411012bdb") },
                    { new Guid("0033b837-ed58-3444-aa5d-bbdb28c9fd16"), 55, new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"), new Guid("6383759e-1eb1-42b0-8ff3-e65177c1b02c") },
                    { new Guid("4c1476e0-6657-1ee2-028d-c3b5a6689056"), 55, new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"), new Guid("f8dfce2a-c6d2-e49f-b749-f6b182ae8a64") },
                    { new Guid("19d7e30b-dbf0-af2a-b42d-9dc3a77aa9dc"), 55, new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"), new Guid("4fafe883-6c3a-446a-0a37-8bcbc0281522") },
                    { new Guid("6d4d1ce0-20b5-b8ae-e4cf-47f4f93e0f05"), 55, new Guid("4edec551-e708-3c41-5a2e-54b4f90c978d"), new Guid("604863e4-0320-0b09-817c-07d7f1b8df30") },
                    { new Guid("15e621a7-86f2-c279-c4c3-736df6448436"), 55, new Guid("6c54248f-902e-27b7-25a9-1628050db7bd"), new Guid("02d67cc5-e4a4-221a-d105-59123387a71d") },
                    { new Guid("9f7f5db1-f5c6-acb4-3f3e-fc5f4d5112a8"), 55, new Guid("257238c6-a427-b0ed-12ca-d989926219e1"), new Guid("bf50bdb0-921b-0544-4ea7-424f95f450fc") },
                    { new Guid("2b5e417f-d76f-7058-7d73-8c63d0848b47"), 55, new Guid("8fae5f78-cf7d-4cc2-ceb0-31b87178284d"), new Guid("69261de2-46c8-a164-6032-b0047c1aa7c7") },
                    { new Guid("75ac8f04-4930-a8bd-ac67-f648d2ee6a79"), 55, new Guid("8fae5f78-cf7d-4cc2-ceb0-31b87178284d"), new Guid("ed5c88d6-cf0b-e7a6-b1da-6aaf30b22431") },
                    { new Guid("0d03c172-57c2-b2cc-c38b-d33e4cc112d6"), 55, new Guid("24cba960-243f-8eed-f954-388350f7a42e"), new Guid("2161ea44-bda9-09c5-626f-ffd1c3377ed7") },
                    { new Guid("afef892e-6ddf-f78b-af7d-256318f2d6f4"), 55, new Guid("24cba960-243f-8eed-f954-388350f7a42e"), new Guid("2161ea44-bda9-09c5-626f-ffd1c3377ed7") },
                    { new Guid("10c0ffe8-0a72-566b-4e8b-b4d62f38f7ae"), 55, new Guid("7b4c3eaf-b74f-164b-c658-902665aafbeb"), new Guid("14eddb72-921e-0b40-3645-e2984f8dfb07") },
                    { new Guid("f210fadd-f385-8e63-1874-352b279a41cc"), 55, new Guid("7243e63d-9bfc-0972-fa64-5c9468bfc3d1"), new Guid("2ab5cdca-1799-a324-71e3-9bcaab100e57") },
                    { new Guid("1c8fd4bd-0d36-9d5a-50eb-b21429d20ffe"), 55, new Guid("6bf84cec-7d37-7772-db15-54951c7b8b5f"), new Guid("6b614d2f-5210-4165-1861-37dc095ddf14") },
                    { new Guid("04379b6b-6a0c-2892-d4b1-207e9ab6c37d"), 55, new Guid("1c644315-1e5b-46c1-cd53-c75e7d46f2bc"), new Guid("cc981adb-bedb-0273-335d-6f9af93d14d3") },
                    { new Guid("4579be77-2e22-bfe4-c327-8bce2175ebb2"), 55, new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598") },
                    { new Guid("4e9f6d20-27be-efe9-c156-835a0e4e15ac"), 55, new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), new Guid("13793282-54c6-a960-73fe-74712bac9cd7") },
                    { new Guid("a623a55c-3dd5-4c1f-7f11-bb6ca667b836"), 55, new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), new Guid("1f8a4088-c807-d1da-71e4-62af9eed2a86") },
                    { new Guid("1af762e8-90a8-2c01-4a41-2835ee468117"), 55, new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), new Guid("cc981adb-bedb-0273-335d-6f9af93d14d3") },
                    { new Guid("0de819ca-d82a-87f6-1df1-28da1954769d"), 55, new Guid("39692b63-4874-aae8-4bd7-4c9ee31c61b3"), new Guid("3b718030-4722-6868-a6f4-11a170b5b4c2") },
                    { new Guid("3bd7d25a-be7f-80d0-3bec-1795cc75f20c"), 55, new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"), new Guid("6b614d2f-5210-4165-1861-37dc095ddf14") },
                    { new Guid("64db8e8f-26f4-2a11-36e1-4b86ae78416b"), 55, new Guid("109571f6-d42a-79e4-b7d3-aed716fb7ae2"), new Guid("7bc8922f-cd77-ec58-7807-c3b9d048d473") },
                    { new Guid("01bd0096-a253-d246-e7a9-4889f1bc529d"), 55, new Guid("8b847645-fafd-0e79-daf8-b34e6bcf2b8a"), new Guid("845097ca-477a-c4a3-703f-98acf1d2314a") },
                    { new Guid("7b154df2-5d18-c7bd-386f-6c2cefed9e03"), 55, new Guid("8b847645-fafd-0e79-daf8-b34e6bcf2b8a"), new Guid("39a0519e-a73f-06d1-9672-6b967a564156") },
                    { new Guid("5659a1a6-b334-2387-dd84-2e2eaea9eed0"), 55, new Guid("d961269f-e5d1-1541-26ed-f97767e65a6c"), new Guid("ed5c88d6-cf0b-e7a6-b1da-6aaf30b22431") },
                    { new Guid("c216df5c-50d4-6ae0-1679-79b5b02aa593"), 55, new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"), new Guid("604863e4-0320-0b09-817c-07d7f1b8df30") },
                    { new Guid("9a9ef423-04f9-02c4-75e8-6ab60d5b9411"), 55, new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"), new Guid("c742eab8-7988-a282-91de-90534583c6ea") },
                    { new Guid("75e6655f-0438-1983-781f-e478a55348a7"), 55, new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"), new Guid("d4e80b03-af95-918b-2308-fae38ce2c309") },
                    { new Guid("8701add4-505c-9919-7618-2e2fc0a7f5fb"), 55, new Guid("329a58e6-03ae-1f52-cf69-6f0bc15af361"), new Guid("604863e4-0320-0b09-817c-07d7f1b8df30") },
                    { new Guid("f55c3dcc-ad71-e092-4232-34fa5b32e24b"), 55, new Guid("161d9192-c183-9e3c-bd02-4935c8b84557"), new Guid("02d67cc5-e4a4-221a-d105-59123387a71d") },
                    { new Guid("800421ec-e40a-9dc9-5d2a-d8d5f0e19577"), 55, new Guid("2ff2b15a-2163-a874-8980-e3b467a12a14"), new Guid("bf50bdb0-921b-0544-4ea7-424f95f450fc") },
                    { new Guid("3b4d37d3-351d-e266-05e7-8667fbddcd36"), 55, new Guid("2ff2b15a-2163-a874-8980-e3b467a12a14"), new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598") },
                    { new Guid("2a5def9e-80bb-94c5-3a0d-90e3076e38f0"), 55, new Guid("43856184-28f8-18f8-ce8f-30a4c88688e4"), new Guid("69261de2-46c8-a164-6032-b0047c1aa7c7") },
                    { new Guid("8a58b8cf-3719-0edc-126b-e63fa1467158"), 55, new Guid("43856184-28f8-18f8-ce8f-30a4c88688e4"), new Guid("13793282-54c6-a960-73fe-74712bac9cd7") },
                    { new Guid("47ed86d7-99be-2d57-bc27-dd0192295684"), 55, new Guid("39692b63-4874-aae8-4bd7-4c9ee31c61b3"), new Guid("c742eab8-7988-a282-91de-90534583c6ea") },
                    { new Guid("e3f14653-6a68-3761-a001-72f6fb664fbb"), 55, new Guid("4feda6f9-d3fc-5c87-7c3a-39b6ab33f4d8"), new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598") },
                    { new Guid("c70de7c4-9bb6-f439-52f8-0a911c2a5884"), 55, new Guid("a45dbb54-3345-1d1a-a0dc-6524630df8c6"), new Guid("94b44eeb-7b80-be4c-507f-ebcfc4c0a9e9") },
                    { new Guid("82151266-f23f-9b0c-4656-5e7c8c53c114"), 55, new Guid("7d680ada-8fdb-c6f1-d85f-9aa4bd8fc81b"), new Guid("6b614d2f-5210-4165-1861-37dc095ddf14") },
                    { new Guid("f5a44759-d677-4998-987b-b8d39f75e7fc"), 55, new Guid("df2d60b1-6e5a-e140-735c-92b88fc72a7c"), new Guid("6fa27318-76f8-bc4f-e774-5e0a0b72d8f1") },
                    { new Guid("c5d1b821-d605-3534-ac50-c9dc0410bfc9"), 55, new Guid("df2d60b1-6e5a-e140-735c-92b88fc72a7c"), new Guid("5e01695b-cfe4-8bdc-8007-d57c539f9494") },
                    { new Guid("2fddf619-b341-7243-2cd7-ed95836cbc9d"), 55, new Guid("5919fa5c-f41a-e47a-2bd2-355a426c0d49"), new Guid("3b718030-4722-6868-a6f4-11a170b5b4c2") },
                    { new Guid("5a7343f1-db54-6580-2425-2e7dba02c8bb"), 55, new Guid("5919fa5c-f41a-e47a-2bd2-355a426c0d49"), new Guid("1823ff21-532b-5cf5-44f6-75ad79ea6111") },
                    { new Guid("9cd98795-6c5c-0169-98fe-3b68f6b91e92"), 55, new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"), new Guid("39a0519e-a73f-06d1-9672-6b967a564156") },
                    { new Guid("0e1fabb1-095a-77fc-0c34-70220809648a"), 55, new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"), new Guid("f79c4f74-43f4-6a14-6c5d-ef0ceac436be") },
                    { new Guid("d6e43927-1eb1-887b-55f7-bdfa945b53f8"), 55, new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"), new Guid("dbba385f-3b3f-dd9e-86c5-ea073272417d") },
                    { new Guid("7c73741a-aaa8-a414-2d19-e00d22fa3d86"), 55, new Guid("ee544476-56a0-2e56-5df0-8c40fad4d07e"), new Guid("48fe92b0-ef9b-5039-c05d-fd3e65d7af4b") },
                    { new Guid("52f50405-2e8b-1514-2236-3d861260a09f"), 55, new Guid("c79beded-a660-c08a-7f24-8228409e7cdb"), new Guid("07016d05-80a3-f718-2935-f6eea5cdf14e") },
                    { new Guid("12ce0b43-a71d-7039-fae1-8333c93aedc6"), 55, new Guid("efaa2963-c4a9-6831-acfb-445bd3c1c265"), new Guid("8330229c-8d98-fdc9-c7aa-8c09b9e49d12") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_OrderId",
                table: "OrderDetails",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_ProductId",
                table: "OrderDetails",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_StaffId",
                table: "Orders",
                column: "StaffId");

            migrationBuilder.CreateIndex(
                name: "IX_Staffs_PartId",
                table: "Staffs",
                column: "PartId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderDetails");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Staffs");

            migrationBuilder.DropTable(
                name: "Parts");
        }
    }
}
