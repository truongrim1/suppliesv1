﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Supplies.Data.Migrations
{
    public partial class addCreateDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("0033b837-ed58-3444-aa5d-bbdb28c9fd16"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("01bd0096-a253-d246-e7a9-4889f1bc529d"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("04379b6b-6a0c-2892-d4b1-207e9ab6c37d"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("0d03c172-57c2-b2cc-c38b-d33e4cc112d6"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("0de819ca-d82a-87f6-1df1-28da1954769d"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("0e1fabb1-095a-77fc-0c34-70220809648a"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("10c0ffe8-0a72-566b-4e8b-b4d62f38f7ae"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("12ce0b43-a71d-7039-fae1-8333c93aedc6"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("15e621a7-86f2-c279-c4c3-736df6448436"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("19d7e30b-dbf0-af2a-b42d-9dc3a77aa9dc"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("1af762e8-90a8-2c01-4a41-2835ee468117"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("1c8fd4bd-0d36-9d5a-50eb-b21429d20ffe"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("2451a1ea-2ca6-989a-1619-5dbfa600a5e6"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("2a5def9e-80bb-94c5-3a0d-90e3076e38f0"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("2b5e417f-d76f-7058-7d73-8c63d0848b47"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("2fddf619-b341-7243-2cd7-ed95836cbc9d"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("3b4d37d3-351d-e266-05e7-8667fbddcd36"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("3bd7d25a-be7f-80d0-3bec-1795cc75f20c"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("4579be77-2e22-bfe4-c327-8bce2175ebb2"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("47ed86d7-99be-2d57-bc27-dd0192295684"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("4c1476e0-6657-1ee2-028d-c3b5a6689056"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("4e9f6d20-27be-efe9-c156-835a0e4e15ac"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("52f50405-2e8b-1514-2236-3d861260a09f"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("5659a1a6-b334-2387-dd84-2e2eaea9eed0"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("5a7343f1-db54-6580-2425-2e7dba02c8bb"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("64db8e8f-26f4-2a11-36e1-4b86ae78416b"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("6d4d1ce0-20b5-b8ae-e4cf-47f4f93e0f05"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("75ac8f04-4930-a8bd-ac67-f648d2ee6a79"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("75e6655f-0438-1983-781f-e478a55348a7"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("7b154df2-5d18-c7bd-386f-6c2cefed9e03"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("7c73741a-aaa8-a414-2d19-e00d22fa3d86"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("800421ec-e40a-9dc9-5d2a-d8d5f0e19577"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("82151266-f23f-9b0c-4656-5e7c8c53c114"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("8701add4-505c-9919-7618-2e2fc0a7f5fb"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("8a58b8cf-3719-0edc-126b-e63fa1467158"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("9311a9c1-74fb-04b9-d2f8-54cb72b86c25"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("9a9ef423-04f9-02c4-75e8-6ab60d5b9411"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("9cd98795-6c5c-0169-98fe-3b68f6b91e92"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("9f7f5db1-f5c6-acb4-3f3e-fc5f4d5112a8"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("a623a55c-3dd5-4c1f-7f11-bb6ca667b836"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("afef892e-6ddf-f78b-af7d-256318f2d6f4"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("c216df5c-50d4-6ae0-1679-79b5b02aa593"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("c5d1b821-d605-3534-ac50-c9dc0410bfc9"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("c5e39430-006c-b382-bd2d-0d179530843f"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("c70de7c4-9bb6-f439-52f8-0a911c2a5884"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("d6e43927-1eb1-887b-55f7-bdfa945b53f8"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("e3f14653-6a68-3761-a001-72f6fb664fbb"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("f210fadd-f385-8e63-1874-352b279a41cc"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("f55c3dcc-ad71-e092-4232-34fa5b32e24b"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("f5a44759-d677-4998-987b-b8d39f75e7fc"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("058753b3-3837-9aea-f0f7-0511c2d29d4b"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("0b66a772-f8ba-2a8f-7cff-611e863dff14"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("0f358ba3-8a3c-2bcb-a2ad-c3eae579db79"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("2c3e6a90-25dc-0f65-d57a-4000c4b96044"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("425dd829-9b9e-201e-bb22-87b28388a9a6"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("4a3110a4-0c2f-34de-523e-e0b900790540"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("4ec824fd-fc0f-b47c-44eb-e0622c8d4d06"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("5f9fa83f-6cbd-c5b8-05e1-4ee1b7c3c701"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("6a5205f3-feb9-d175-962f-22a9ed849b66"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("78ac5e27-766b-cf6d-c472-46f74a23448f"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("822b7a9e-e1c5-1495-ca67-b2aaae095cbc"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("923ca8a6-1d68-fb7a-37ab-d9eac6a9193d"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("a090ff59-c55b-00a2-aeb1-9e6f8e7437f1"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("a53c05f1-dee7-fe26-1ae6-e4ce7e13bb49"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("b0130de5-0167-51f9-9652-76057f1adc7a"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("b0b20bef-ee68-9076-8e33-83226a277b8f"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("b6d1a298-a50a-157c-7422-0dfaed645f2b"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("babc03e8-adfe-ab67-cccd-3d43ae9f1a47"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("e580e679-ec05-fecd-c5ea-0ca425b9776d"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("f30dd851-b816-40fd-e9df-97223f7668da"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("0ae57fc6-ab6c-1770-e443-34af5836288b"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("18ad7730-abae-2ff3-291c-4d098d623787"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("190a9a8e-6ab3-3c92-b5a2-2b93ba3c25bb"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("2dafe3c0-a279-987e-2107-257e24ae1034"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("3e84ffc7-0fb2-c5c4-812e-711fb9713894"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("45d697c4-dd92-c266-249d-7b2522d95405"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("54cbeff1-bc4e-b8bc-085c-5fcef84901c5"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("5a106410-72c4-2306-13b3-0a1409186a97"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("8a8b4e83-105a-6b0e-b01a-d57f9073f167"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("8cea1109-a4d9-15df-3571-97b9eddc511f"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("8d1a5116-082e-786d-c51a-004842a4f743"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("8d9a7156-d169-f7c2-c9bf-661cb8482769"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("934ffda7-2981-8675-882e-d688e0257d8c"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("abda5ba0-e1b8-ad90-02e6-79e8b928cbad"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("bfb1409a-1b30-1ba9-2b65-fb8b469511f6"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("d20a4b2a-d920-80c4-e396-19ef9327f24c"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("ec39f26f-c3ba-926e-27ef-852e77817ae9"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("f0c4af24-6d8e-d39f-cccf-8d568ca23226"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("f29200bd-2d91-1ef1-fd90-c060176f0d18"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("f54e5991-8125-c329-cdee-c5e5ab778280"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("fc49c059-de46-c66f-a1f4-ee1737a1dbef"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("22ec42e5-d991-d18e-d9b8-271713e2c2c2"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("23ade046-012d-7f1c-4aa0-73562214b462"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("25c1263a-d432-5147-b5b6-fd2ee76f8c5b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("2c8873d3-0915-8721-252c-6ac8e77a6c72"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("3d754e21-32d6-bcfa-3123-945fc99919a4"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("4240bc0e-f0b9-0020-dcf7-8cbf7fedafff"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("51aa6206-2a1b-8574-7109-5cbc9d0cb5d2"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("80821748-ff3b-759d-7600-f6cd1e6ed01b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("9e946255-a266-9c1c-c43d-c35e86e51871"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("ac4fd606-c816-2c51-cb97-6acb0c3ba898"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("b5160fee-9310-8c33-ac62-7b663cd0e930"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c3617645-dae9-cf74-e1df-5425d4567ce4"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("caa1fe61-404b-6aab-3824-39d8b25191cd"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("cadcd8a6-8a01-ff2f-3594-4201ce60e94e"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("d3eab76c-ddd1-350b-ea92-75d5d11ac2e9"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("dcaf5213-c961-2bae-9266-ca7a4140818b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("e1bb0da4-51fd-b627-99c7-1c0faf74f05d"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("08644456-3ebe-f008-7ce7-c6e9cb0c334a"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("13208737-53bc-80c8-483b-8e63d9d042c8"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("1a8b10a0-df5e-4b1e-fa34-ddb35be0eb8b"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("23d0f4ff-61f4-cf86-f9c3-c9d01567ba63"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("292d1131-2381-b342-3a05-72ccf4e3d147"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("2c850a60-85fc-7d04-b9f7-32159895f8e5"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("2ef232d7-920f-4a24-11a3-4e576177ccfa"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("37323f33-86c8-3f82-842a-a4d548beabb8"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("3cbf7731-ffcf-0075-6676-bde35fddb8d1"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("3cdf8fd0-1f92-3e5b-f04d-c9f45530bcec"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("428ac028-9d86-4ced-8c8a-a85d4b9c1e3d"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("69f34ae5-db6d-2a23-2ac4-8bc2d0a17470"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("70176971-a6cc-265a-0259-d18af3babc70"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("8b3f7c2d-70e0-90b6-5263-889b49b96b64"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("9038bc44-07b5-7d03-f25e-24555f29aff4"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("a0fdb218-16c6-056f-cc8c-cf32a6134249"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("a32b4e75-4941-9679-163a-1a57f6dd702a"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("a417997b-6a8d-1047-8b38-49481bd044cb"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("d8035464-71c8-ac5e-234a-9b805a11a569"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("dc304c87-09f7-8fab-b2f9-bd99b6da2aea"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("e7b7328a-7a58-2d20-e6e9-ae675adfc3f9"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("419b6b26-8c6d-1ccc-fbe7-3cf93e594ade"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("477576f6-398d-2e8d-60ad-8724cb1e355c"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("4e38f433-caac-573e-4646-7df396ed495f"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("570ba0d7-214c-9d3d-601a-7c9141c09ee5"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("62bacc24-92af-2457-cfc1-8901035b8d54"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("677c530a-3d2d-f2f7-47c8-e1a01d801fc6"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("689e82f2-b006-3f1f-ac57-d17dbfc75b5b"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("68fdcb8a-8c50-a950-38dc-2ec486f5ec36"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8f1cab08-e6d7-7fba-260e-6ae892375b70"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("9063a2f2-b2e8-fb0c-eafb-1ea697155f37"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("97d69928-dae5-c4ff-46b6-493847444096"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("9ee19294-5777-af49-8bd4-5338ce965509"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("b6fcb092-ac8c-f934-e8db-d25e9ab4d170"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("db3323c8-0d80-f637-2bae-93f36458eaea"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("de7a58f1-b785-ec87-b1ea-2d1bd1b12a22"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("eb0cfd23-c79b-a4dc-7517-bf0a0892d4e4"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("edb64a32-5259-abac-d0e3-fcc82b2e920f"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("efcca2ee-7acf-09f6-5dcc-8fe04200d997"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("f5a1ac76-08c4-4b03-0aa3-08c4fbbafff5"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("f5b58a27-6b3b-1d35-5cf0-c5c70a1d0d84"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("109571f6-d42a-79e4-b7d3-aed716fb7ae2"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("161d9192-c183-9e3c-bd02-4935c8b84557"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("1c644315-1e5b-46c1-cd53-c75e7d46f2bc"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("24cba960-243f-8eed-f954-388350f7a42e"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("257238c6-a427-b0ed-12ca-d989926219e1"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("2ff2b15a-2163-a874-8980-e3b467a12a14"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("329a58e6-03ae-1f52-cf69-6f0bc15af361"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("39692b63-4874-aae8-4bd7-4c9ee31c61b3"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("43856184-28f8-18f8-ce8f-30a4c88688e4"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("4edec551-e708-3c41-5a2e-54b4f90c978d"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("4feda6f9-d3fc-5c87-7c3a-39b6ab33f4d8"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("5919fa5c-f41a-e47a-2bd2-355a426c0d49"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("6bf84cec-7d37-7772-db15-54951c7b8b5f"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("6c54248f-902e-27b7-25a9-1628050db7bd"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("7243e63d-9bfc-0972-fa64-5c9468bfc3d1"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("7b4c3eaf-b74f-164b-c658-902665aafbeb"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("7d680ada-8fdb-c6f1-d85f-9aa4bd8fc81b"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("8b847645-fafd-0e79-daf8-b34e6bcf2b8a"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("8fae5f78-cf7d-4cc2-ceb0-31b87178284d"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("a45dbb54-3345-1d1a-a0dc-6524630df8c6"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("c79beded-a660-c08a-7f24-8228409e7cdb"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("d961269f-e5d1-1541-26ed-f97767e65a6c"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("df2d60b1-6e5a-e140-735c-92b88fc72a7c"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("ee544476-56a0-2e56-5df0-8c40fad4d07e"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("efaa2963-c4a9-6831-acfb-445bd3c1c265"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("4698230e-e5c9-2b80-539b-7bd29e469c5f"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("60a2f32b-01ee-c7eb-6890-3a42c679f812"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("6ffde2a4-e3e3-c753-2e7d-5013521f25aa"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("92ac021c-7937-6a0a-372d-aecccea2d620"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("c916e51d-7541-f313-d3dc-353f64db448b"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("cdd65641-25c3-1c01-0d79-02f17be21358"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("02d67cc5-e4a4-221a-d105-59123387a71d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("07016d05-80a3-f718-2935-f6eea5cdf14e"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("13793282-54c6-a960-73fe-74712bac9cd7"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("14eddb72-921e-0b40-3645-e2984f8dfb07"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("1823ff21-532b-5cf5-44f6-75ad79ea6111"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("1f8a4088-c807-d1da-71e4-62af9eed2a86"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("2161ea44-bda9-09c5-626f-ffd1c3377ed7"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("2ab5cdca-1799-a324-71e3-9bcaab100e57"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("39a0519e-a73f-06d1-9672-6b967a564156"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("3b718030-4722-6868-a6f4-11a170b5b4c2"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("48fe92b0-ef9b-5039-c05d-fd3e65d7af4b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("4fafe883-6c3a-446a-0a37-8bcbc0281522"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("5e01695b-cfe4-8bdc-8007-d57c539f9494"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("604863e4-0320-0b09-817c-07d7f1b8df30"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("6383759e-1eb1-42b0-8ff3-e65177c1b02c"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("69261de2-46c8-a164-6032-b0047c1aa7c7"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("6b614d2f-5210-4165-1861-37dc095ddf14"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("6fa27318-76f8-bc4f-e774-5e0a0b72d8f1"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("7bc8922f-cd77-ec58-7807-c3b9d048d473"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("8330229c-8d98-fdc9-c7aa-8c09b9e49d12"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("845097ca-477a-c4a3-703f-98acf1d2314a"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("94b44eeb-7b80-be4c-507f-ebcfc4c0a9e9"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("bf50bdb0-921b-0544-4ea7-424f95f450fc"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c742eab8-7988-a282-91de-90534583c6ea"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("cc981adb-bedb-0273-335d-6f9af93d14d3"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("d3ce43d0-86f4-d863-7d8f-eb0afca5b631"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("d4e80b03-af95-918b-2308-fae38ce2c309"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("dbba385f-3b3f-dd9e-86c5-ea073272417d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("e516289b-2834-3265-b7ec-aef411012bdb"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("ed5c88d6-cf0b-e7a6-b1da-6aaf30b22431"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("f79c4f74-43f4-6a14-6c5d-ef0ceac436be"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("f8dfce2a-c6d2-e49f-b749-f6b182ae8a64"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("0e02ac36-74c6-9f0b-0d15-005d5d84dfbe"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("64735d54-7642-72c8-659b-7616a68a3338"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("6585182d-2c5a-11bb-a70d-22318d15b950"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("707740bf-6987-5dc0-87db-230cd35bed46"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("737a5e3b-fc31-080b-d386-725d2e47f4c9"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("9b50b98c-25b6-12bb-5cd4-fd740442f74f"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("c757d324-c682-9d14-f213-adffd86f17fc"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("fdd2cb5d-621f-f1cf-4a18-78301b15a936"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("2787ed82-4670-1d19-ddc3-29ec8b2ff54b"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("6178377b-00c3-b8ad-1483-23b0e32b5c48"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("802c4c29-235d-1382-a9dd-e6c2dc754817"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("b42e3f0d-5d13-c155-6986-da482ceb06e5"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("f53df199-020a-5341-a3e0-b96d83d96b41"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("00e54572-9329-cc20-8146-45548f7f4fcc"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("03c19da2-646d-e11e-cdd1-5e329bb32962"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("17a6f792-66c6-b278-b806-19485cacee54"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("19298057-71d1-e643-d8d2-75a075a133ce"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("2a738e5d-dca9-82bd-17e9-9ef7db1b7d98"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("363bc0cb-083e-5630-71ba-dabbacd706dd"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("4c4d04a1-b098-8ce0-652c-d96d36dbbe44"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("596e114a-dcf4-28b6-d40e-60f7ecd2c3e8"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("5ed3fbd8-5d8a-5141-0d80-b6bd5ee3961b"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("6148b7c1-313d-0b71-99ee-aed730fb76ee"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("66dcd09b-a68e-fa1a-e5f5-242fe8f40fbe"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("97a802c2-c94f-bed8-925d-31478f9b50e8"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("cd4ac6da-0caa-def4-be85-c2d738cf916f"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("d918a0f1-d04a-4402-5035-c1b95e4e8a48"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("dc92c1ac-22b7-9fd3-2696-61fd96d2801b"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("ecc1fb02-1a2e-902c-309d-8f348ae34023"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("eece8354-25ec-4cd2-c00d-f56eed3b0225"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("f777c83e-2104-c344-3f79-db4a3b634b4b"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("1388baaf-6ce5-dfc4-530e-74cb59bfb41d"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("2224bab3-fbc1-d35b-120f-b791fdc09be5"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("2deeb484-ae82-07ac-87ee-f29d378b1f18"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("30ca3495-b39b-05c1-eb6d-5631b92acd9b"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("52b8db66-8bb3-d9b9-2c19-fd0c4091e542"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("54441296-7f5f-8b1d-c61a-4dbc33d4a21c"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("67c24ff5-7a90-8e66-2d76-c71dbb821837"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("715901aa-987a-0673-33da-522121bec10a"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("8e6ffe4d-89ab-1d0d-7974-85cd99e0807a"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("ad117f58-44b5-b5ca-e32e-f037ec72c640"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("bbeade3d-2f8e-be2a-c4ee-94915f9d03ea"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("e29e5fe3-fe78-cb5e-e583-376c6eae57b0"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("e5593121-ceb7-1f00-f78e-7148efe4d715"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("f8342ea6-d3c8-4288-5cfc-b139ae092c02"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Orders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                table: "Parts",
                columns: new[] { "Id", "CreateDate", "Name", "NameLeader" },
                values: new object[,]
                {
                    { new Guid("77099fae-559e-3a16-4c8b-97521d98abbb"), new DateTime(2022, 8, 21, 13, 59, 1, 452, DateTimeKind.Local).AddTicks(6578), "Jacquelyn_Koepp", "Verlie_OKon" },
                    { new Guid("84a6d2a5-56c2-677c-ae5c-0ef72b7df197"), new DateTime(2022, 8, 28, 17, 39, 20, 538, DateTimeKind.Local).AddTicks(9995), "Vernice93", "Yoshiko29" },
                    { new Guid("9f1e6777-de9f-936b-bff3-27c83e69b096"), new DateTime(2022, 8, 19, 8, 11, 23, 79, DateTimeKind.Local).AddTicks(6051), "Jacinthe_Haag", "Junior.Kozey8" },
                    { new Guid("79feb4f7-8bed-934f-4aee-5ec70f7da364"), new DateTime(2022, 8, 6, 16, 6, 26, 944, DateTimeKind.Local).AddTicks(7263), "Connie_Miller54", "Raven53" },
                    { new Guid("ae165182-f74c-cf5e-e6b6-9546059c79ab"), new DateTime(2022, 8, 14, 3, 26, 2, 437, DateTimeKind.Local).AddTicks(349), "Sabryna89", "Mateo.Nikolaus26" },
                    { new Guid("93a3725d-c055-ddbd-08fd-7d9ba167693f"), new DateTime(2022, 8, 24, 5, 15, 19, 848, DateTimeKind.Local).AddTicks(3421), "Baby_Watsica84", "Stefanie_Price" },
                    { new Guid("3c780d06-9eee-bb51-99ec-2b5260d48927"), new DateTime(2022, 8, 31, 19, 53, 46, 45, DateTimeKind.Local).AddTicks(3260), "Lyda91", "Anibal_Osinski54" },
                    { new Guid("2f0d38a8-231c-b74e-baaf-65cc0ede4ae6"), new DateTime(2022, 8, 10, 3, 47, 49, 223, DateTimeKind.Local).AddTicks(2285), "Laurence.Steuber23", "Alberta.Howe7" },
                    { new Guid("c8357dc2-8c20-f678-18ae-6acf0804a81a"), new DateTime(2022, 8, 11, 7, 36, 48, 817, DateTimeKind.Local).AddTicks(6304), "Chasity66", "Adah.Mraz88" },
                    { new Guid("9daee96b-95b1-d142-8c3a-84e7453f96e7"), new DateTime(2022, 8, 12, 10, 38, 23, 711, DateTimeKind.Local).AddTicks(3070), "Gussie96", "Federico.Bayer" },
                    { new Guid("026dd299-4389-0132-dc21-8f487bfcd58d"), new DateTime(2022, 8, 12, 13, 43, 43, 517, DateTimeKind.Local).AddTicks(5035), "Abbigail.Deckow", "Briana_Hansen" },
                    { new Guid("696630a3-f998-c36e-2b07-13dc562114d3"), new DateTime(2022, 8, 9, 9, 5, 25, 290, DateTimeKind.Local).AddTicks(7473), "Jeremy45", "Gideon35" },
                    { new Guid("1cd04874-964b-8299-4192-e6a3f58978a0"), new DateTime(2022, 8, 27, 13, 19, 42, 283, DateTimeKind.Local).AddTicks(5177), "Melvin_Crona", "Mina.Von43" },
                    { new Guid("fc556c66-c305-e207-c7d1-307b80ea3e93"), new DateTime(2022, 8, 11, 2, 17, 58, 67, DateTimeKind.Local).AddTicks(4347), "Luna83", "Jamison46" },
                    { new Guid("3f4e0c83-a151-703b-9794-f70d948147f4"), new DateTime(2022, 8, 10, 5, 42, 38, 611, DateTimeKind.Local).AddTicks(8943), "Skye.Aufderhar44", "Miguel_Herzog" },
                    { new Guid("de6d8034-3aec-3962-81b7-b8a37530f50b"), new DateTime(2022, 8, 23, 18, 59, 5, 542, DateTimeKind.Local).AddTicks(5387), "Mariana79", "Celestino68" },
                    { new Guid("89a2ce03-f644-a9c8-cb6d-bb02b9a8bdaa"), new DateTime(2022, 8, 7, 5, 47, 7, 4, DateTimeKind.Local).AddTicks(818), "Milton36", "Mireya.Walker14" },
                    { new Guid("22103fbf-32c2-d579-e86b-c0b03dc1f0f9"), new DateTime(2022, 8, 26, 11, 26, 51, 509, DateTimeKind.Local).AddTicks(9398), "Destinee.Balistreri29", "Gladyce45" },
                    { new Guid("0117b9be-469a-ed5b-3788-99a8aa8989e3"), new DateTime(2022, 8, 31, 8, 16, 11, 249, DateTimeKind.Local).AddTicks(7179), "Nasir68", "Orland.Green66" },
                    { new Guid("12272521-34b5-511a-73bf-bde7d179544f"), new DateTime(2022, 9, 2, 15, 39, 59, 174, DateTimeKind.Local).AddTicks(7821), "Mikayla.Marquardt93", "Haskell_Rosenbaum" },
                    { new Guid("f0ae5df0-206e-29a8-2536-c525d2648e58"), new DateTime(2022, 8, 23, 16, 45, 4, 933, DateTimeKind.Local).AddTicks(2408), "Scot60", "Grayce87" },
                    { new Guid("575897e3-a571-24dc-7e44-bca282519d3e"), new DateTime(2022, 8, 9, 11, 24, 4, 104, DateTimeKind.Local).AddTicks(484), "Jean50", "Jordyn2" },
                    { new Guid("1525b69a-80b7-3c07-ab8b-8950d1b491e9"), new DateTime(2022, 8, 18, 11, 30, 5, 71, DateTimeKind.Local).AddTicks(1914), "Nels_Terry", "Ian90" },
                    { new Guid("c651c8cf-fb12-6264-9186-0bfb763e3581"), new DateTime(2022, 8, 25, 9, 52, 53, 258, DateTimeKind.Local).AddTicks(5640), "Nia.Wehner7", "Otho_Yundt" },
                    { new Guid("deb3562d-0087-5e3e-9415-ea062df917e8"), new DateTime(2022, 8, 27, 4, 35, 34, 258, DateTimeKind.Local).AddTicks(619), "Jennings.Lindgren", "Wanda_Torphy94" },
                    { new Guid("1a9f0107-2b38-cf06-db18-c2d4cf03f652"), new DateTime(2022, 9, 1, 18, 38, 20, 767, DateTimeKind.Local).AddTicks(328), "Alexys_Trantow78", "Sonia.Gleason34" },
                    { new Guid("4b62c635-b681-1e61-863a-5275afc8eb89"), new DateTime(2022, 8, 16, 10, 48, 4, 583, DateTimeKind.Local).AddTicks(5137), "Samson88", "John_Mitchell" },
                    { new Guid("344b2122-0d80-5d59-c7bb-dd21523188ae"), new DateTime(2022, 9, 3, 8, 51, 25, 293, DateTimeKind.Local).AddTicks(4524), "Rowena_OKon94", "Vida_Miller72" },
                    { new Guid("0eebb743-07b8-f025-36e5-2823c748ed82"), new DateTime(2022, 8, 23, 21, 59, 49, 410, DateTimeKind.Local).AddTicks(9439), "Jameson.Dietrich", "Louie_Schuster73" },
                    { new Guid("a2a96c5c-bf9c-cf1e-5b9d-2a2b93847162"), new DateTime(2022, 8, 20, 7, 29, 31, 722, DateTimeKind.Local).AddTicks(7570), "Richmond.Armstrong", "Maddison_Casper49" },
                    { new Guid("edaef557-a7e0-4263-aab9-4693496804f8"), new DateTime(2022, 8, 31, 14, 35, 22, 174, DateTimeKind.Local).AddTicks(1235), "Althea10", "Freddy_Auer99" },
                    { new Guid("7b4c13c1-b403-fa9d-6828-206dbd83c851"), new DateTime(2022, 8, 8, 10, 57, 19, 498, DateTimeKind.Local).AddTicks(9108), "Darron_Olson", "Stevie44" },
                    { new Guid("c4f19d18-7154-7f18-bce6-4ff52eb592f6"), new DateTime(2022, 8, 13, 4, 40, 22, 996, DateTimeKind.Local).AddTicks(7091), "Janet48", "Friedrich95" },
                    { new Guid("1a79cb28-a6b9-1c80-87ea-16ac3613b707"), new DateTime(2022, 8, 21, 22, 9, 50, 130, DateTimeKind.Local).AddTicks(5183), "Alva51", "Francesco.Mills" },
                    { new Guid("d2379191-7d30-efc7-bc9e-8bc9c312644e"), new DateTime(2022, 8, 8, 3, 55, 43, 593, DateTimeKind.Local).AddTicks(6994), "Rosalind_Parker41", "Kaylin.Armstrong" },
                    { new Guid("130e3904-2d03-8992-e3f4-90d306aedeb0"), new DateTime(2022, 8, 13, 11, 25, 15, 725, DateTimeKind.Local).AddTicks(9089), "Ruthe80", "Ora.Beer22" },
                    { new Guid("08671406-da93-ae29-788e-e0a99a5e373a"), new DateTime(2022, 8, 27, 16, 39, 7, 192, DateTimeKind.Local).AddTicks(1514), "Katelyn_Mann", "Martina17" },
                    { new Guid("6ca3a999-d370-cfe1-ea26-7d22da61b6aa"), new DateTime(2022, 8, 21, 19, 42, 9, 517, DateTimeKind.Local).AddTicks(9831), "Clement78", "Margarita.Franecki56" },
                    { new Guid("58b61a1b-8674-a6c8-372c-a69f65023fcf"), new DateTime(2022, 8, 26, 10, 44, 51, 469, DateTimeKind.Local).AddTicks(9362), "Elaina58", "Annette_Walker" },
                    { new Guid("4b5810ab-d617-cc88-d764-ba900e5caa32"), new DateTime(2022, 9, 4, 0, 8, 20, 164, DateTimeKind.Local).AddTicks(3764), "Glen_Doyle0", "Napoleon55" },
                    { new Guid("e0e8e101-53d0-90d5-0cdc-71bcc8589b72"), new DateTime(2022, 8, 22, 13, 23, 27, 375, DateTimeKind.Local).AddTicks(7992), "Kasandra.Ferry68", "Genoveva_Terry" },
                    { new Guid("0fc3151e-68e7-828b-cc00-c64929ddb0b1"), new DateTime(2022, 8, 12, 4, 57, 19, 413, DateTimeKind.Local).AddTicks(3249), "Webster.Koss99", "Bernardo38" },
                    { new Guid("d3896182-174d-b8ba-35bf-cdfcf4083146"), new DateTime(2022, 8, 18, 14, 7, 45, 508, DateTimeKind.Local).AddTicks(9585), "Kenna50", "Sid_DAmore" },
                    { new Guid("300842b6-f73f-6c1e-40e6-29f8f9b3ae8e"), new DateTime(2022, 8, 22, 20, 35, 23, 593, DateTimeKind.Local).AddTicks(1965), "Eugene.Kihn50", "Kassandra.Hahn" },
                    { new Guid("693af881-eddb-a025-96d5-b187f6c8af90"), new DateTime(2022, 8, 23, 2, 11, 3, 290, DateTimeKind.Local).AddTicks(3240), "Clarissa40", "Stefan_Schulist" },
                    { new Guid("327f83fe-bdcc-8f1d-eb76-2531d70a3cd2"), new DateTime(2022, 8, 13, 7, 42, 59, 83, DateTimeKind.Local).AddTicks(1338), "Shakira.Krajcik", "Sammie.Goldner" },
                    { new Guid("52e36235-98c1-61fb-9cdc-69a66dc91794"), new DateTime(2022, 8, 16, 12, 39, 11, 281, DateTimeKind.Local).AddTicks(115), "Donato_Moen96", "Ole.Rolfson2" },
                    { new Guid("4ce2fc23-4a5b-97c3-5a39-3c29dd29d03a"), new DateTime(2022, 9, 2, 15, 29, 55, 914, DateTimeKind.Local).AddTicks(6864), "Clyde_Spencer72", "Micah_Harvey" },
                    { new Guid("21126ff6-e1ce-29af-f931-854208bbffd9"), new DateTime(2022, 8, 21, 8, 14, 27, 724, DateTimeKind.Local).AddTicks(271), "Merle.Lesch", "Torrance.Kertzmann" },
                    { new Guid("62794e50-f1b4-84a4-9eb3-3e0f2165a111"), new DateTime(2022, 8, 10, 1, 15, 2, 276, DateTimeKind.Local).AddTicks(7282), "Gunnar.Strosin", "Mac.McLaughlin32" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("c3f318d6-7f4d-3918-ad52-fe829b0b253d"), "Bria Heathcote", 8.996558517681695m },
                    { new Guid("a4883564-6048-79b5-4548-c8292f81db2b"), "Raegan Bernhard", 5.4157505232914124m },
                    { new Guid("6efdc076-1252-bad3-7a9c-be5bfda6156d"), "Kallie White", 6.97627820213152m },
                    { new Guid("2a287412-7dc6-1cc7-d695-abf913f0b177"), "Jewel Spinka", 9.332459848528945m },
                    { new Guid("e2eaf27d-9cd2-6dbd-a73b-549aaaea146d"), "Ava Wiegand", 7.588406936539529m },
                    { new Guid("81c6bfcc-641c-85b3-2cf2-f43daa7ee649"), "Cicero Green", 11.087673159822667m },
                    { new Guid("d82e2668-b0dc-6ff3-bc59-80139142ce6f"), "Christy Breitenberg", 10.429283555331307m },
                    { new Guid("737162ae-221d-1a0c-a009-f3b527a7b5ac"), "Elouise McCullough", 11.653641652154567m },
                    { new Guid("999c0db4-728d-73b1-8fda-217a30580f95"), "Anahi Hamill", 7.361583128739884m },
                    { new Guid("b22ac294-c14f-f51f-6173-59387aa0eb22"), "Gregorio Hudson", 11.932005824023861m },
                    { new Guid("2730253b-2607-c546-2d1d-0943043dc960"), "Jerod Treutel", 5.8787329764472m },
                    { new Guid("b461ef5d-b6bb-5863-0790-d710877fec22"), "Marcelino Borer", 7.460381209599033m },
                    { new Guid("ac477294-5684-9685-1d3d-67f7165bff0c"), "Enos Schamberger", 9.463269794109868m },
                    { new Guid("4e7bc7a7-9a78-a7a0-6377-9f8ad2dd11a5"), "Ozella Roberts", 6.228857933184532m },
                    { new Guid("c803b517-9a6e-3367-751a-a6bf4313dcdb"), "Verner Walsh", 9.001812335570252m },
                    { new Guid("aee312cf-1f22-b37d-9419-05b6f3ee7d88"), "Jaeden Steuber", 10.061212297091825m },
                    { new Guid("6610b40d-8d2b-b7bd-5c77-f601f38ce94e"), "Salma Schowalter", 7.830638906373943m },
                    { new Guid("e7f1a9e0-19d2-6370-b3d1-c68a7836200b"), "Dennis Cole", 7.857189617984548m },
                    { new Guid("676421b7-3c21-179e-122a-099feb545ec3"), "Yesenia Watsica", 5.5868381986333237m },
                    { new Guid("29bb4c3e-e60a-ec96-7dcb-d2807990e67c"), "Ross Hansen", 6.01289973362018m },
                    { new Guid("0234abd0-395e-31a9-db35-1a72cbefa280"), "Jarvis Kris", 6.702167604911221m },
                    { new Guid("4f8b815a-4c20-a93f-8f0f-04d87947124b"), "Yvonne Hegmann", 9.728028183210654m },
                    { new Guid("c23b8ada-45ec-ec83-4709-664395811ff4"), "Kathleen Marquardt", 7.950644648610918m },
                    { new Guid("0b9b5165-5f88-d78b-ce2a-8c75a6cc64b3"), "Gregory Koelpin", 8.762016928643925m },
                    { new Guid("a79aa2ed-5ff4-2c65-725e-368a13aad6c5"), "Tara Shanahan", 7.246028008985348m },
                    { new Guid("cfebbbe6-d9e2-6900-1a92-955d503da84b"), "Augustine Cronin", 10.247355354599358m },
                    { new Guid("992aae9b-bc81-3de0-3dd2-7269635706c9"), "Hardy Connelly", 5.943420555416227m },
                    { new Guid("bb909bf9-f076-53ef-44ff-495aca31c29a"), "Lorine Roberts", 8.262521193485022m },
                    { new Guid("9b1a54bf-87c2-e2e9-f6e4-9740a1fe41c1"), "Lavinia Gibson", 9.899702213657881m },
                    { new Guid("517145b8-0f50-fa45-f3f7-bae62a5ffd13"), "Minerva Bradtke", 8.326727589278821m },
                    { new Guid("0e8d34ce-6ef8-bfad-a75b-886f3b1a93f0"), "Avery Boyle", 5.06385563363500664m },
                    { new Guid("20f11321-afa2-7f99-ec6a-48ad50e286b2"), "Tia Collier", 11.32430221202052m },
                    { new Guid("c84b0e4d-0a29-c2be-214b-16e5cbd819bd"), "Jaron Stehr", 11.007799451708698m },
                    { new Guid("ef5f5eea-54c0-ae5f-388d-b4670bb13f2b"), "Corbin Donnelly", 11.383335596128988m },
                    { new Guid("ece316c8-3378-85d5-7cd4-37249ad57554"), "April Cartwright", 9.285789950418186m },
                    { new Guid("5bc9738a-3586-e36c-0b33-8cac50e7a114"), "Greg Wisozk", 11.258070840154805m },
                    { new Guid("4f0cb75d-51c9-28ef-e63c-f8c0774d7d07"), "Nannie Block", 5.6908566340295858m },
                    { new Guid("cb8581e7-5051-d676-3c6e-a89cca1642a9"), "Wiley Lindgren", 7.29559111143257m },
                    { new Guid("7d465ccc-87f8-66d1-4732-5c65aad55951"), "Cindy Koepp", 5.2855873109240025m },
                    { new Guid("7a53feba-b9b3-a03e-2ea9-35a4c32cf77d"), "John Christiansen", 5.6028557646101599m },
                    { new Guid("b1f37e21-2d19-d222-1e0f-d23644ac3216"), "Aimee Connelly", 7.732550795531158m },
                    { new Guid("5f6eb272-e57a-3232-d795-d6b63c544b8c"), "Edna Keebler", 6.074550290161068m },
                    { new Guid("323fc2b0-3e52-6e9d-0f89-4345c445f907"), "Alysa Trantow", 8.897937108715034m },
                    { new Guid("f1c451ef-cd0c-6e8a-7577-9bdbcfbd618f"), "Clay Kuhic", 11.585778105811114m },
                    { new Guid("7e5ddb00-98bd-fc1a-c380-ec6e90db3c4f"), "Pat Denesik", 11.520412374064519m },
                    { new Guid("2a33b051-84a4-a527-1cb1-2f4b057251dc"), "Furman Leannon", 11.935188871312506m },
                    { new Guid("8149b3fb-b0da-aa96-2bd3-f50c89ae21b4"), "Angelo Lebsack", 10.876768408751471m },
                    { new Guid("8cdbf83f-28f4-6be9-0f09-4d2f0d22686c"), "Tanner Bednar", 11.555102884562267m },
                    { new Guid("7e2f9e6b-c757-3869-63ef-97ff09d0cd20"), "Nolan Lubowitz", 7.523264541534366m },
                    { new Guid("5b6e563b-2f59-bf16-89dc-a0647f95edd2"), "Celestino McKenzie", 11.34132793561617m }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreateDate", "Email", "Password", "UserName" },
                values: new object[,]
                {
                    { new Guid("460bbfbb-a698-695c-ebc9-f514a15297c5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lurline_Raynor@gmail.com", "tTxJ_Hatr_", "Romaine90" },
                    { new Guid("3562eceb-3acf-e655-af30-73ce06e9f790"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sam_Upton15@gmail.com", "f0lzHnfusX", "Blanca70" },
                    { new Guid("8b00dff9-8f45-6aab-326a-9c9b26bef747"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alfreda_OKon@yahoo.com", "eBGb_ogbgr", "Josiane.Kshlerin" },
                    { new Guid("8b2813fa-cb4a-a873-374a-15e8aba9e5d3"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Adrian35@yahoo.com", "f7ZHpNddaF", "Virgie52" },
                    { new Guid("06a67d5f-888b-a6fe-ea33-dff4eee07f79"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Shawn_Reichel71@yahoo.com", "80j4QDQ3kK", "Jamir_Shields50" },
                    { new Guid("a021927a-0df3-ccaa-11a0-00d6c4e6648d"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Horacio.Becker36@hotmail.com", "1RsZ8ncLsy", "Willy_Stark29" },
                    { new Guid("de9afa0b-03da-072f-5faa-6642d01f4072"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Howard_Fisher@hotmail.com", "nDRNRF5ws4", "Ella.Stoltenberg" },
                    { new Guid("d18490b1-6c24-67f0-3078-6cab0d35bcb7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mafalda50@hotmail.com", "xlIOrNzH8K", "Donnell_Mueller22" },
                    { new Guid("621f153e-eaf5-1f41-24ad-2891085d7f0e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Meda_Senger@hotmail.com", "Skk2m8sGQq", "Edmond_Greenfelder" },
                    { new Guid("37e09732-c3a8-54b1-b96d-be24d899478f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lourdes_Quigley14@hotmail.com", "MO6PkzwFwp", "Josianne_Lockman" },
                    { new Guid("4a3b8bd6-d35e-0142-830c-b21bda586218"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lina_McGlynn@gmail.com", "wh16v2FVOy", "Alden.Legros33" },
                    { new Guid("cf502e15-6283-ed7e-be55-967d980826bb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Dallas90@gmail.com", "dZyZVMZi71", "Tatyana_Kuvalis64" },
                    { new Guid("87feca68-3256-44ca-8c8b-81db8b86c88c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Deon60@hotmail.com", "4XfYNH91L4", "Adelia_Langosh" },
                    { new Guid("e1835bbb-29b7-77ee-e3ff-8dff540ae0eb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Edwardo85@hotmail.com", "cyx0mrmKlx", "Fae45" },
                    { new Guid("2d9fcbea-cb98-48f0-be57-461a1dc5c683"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Zaria.Thiel@yahoo.com", "bfzXJCZYjH", "Stella42" },
                    { new Guid("09c95b30-2b12-210b-5e83-d44143b8602c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Parker_Brekke@yahoo.com", "v_SwYHlklL", "Alysha75" },
                    { new Guid("692a9301-d046-a62e-9270-5a00a56fe60a"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Isidro_Rohan4@gmail.com", "NKt0BnTmlh", "Gudrun32" },
                    { new Guid("0f72b503-5fff-42c5-da9c-a680dbc31894"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Dereck_Greenfelder@hotmail.com", "5Bn80P019h", "Lexie_Thiel77" },
                    { new Guid("01f9697b-ea02-8bcc-073a-0f817261a877"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Katrine51@hotmail.com", "TGbIywUzyL", "Ludwig_Sawayn56" },
                    { new Guid("9865fa07-979f-63fa-e7f3-ecce7fc013d1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Charles.Hills13@gmail.com", "0KfjhuUBo_", "Kellie.Lockman65" }
                });

            migrationBuilder.InsertData(
                table: "Staffs",
                columns: new[] { "Id", "Adress", "CreateDate", "Name", "NamePart", "PartId", "Phone" },
                values: new object[,]
                {
                    { new Guid("d7f2b07c-fd6a-e29d-27dc-243cfa1c228a"), "187 Flatley Prairie", new DateTime(2022, 7, 10, 22, 41, 21, 88, DateTimeKind.Local).AddTicks(2770), "Rubie Toy", "Elmo Swift", new Guid("77099fae-559e-3a16-4c8b-97521d98abbb"), "1-237-283-6193 x16563" },
                    { new Guid("45a88052-16c9-e615-393c-9aaf2b5b802d"), "105 Jacklyn Street", new DateTime(2022, 7, 18, 2, 14, 25, 513, DateTimeKind.Local).AddTicks(4477), "Cynthia Effertz", "Shany Haag", new Guid("344b2122-0d80-5d59-c7bb-dd21523188ae"), "968.848.6496" },
                    { new Guid("7c385e28-8d6e-15ca-33c4-3c66415a1010"), "115 Mitchell Greens", new DateTime(2022, 8, 21, 10, 46, 40, 355, DateTimeKind.Local).AddTicks(5657), "George Ernser", "Eino Fadel", new Guid("ae165182-f74c-cf5e-e6b6-9546059c79ab"), "842-502-8583 x00272" },
                    { new Guid("ecd43c2c-bd7c-4206-55fe-8dd10590a391"), "51718 Riley Throughway", new DateTime(2022, 8, 9, 8, 7, 23, 584, DateTimeKind.Local).AddTicks(9475), "Mozell Lesch", "Dayton Lehner", new Guid("3c780d06-9eee-bb51-99ec-2b5260d48927"), "1-834-303-6471" },
                    { new Guid("1576ef56-f34c-f0a0-ad29-887b032cc059"), "681 Ayla Fall", new DateTime(2022, 7, 17, 0, 5, 55, 7, DateTimeKind.Local).AddTicks(8111), "Darrel Kuvalis", "Annabel Hyatt", new Guid("2f0d38a8-231c-b74e-baaf-65cc0ede4ae6"), "992-760-9866" },
                    { new Guid("602a59f3-6aae-3d93-15eb-e922f7448061"), "4873 Leannon Street", new DateTime(2022, 7, 16, 21, 22, 56, 442, DateTimeKind.Local).AddTicks(2984), "Enos Cassin", "Haley Schamberger", new Guid("c8357dc2-8c20-f678-18ae-6acf0804a81a"), "794-223-6073 x11451" },
                    { new Guid("27dc472c-0295-1a8f-9fa6-2bcf1dea256a"), "582 Amya Bypass", new DateTime(2022, 7, 19, 10, 54, 44, 700, DateTimeKind.Local).AddTicks(1125), "Camren Kuphal", "Jess Lakin", new Guid("9daee96b-95b1-d142-8c3a-84e7453f96e7"), "248-969-0929" },
                    { new Guid("75e8b596-b7e8-1b04-089d-6b72b4dca91c"), "755 Jody Rapid", new DateTime(2022, 7, 12, 21, 6, 41, 489, DateTimeKind.Local).AddTicks(9518), "Sabryna Fadel", "Darwin Renner", new Guid("026dd299-4389-0132-dc21-8f487bfcd58d"), "1-268-661-0301 x59215" },
                    { new Guid("8c4012dc-78de-45ae-65e9-f3916dbd8e68"), "18078 Herminia Station", new DateTime(2022, 8, 12, 4, 24, 15, 364, DateTimeKind.Local).AddTicks(2183), "Peggie Wuckert", "Caterina Walsh", new Guid("026dd299-4389-0132-dc21-8f487bfcd58d"), "(573) 994-4159" },
                    { new Guid("501e5bf1-0a44-9d1d-481c-455371c5c8b1"), "9133 Pfannerstill Mission", new DateTime(2022, 7, 26, 1, 9, 43, 179, DateTimeKind.Local).AddTicks(2434), "Dortha Prosacco", "Lonny Auer", new Guid("1a9f0107-2b38-cf06-db18-c2d4cf03f652"), "905-888-5102" },
                    { new Guid("7e1a4c29-fe7a-14a2-fef4-178c7e5a24c9"), "23101 Quigley Locks", new DateTime(2022, 8, 19, 21, 27, 14, 457, DateTimeKind.Local).AddTicks(3142), "Cyrus Pfannerstill", "Lucile Wilkinson", new Guid("1a9f0107-2b38-cf06-db18-c2d4cf03f652"), "540-788-3972 x3027" },
                    { new Guid("1bbda07c-89f1-f4ef-607a-1977a8168074"), "16017 Morgan Squares", new DateTime(2022, 8, 4, 16, 31, 18, 98, DateTimeKind.Local).AddTicks(4574), "Dangelo Hyatt", "Cade Schimmel", new Guid("3f4e0c83-a151-703b-9794-f70d948147f4"), "1-311-430-6276 x01585" },
                    { new Guid("3f139fa5-875a-33d2-aabd-582fd2ebb942"), "98453 Batz Island", new DateTime(2022, 7, 18, 4, 18, 27, 493, DateTimeKind.Local).AddTicks(6045), "Vita Terry", "Alvera Hane", new Guid("3f4e0c83-a151-703b-9794-f70d948147f4"), "979.929.2045 x887" },
                    { new Guid("c03695a0-d0ba-cd7d-c8b1-b1fce868607d"), "02520 Swift Curve", new DateTime(2022, 8, 21, 2, 5, 4, 629, DateTimeKind.Local).AddTicks(224), "Janelle Jaskolski", "Cristian Rowe", new Guid("3f4e0c83-a151-703b-9794-f70d948147f4"), "353-574-0929" },
                    { new Guid("0775e4c9-5f72-f699-ce46-2e1834fe6039"), "76433 Osinski Harbors", new DateTime(2022, 8, 20, 4, 41, 31, 986, DateTimeKind.Local).AddTicks(7997), "Gillian Prosacco", "Erling Bernhard", new Guid("3f4e0c83-a151-703b-9794-f70d948147f4"), "224-422-9404 x792" },
                    { new Guid("e53814c3-7e6c-1db8-aadc-b1a5a2de463a"), "56280 Denesik Square", new DateTime(2022, 9, 1, 23, 42, 36, 218, DateTimeKind.Local).AddTicks(1736), "Kennedi Christiansen", "Marlin Kertzmann", new Guid("de6d8034-3aec-3962-81b7-b8a37530f50b"), "(294) 363-8318 x8945" },
                    { new Guid("dd8087a5-6ef5-7959-aa44-6580b8f8117c"), "4226 Vincent Point", new DateTime(2022, 7, 12, 9, 58, 47, 641, DateTimeKind.Local).AddTicks(2327), "Roslyn McGlynn", "Dedric McDermott", new Guid("de6d8034-3aec-3962-81b7-b8a37530f50b"), "(494) 322-1604" },
                    { new Guid("a3335abb-4b2f-1756-8e5c-cae6e3d628b9"), "7444 Hudson Shoal", new DateTime(2022, 9, 1, 5, 35, 9, 136, DateTimeKind.Local).AddTicks(5207), "August Skiles", "Nedra Murazik", new Guid("89a2ce03-f644-a9c8-cb6d-bb02b9a8bdaa"), "649-806-3074" },
                    { new Guid("fed24806-3a93-2c77-3377-383c163ae85d"), "8255 Maryam Bridge", new DateTime(2022, 7, 15, 5, 49, 38, 413, DateTimeKind.Local).AddTicks(3994), "Johnpaul Blick", "Jarrod Cormier", new Guid("0117b9be-469a-ed5b-3788-99a8aa8989e3"), "946-286-6062" },
                    { new Guid("e6fcce4a-8829-86d0-9a1a-a7f2f85381fb"), "194 Agnes Gardens", new DateTime(2022, 7, 20, 0, 29, 58, 648, DateTimeKind.Local).AddTicks(2211), "Hollie Lind", "Corrine Legros", new Guid("12272521-34b5-511a-73bf-bde7d179544f"), "643-601-9740" },
                    { new Guid("5b131988-4db4-058e-ae63-231e09c967e1"), "0773 Johnston Flats", new DateTime(2022, 8, 16, 21, 42, 1, 38, DateTimeKind.Local).AddTicks(9069), "Bell Schamberger", "Caroline Ryan", new Guid("f0ae5df0-206e-29a8-2536-c525d2648e58"), "1-441-411-9739 x10573" },
                    { new Guid("9debb735-9d76-161c-ab78-304e42c55a45"), "4265 Lakin Islands", new DateTime(2022, 8, 30, 6, 3, 49, 656, DateTimeKind.Local).AddTicks(5798), "Louisa Wisozk", "Arch Connelly", new Guid("f0ae5df0-206e-29a8-2536-c525d2648e58"), "(350) 294-0662 x44603" },
                    { new Guid("1de1b04f-d06a-9197-2e58-d0353aa140cc"), "0311 Sophia Gateway", new DateTime(2022, 7, 19, 0, 0, 46, 857, DateTimeKind.Local).AddTicks(8026), "Domingo Kiehn", "Hal Schaden", new Guid("62794e50-f1b4-84a4-9eb3-3e0f2165a111"), "996.686.0184 x8127" },
                    { new Guid("5ad907d8-d545-dfcb-eca4-f41d31066708"), "801 Sanford Fords", new DateTime(2022, 8, 24, 23, 41, 31, 60, DateTimeKind.Local).AddTicks(8525), "Annetta Borer", "Alison Rohan", new Guid("62794e50-f1b4-84a4-9eb3-3e0f2165a111"), "454-941-0573" },
                    { new Guid("1557fab4-7e0b-7c4d-9a64-daa473306434"), "698 Hartmann Pike", new DateTime(2022, 8, 20, 15, 47, 46, 833, DateTimeKind.Local).AddTicks(1188), "Nat Thompson", "Juanita Powlowski", new Guid("62794e50-f1b4-84a4-9eb3-3e0f2165a111"), "730-343-8803" },
                    { new Guid("2c76d6fc-edda-4033-40a3-21df54fe4536"), "8501 Schmeler Loop", new DateTime(2022, 8, 3, 20, 4, 43, 457, DateTimeKind.Local).AddTicks(4112), "Doyle Hintz", "Virgil Smith", new Guid("4b5810ab-d617-cc88-d764-ba900e5caa32"), "218-386-8034 x5182" },
                    { new Guid("38a7e26a-4c55-334b-82cf-c322f31b5206"), "7758 Ferry Lane", new DateTime(2022, 7, 24, 23, 6, 55, 169, DateTimeKind.Local).AddTicks(6771), "Noelia Bartell", "Leonie Nikolaus", new Guid("0eebb743-07b8-f025-36e5-2823c748ed82"), "485.381.8047" },
                    { new Guid("767704d1-46da-8d3b-a963-37b57a4f8e0a"), "091 Kuhic Hollow", new DateTime(2022, 7, 6, 18, 27, 17, 455, DateTimeKind.Local).AddTicks(2410), "Judy Mertz", "Elaina Hickle", new Guid("0eebb743-07b8-f025-36e5-2823c748ed82"), "952.978.3594 x458" },
                    { new Guid("eef44370-6d29-b1fb-25ca-07b26e644236"), "77224 Gerhold Glens", new DateTime(2022, 8, 9, 1, 10, 26, 250, DateTimeKind.Local).AddTicks(4240), "Salvador Dickinson", "Erika Gislason", new Guid("edaef557-a7e0-4263-aab9-4693496804f8"), "(455) 256-3197 x184" },
                    { new Guid("0a90722b-4af9-ea37-eaf1-eb75cb4b1dbf"), "306 Roberts Harbors", new DateTime(2022, 8, 7, 13, 40, 30, 433, DateTimeKind.Local).AddTicks(2158), "Myrtis Douglas", "Reilly Quitzon", new Guid("7b4c13c1-b403-fa9d-6828-206dbd83c851"), "246.377.6402" },
                    { new Guid("fab5d732-c6f1-d7c5-79d3-3c5809b46e0a"), "5035 Murray Causeway", new DateTime(2022, 7, 29, 8, 4, 26, 469, DateTimeKind.Local).AddTicks(1509), "Amy Robel", "Angeline Dicki", new Guid("c4f19d18-7154-7f18-bce6-4ff52eb592f6"), "(472) 400-9230" },
                    { new Guid("f071a7f8-18d3-181b-71c7-11e6bef00685"), "92554 Beer Crossroad", new DateTime(2022, 8, 14, 6, 45, 53, 785, DateTimeKind.Local).AddTicks(6379), "Curtis Baumbach", "Lynn Reilly", new Guid("c4f19d18-7154-7f18-bce6-4ff52eb592f6"), "831-809-4240 x59219" },
                    { new Guid("75b46c44-9b11-e0ef-58ee-c2efaa0ce6ef"), "2266 Von Curve", new DateTime(2022, 8, 22, 12, 17, 37, 745, DateTimeKind.Local).AddTicks(2791), "Max Lesch", "Marcellus Hermann", new Guid("1a79cb28-a6b9-1c80-87ea-16ac3613b707"), "1-407-315-7858 x559" },
                    { new Guid("ee7a66d8-3414-cfa9-eff1-9d5084272344"), "8440 Jacey Stravenue", new DateTime(2022, 9, 1, 9, 27, 2, 382, DateTimeKind.Local).AddTicks(4049), "Dayana Wiza", "Dax Homenick", new Guid("1a79cb28-a6b9-1c80-87ea-16ac3613b707"), "(635) 551-0290" },
                    { new Guid("1c5d8ef1-bc50-74b1-ac03-7bab477c0ec1"), "700 Quigley Street", new DateTime(2022, 8, 1, 12, 7, 0, 106, DateTimeKind.Local).AddTicks(2108), "Roscoe Boyer", "Asha Cole", new Guid("58b61a1b-8674-a6c8-372c-a69f65023fcf"), "(380) 504-4296" },
                    { new Guid("18c0e958-9922-5d45-d175-8c15fb17292e"), "09018 Beer Station", new DateTime(2022, 8, 9, 18, 44, 52, 732, DateTimeKind.Local).AddTicks(8007), "Newell Gislason", "Jamar Senger", new Guid("58b61a1b-8674-a6c8-372c-a69f65023fcf"), "1-523-840-7736 x2804" },
                    { new Guid("77392d92-a0f5-f311-377b-3cea2facf3eb"), "92207 Leffler Meadow", new DateTime(2022, 7, 23, 16, 50, 41, 475, DateTimeKind.Local).AddTicks(486), "Aric Dibbert", "Zoie Wyman", new Guid("f0ae5df0-206e-29a8-2536-c525d2648e58"), "820-417-7984 x1240" },
                    { new Guid("9fa29937-f0ec-bc35-e300-156b89cafed1"), "4119 Medhurst Drives", new DateTime(2022, 8, 22, 22, 2, 56, 61, DateTimeKind.Local).AddTicks(280), "Itzel Johns", "Eliza VonRueden", new Guid("e0e8e101-53d0-90d5-0cdc-71bcc8589b72"), "868-310-7235 x17342" },
                    { new Guid("4f97da60-e073-9bcf-1344-278f499e19bf"), "5113 Iva Villages", new DateTime(2022, 8, 29, 5, 2, 20, 373, DateTimeKind.Local).AddTicks(5514), "Kathryn Ward", "Dorris Wilderman", new Guid("300842b6-f73f-6c1e-40e6-29f8f9b3ae8e"), "728-633-8795" },
                    { new Guid("71e8bff3-6db1-83ae-c578-4a857b88bd14"), "787 Dante Trafficway", new DateTime(2022, 8, 27, 13, 43, 49, 619, DateTimeKind.Local).AddTicks(5827), "Alexandre Bergnaum", "Michel Murphy", new Guid("300842b6-f73f-6c1e-40e6-29f8f9b3ae8e"), "497-444-4522" },
                    { new Guid("c73d2854-a2b8-0e9d-72ba-5b35f7627050"), "81673 Edward Landing", new DateTime(2022, 8, 20, 7, 38, 55, 544, DateTimeKind.Local).AddTicks(2816), "Misty Dooley", "Karina Metz", new Guid("300842b6-f73f-6c1e-40e6-29f8f9b3ae8e"), "691-560-8033 x64692" },
                    { new Guid("4e855dfc-552e-7ba6-2ae5-559f3a3aa535"), "1845 Lynch Cove", new DateTime(2022, 8, 30, 7, 24, 15, 585, DateTimeKind.Local).AddTicks(1837), "Tiffany Nicolas", "Gloria Littel", new Guid("693af881-eddb-a025-96d5-b187f6c8af90"), "582.248.9257 x8292" },
                    { new Guid("4f8da9b7-6dfa-7869-5def-de318d9efe95"), "4440 Aufderhar Mission", new DateTime(2022, 8, 28, 20, 26, 17, 240, DateTimeKind.Local).AddTicks(945), "Carey Dietrich", "Dion Glover", new Guid("693af881-eddb-a025-96d5-b187f6c8af90"), "(468) 687-0837" },
                    { new Guid("09e1f6a4-1d91-b141-a426-b040be26397a"), "95127 Considine Bypass", new DateTime(2022, 8, 15, 13, 43, 3, 135, DateTimeKind.Local).AddTicks(9080), "Vincent Kunze", "Camila Homenick", new Guid("327f83fe-bdcc-8f1d-eb76-2531d70a3cd2"), "563-389-8812" },
                    { new Guid("0521bb1e-a48d-fa47-0a04-d039339c6c61"), "3877 Fay Wall", new DateTime(2022, 8, 20, 21, 45, 17, 287, DateTimeKind.Local).AddTicks(4502), "Mauricio Swaniawski", "Jacklyn Green", new Guid("4ce2fc23-4a5b-97c3-5a39-3c29dd29d03a"), "644-283-8938 x04572" },
                    { new Guid("e9dbcd27-0c24-2d4f-2b24-ac0ef0175ca6"), "1984 Stroman Shore", new DateTime(2022, 8, 12, 22, 15, 58, 175, DateTimeKind.Local).AddTicks(1526), "Kaela McClure", "Erling Bogisich", new Guid("4b5810ab-d617-cc88-d764-ba900e5caa32"), "(869) 569-7660 x462" },
                    { new Guid("7de35c6c-6128-572a-485c-2f46c1c85273"), "909 Amanda Manor", new DateTime(2022, 8, 9, 7, 8, 38, 436, DateTimeKind.Local).AddTicks(9773), "Cali Dooley", "Norbert Rosenbaum", new Guid("4b5810ab-d617-cc88-d764-ba900e5caa32"), "870-933-4086 x2769" },
                    { new Guid("dd670812-e9d3-41af-b570-ca87596954d3"), "659 Rau Islands", new DateTime(2022, 8, 4, 21, 22, 31, 651, DateTimeKind.Local).AddTicks(6463), "Sebastian Aufderhar", "Zella Boehm", new Guid("4b5810ab-d617-cc88-d764-ba900e5caa32"), "589-938-3946 x662" },
                    { new Guid("e8381bd8-4ee0-e69c-97d6-efb60fc6c3b5"), "7754 Wendell Islands", new DateTime(2022, 8, 30, 18, 48, 3, 470, DateTimeKind.Local).AddTicks(4103), "Garrison Vandervort", "Emma Bergnaum", new Guid("d3896182-174d-b8ba-35bf-cdfcf4083146"), "(656) 256-3683 x7315" },
                    { new Guid("06810705-56c5-2841-08a0-b6a9fb65e25e"), "6066 Hahn Trace", new DateTime(2022, 7, 27, 5, 59, 33, 574, DateTimeKind.Local).AddTicks(7661), "Marisol Hintz", "Loraine Hudson", new Guid("1525b69a-80b7-3c07-ab8b-8950d1b491e9"), "1-667-550-6437" }
                });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "CreateDate", "NamePart", "NameStaff", "StaffId" },
                values: new object[,]
                {
                    { new Guid("0623c3d4-53eb-3ccd-d696-b7662e4db573"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nels_Terry", "Judy Mertz", new Guid("d7f2b07c-fd6a-e29d-27dc-243cfa1c228a") },
                    { new Guid("7d7ee00b-83b4-083a-1558-4729605002ab"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Skye.Aufderhar44", "Tiffany Nicolas", new Guid("1de1b04f-d06a-9197-2e58-d0353aa140cc") },
                    { new Guid("2b7e7d26-bf34-d80d-6d88-585cb213092b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rowena_OKon94", "Garrison Vandervort", new Guid("45a88052-16c9-e615-393c-9aaf2b5b802d") },
                    { new Guid("7e76fc2c-544e-57dc-e49a-b29de1f4bacf"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Elaina58", "Marisol Hintz", new Guid("45a88052-16c9-e615-393c-9aaf2b5b802d") },
                    { new Guid("07e3de08-45fa-909b-13bd-6e1a1e9580a2"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sabryna89", "Misty Dooley", new Guid("45a88052-16c9-e615-393c-9aaf2b5b802d") },
                    { new Guid("3d7c4b0f-93be-4373-898e-c6acfc2da0e1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gussie96", "Curtis Baumbach", new Guid("45a88052-16c9-e615-393c-9aaf2b5b802d") },
                    { new Guid("d1fbe8ad-acbf-578b-4ff2-87a5c660627f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Clement78", "Cyrus Pfannerstill", new Guid("ecd43c2c-bd7c-4206-55fe-8dd10590a391") },
                    { new Guid("8f03bd8b-68ba-aed6-9d9c-487b351b5412"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Katelyn_Mann", "Itzel Johns", new Guid("1576ef56-f34c-f0a0-ad29-887b032cc059") },
                    { new Guid("f31004d8-eb8c-0902-5e33-3e6641005012"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Clyde_Spencer72", "Peggie Wuckert", new Guid("27dc472c-0295-1a8f-9fa6-2bcf1dea256a") },
                    { new Guid("39fed7d2-bcd4-077e-ddfc-9f7ae48b58e9"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lyda91", "Louisa Wisozk", new Guid("75e8b596-b7e8-1b04-089d-6b72b4dca91c") },
                    { new Guid("846d2bb8-f6f4-b2c3-f9cb-1b43a308f334"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Richmond.Armstrong", "Myrtis Douglas", new Guid("8c4012dc-78de-45ae-65e9-f3916dbd8e68") },
                    { new Guid("b1aed86a-c94a-f86c-b62a-6eace9752f28"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jean50", "Bell Schamberger", new Guid("8c4012dc-78de-45ae-65e9-f3916dbd8e68") },
                    { new Guid("7eb3b38c-0562-b033-515f-88c6ced4332c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Melvin_Crona", "Gillian Prosacco", new Guid("8c4012dc-78de-45ae-65e9-f3916dbd8e68") },
                    { new Guid("b17a50fd-acd6-d63a-df0c-b3200f78ab0e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Destinee.Balistreri29", "Hollie Lind", new Guid("7e1a4c29-fe7a-14a2-fef4-178c7e5a24c9") },
                    { new Guid("4c437910-d035-873d-1ac0-11631e00fc92"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jeremy45", "Cynthia Effertz", new Guid("7e1a4c29-fe7a-14a2-fef4-178c7e5a24c9") },
                    { new Guid("9f18aef8-88b5-39c1-7217-925fd8d810ba"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kenna50", "Enos Cassin", new Guid("1bbda07c-89f1-f4ef-607a-1977a8168074") },
                    { new Guid("40d5f042-8c2f-57e6-b324-1589000c8886"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alexys_Trantow78", "Carey Dietrich", new Guid("3f139fa5-875a-33d2-aabd-582fd2ebb942") },
                    { new Guid("3e3e3448-c127-755f-7948-7ab0ea298659"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mikayla.Marquardt93", "Salvador Dickinson", new Guid("c03695a0-d0ba-cd7d-c8b1-b1fce868607d") },
                    { new Guid("9f26a5d9-e4b5-6ee8-5cd6-7279b3eb5f0a"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lyda91", "Roscoe Boyer", new Guid("0775e4c9-5f72-f699-ce46-2e1834fe6039") },
                    { new Guid("e7cbfd7f-abe7-9e1a-5541-0460102d3b5c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Connie_Miller54", "Gillian Prosacco", new Guid("5b131988-4db4-058e-ae63-231e09c967e1") },
                    { new Guid("3c511e3a-43b4-2de1-0d8c-fe2e7edccbee"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lyda91", "Annetta Borer", new Guid("9debb735-9d76-161c-ab78-304e42c55a45") },
                    { new Guid("19c9834a-f210-5081-58d1-498ed2a89d48"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jameson.Dietrich", "Rubie Toy", new Guid("9debb735-9d76-161c-ab78-304e42c55a45") },
                    { new Guid("3257b4ba-5fc0-e0df-4d2d-f4b228e9ac88"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Althea10", "Tiffany Nicolas", new Guid("5ad907d8-d545-dfcb-eca4-f41d31066708") },
                    { new Guid("d12c1603-ffa0-41af-0a16-6f10dfee3396"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Katelyn_Mann", "Dortha Prosacco", new Guid("5ad907d8-d545-dfcb-eca4-f41d31066708") },
                    { new Guid("24f536ff-2257-47dc-fa46-15e593d2e40f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eugene.Kihn50", "Cali Dooley", new Guid("1557fab4-7e0b-7c4d-9a64-daa473306434") },
                    { new Guid("bab44829-2c30-7b6c-2471-5b9618905b64"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Darron_Olson", "Garrison Vandervort", new Guid("2c76d6fc-edda-4033-40a3-21df54fe4536") },
                    { new Guid("91f85c06-7c06-8d12-073a-f3d5f9a397e6"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jameson.Dietrich", "Annetta Borer", new Guid("eef44370-6d29-b1fb-25ca-07b26e644236") },
                    { new Guid("2f1420aa-8f7e-eab6-15d2-699f1d7a8407"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gussie96", "Roslyn McGlynn", new Guid("0a90722b-4af9-ea37-eaf1-eb75cb4b1dbf") },
                    { new Guid("2b4e74a1-82a3-9769-9f0c-66a7b062f546"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sabryna89", "Sebastian Aufderhar", new Guid("0a90722b-4af9-ea37-eaf1-eb75cb4b1dbf") },
                    { new Guid("0d946eb1-4efc-8152-30e9-92f1c78bd59f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Shakira.Krajcik", "Dangelo Hyatt", new Guid("fab5d732-c6f1-d7c5-79d3-3c5809b46e0a") },
                    { new Guid("1d465b1f-808a-ab40-619e-a00104cf6fda"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jacquelyn_Koepp", "Louisa Wisozk", new Guid("fab5d732-c6f1-d7c5-79d3-3c5809b46e0a") },
                    { new Guid("e882674d-64e6-9206-9663-a19447030f30"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Glen_Doyle0", "Nat Thompson", new Guid("f071a7f8-18d3-181b-71c7-11e6bef00685") },
                    { new Guid("78421bd6-9784-b36c-30e5-5c8dc18793e3"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Luna83", "Vincent Kunze", new Guid("f071a7f8-18d3-181b-71c7-11e6bef00685") },
                    { new Guid("1609cd27-5c3c-819a-48fd-54fee970ed71"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lyda91", "Dangelo Hyatt", new Guid("f071a7f8-18d3-181b-71c7-11e6bef00685") },
                    { new Guid("0da52614-1535-924a-7677-8a03dc649c6e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nia.Wehner7", "Vincent Kunze", new Guid("75b46c44-9b11-e0ef-58ee-c2efaa0ce6ef") },
                    { new Guid("731428c4-bec7-ad4d-da48-ffbec51dc64f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Richmond.Armstrong", "Cyrus Pfannerstill", new Guid("ee7a66d8-3414-cfa9-eff1-9d5084272344") },
                    { new Guid("e63449ad-5be6-8bd8-a93b-7319e6f802ec"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Webster.Koss99", "Dayana Wiza", new Guid("77392d92-a0f5-f311-377b-3cea2facf3eb") },
                    { new Guid("10d303d4-8923-1388-486f-ac95686343db"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Abbigail.Deckow", "Tiffany Nicolas", new Guid("ee7a66d8-3414-cfa9-eff1-9d5084272344") },
                    { new Guid("1322bf58-a6a4-1a50-089b-6693d5a14179"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mikayla.Marquardt93", "Rubie Toy", new Guid("18c0e958-9922-5d45-d175-8c15fb17292e") },
                    { new Guid("3fda1ea6-9435-8df2-c112-d0394e4df5bb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Samson88", "Misty Dooley", new Guid("9fa29937-f0ec-bc35-e300-156b89cafed1") },
                    { new Guid("95353346-39bd-7592-cccd-1dc834368a7e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kenna50", "Bell Schamberger", new Guid("4f97da60-e073-9bcf-1344-278f499e19bf") },
                    { new Guid("bd506805-036d-3671-0538-d8d12d694fec"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Darron_Olson", "Amy Robel", new Guid("71e8bff3-6db1-83ae-c578-4a857b88bd14") },
                    { new Guid("e0c43a00-c720-7bd2-9b0e-9f57ee8da68e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Clyde_Spencer72", "Nat Thompson", new Guid("4e855dfc-552e-7ba6-2ae5-559f3a3aa535") },
                    { new Guid("34d5cb8f-035b-0b10-c2f3-521f70d30137"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nia.Wehner7", "Kennedi Christiansen", new Guid("4e855dfc-552e-7ba6-2ae5-559f3a3aa535") },
                    { new Guid("5e8b02ca-a523-2492-6be1-aec1277bb786"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Merle.Lesch", "Kathryn Ward", new Guid("0521bb1e-a48d-fa47-0a04-d039339c6c61") },
                    { new Guid("f02e3512-5268-2093-0ddd-f7b66938399a"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Skye.Aufderhar44", "Sebastian Aufderhar", new Guid("0521bb1e-a48d-fa47-0a04-d039339c6c61") },
                    { new Guid("1e189349-f401-d4f2-45cc-3085131bb79b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Darron_Olson", "Newell Gislason", new Guid("0521bb1e-a48d-fa47-0a04-d039339c6c61") },
                    { new Guid("3a3addc1-0a36-fd88-b60a-f719f9dfdf65"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Darron_Olson", "Cyrus Pfannerstill", new Guid("7de35c6c-6128-572a-485c-2f46c1c85273") },
                    { new Guid("dee28f49-7426-6ea7-a8c5-f88868d5eb19"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Clarissa40", "Newell Gislason", new Guid("18c0e958-9922-5d45-d175-8c15fb17292e") },
                    { new Guid("0d713589-0c14-4551-b453-bedf591bb133"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nasir68", "Dangelo Hyatt", new Guid("06810705-56c5-2841-08a0-b6a9fb65e25e") }
                });

            migrationBuilder.InsertData(
                table: "OrderDetails",
                columns: new[] { "Id", "Amount", "OrderId", "ProductId" },
                values: new object[,]
                {
                    { new Guid("7db59c9f-6709-6a5d-031b-04a64c438922"), 69, new Guid("0623c3d4-53eb-3ccd-d696-b7662e4db573"), new Guid("7d465ccc-87f8-66d1-4732-5c65aad55951") },
                    { new Guid("236176e7-0359-0514-669d-b9817e61a8b1"), 69, new Guid("d12c1603-ffa0-41af-0a16-6f10dfee3396"), new Guid("aee312cf-1f22-b37d-9419-05b6f3ee7d88") },
                    { new Guid("2379187b-d6a7-fac0-7fa7-1b9a5ff3e9b0"), 69, new Guid("d12c1603-ffa0-41af-0a16-6f10dfee3396"), new Guid("81c6bfcc-641c-85b3-2cf2-f43daa7ee649") },
                    { new Guid("2ce8c6fa-6560-7908-a8fc-5e91049dd356"), 69, new Guid("7d7ee00b-83b4-083a-1558-4729605002ab"), new Guid("ef5f5eea-54c0-ae5f-388d-b4670bb13f2b") },
                    { new Guid("e8f6f26a-e29c-4d1b-714f-6bfb49295062"), 69, new Guid("7d7ee00b-83b4-083a-1558-4729605002ab"), new Guid("20f11321-afa2-7f99-ec6a-48ad50e286b2") },
                    { new Guid("59e49247-2dc2-ec4f-5799-74e0e328f375"), 69, new Guid("2b7e7d26-bf34-d80d-6d88-585cb213092b"), new Guid("a4883564-6048-79b5-4548-c8292f81db2b") },
                    { new Guid("27ae558b-4fe8-189e-f57e-3b77b989825c"), 69, new Guid("7e76fc2c-544e-57dc-e49a-b29de1f4bacf"), new Guid("5bc9738a-3586-e36c-0b33-8cac50e7a114") },
                    { new Guid("25a810ce-393f-47ed-2a4d-3046ae6048f6"), 69, new Guid("7e76fc2c-544e-57dc-e49a-b29de1f4bacf"), new Guid("cb8581e7-5051-d676-3c6e-a89cca1642a9") },
                    { new Guid("0d98c453-d8e3-2a72-a378-5e44e8892f38"), 69, new Guid("f31004d8-eb8c-0902-5e33-3e6641005012"), new Guid("b22ac294-c14f-f51f-6173-59387aa0eb22") },
                    { new Guid("86bbc070-3ea3-d37f-0e46-23f15e16ed9a"), 69, new Guid("f31004d8-eb8c-0902-5e33-3e6641005012"), new Guid("323fc2b0-3e52-6e9d-0f89-4345c445f907") },
                    { new Guid("8a50bd00-096b-0b07-b5e3-ab0abfcacb5f"), 69, new Guid("7eb3b38c-0562-b033-515f-88c6ced4332c"), new Guid("e2eaf27d-9cd2-6dbd-a73b-549aaaea146d") },
                    { new Guid("151eee9b-8209-c0c8-f8ea-fca989402c1f"), 69, new Guid("7eb3b38c-0562-b033-515f-88c6ced4332c"), new Guid("7a53feba-b9b3-a03e-2ea9-35a4c32cf77d") },
                    { new Guid("a18d2a77-c030-da29-3f29-88f461281b3f"), 69, new Guid("7eb3b38c-0562-b033-515f-88c6ced4332c"), new Guid("0b9b5165-5f88-d78b-ce2a-8c75a6cc64b3") },
                    { new Guid("a92626f3-64e3-0555-be23-6439e6ba7747"), 69, new Guid("40d5f042-8c2f-57e6-b324-1589000c8886"), new Guid("ece316c8-3378-85d5-7cd4-37249ad57554") },
                    { new Guid("5bf3c083-771c-0372-49bb-43dda95f1aec"), 69, new Guid("40d5f042-8c2f-57e6-b324-1589000c8886"), new Guid("0e8d34ce-6ef8-bfad-a75b-886f3b1a93f0") },
                    { new Guid("10e888bf-2345-cace-ef8e-8f1be73d4564"), 69, new Guid("40d5f042-8c2f-57e6-b324-1589000c8886"), new Guid("9b1a54bf-87c2-e2e9-f6e4-9740a1fe41c1") },
                    { new Guid("c640e266-22b4-9913-2cdd-dc9d8a591d45"), 69, new Guid("3e3e3448-c127-755f-7948-7ab0ea298659"), new Guid("b1f37e21-2d19-d222-1e0f-d23644ac3216") },
                    { new Guid("faafb5d7-cda8-8cce-af1a-bca66ce7c4a8"), 69, new Guid("e7cbfd7f-abe7-9e1a-5541-0460102d3b5c"), new Guid("9b1a54bf-87c2-e2e9-f6e4-9740a1fe41c1") },
                    { new Guid("e019bd2e-27b0-8a62-a9ec-15b0e133c9f4"), 69, new Guid("e7cbfd7f-abe7-9e1a-5541-0460102d3b5c"), new Guid("81c6bfcc-641c-85b3-2cf2-f43daa7ee649") },
                    { new Guid("84fe903d-0362-a232-d3b2-21e31a251f0a"), 69, new Guid("3c511e3a-43b4-2de1-0d8c-fe2e7edccbee"), new Guid("0b9b5165-5f88-d78b-ce2a-8c75a6cc64b3") },
                    { new Guid("7dfce24a-b72a-49ca-7f90-fa7c1158f323"), 69, new Guid("3c511e3a-43b4-2de1-0d8c-fe2e7edccbee"), new Guid("0b9b5165-5f88-d78b-ce2a-8c75a6cc64b3") },
                    { new Guid("1ce0f278-650d-e9ac-d232-fb5d45c929b5"), 69, new Guid("3c511e3a-43b4-2de1-0d8c-fe2e7edccbee"), new Guid("c803b517-9a6e-3367-751a-a6bf4313dcdb") },
                    { new Guid("8bd64741-6ce7-4160-1e30-15d4d9581cfc"), 69, new Guid("24f536ff-2257-47dc-fa46-15e593d2e40f"), new Guid("9b1a54bf-87c2-e2e9-f6e4-9740a1fe41c1") },
                    { new Guid("60be6b88-025f-7a5b-6d2b-b091448e7afb"), 69, new Guid("bab44829-2c30-7b6c-2471-5b9618905b64"), new Guid("7e2f9e6b-c757-3869-63ef-97ff09d0cd20") },
                    { new Guid("9e475305-22ce-f2ca-cad0-22cdf849e603"), 69, new Guid("bab44829-2c30-7b6c-2471-5b9618905b64"), new Guid("cb8581e7-5051-d676-3c6e-a89cca1642a9") },
                    { new Guid("a7015bf6-f4aa-33d8-cbc9-abc0741d8ec4"), 69, new Guid("bab44829-2c30-7b6c-2471-5b9618905b64"), new Guid("ece316c8-3378-85d5-7cd4-37249ad57554") },
                    { new Guid("3902bbe1-c4b7-76ad-ae7c-007f2c2f7aa1"), 69, new Guid("2f1420aa-8f7e-eab6-15d2-699f1d7a8407"), new Guid("7e5ddb00-98bd-fc1a-c380-ec6e90db3c4f") },
                    { new Guid("4dc2ddff-fa13-846d-a507-8241d8589ee6"), 69, new Guid("2b4e74a1-82a3-9769-9f0c-66a7b062f546"), new Guid("6efdc076-1252-bad3-7a9c-be5bfda6156d") },
                    { new Guid("ad4e37ad-7482-9fba-5c9f-63fa3f89b842"), 69, new Guid("2b4e74a1-82a3-9769-9f0c-66a7b062f546"), new Guid("323fc2b0-3e52-6e9d-0f89-4345c445f907") },
                    { new Guid("11222f37-1d70-0765-c165-43a5bce8ebdf"), 69, new Guid("0d946eb1-4efc-8152-30e9-92f1c78bd59f"), new Guid("517145b8-0f50-fa45-f3f7-bae62a5ffd13") },
                    { new Guid("0b2fa32f-e811-b34d-c46e-db2915f13bfb"), 69, new Guid("1d465b1f-808a-ab40-619e-a00104cf6fda"), new Guid("5b6e563b-2f59-bf16-89dc-a0647f95edd2") },
                    { new Guid("fbea7e2d-ebdf-38b0-e502-1869eac44d0c"), 69, new Guid("e882674d-64e6-9206-9663-a19447030f30"), new Guid("b461ef5d-b6bb-5863-0790-d710877fec22") },
                    { new Guid("98da886e-3f08-b1dd-f973-388c69ede945"), 69, new Guid("e882674d-64e6-9206-9663-a19447030f30"), new Guid("0e8d34ce-6ef8-bfad-a75b-886f3b1a93f0") },
                    { new Guid("fb17e24c-7195-2519-2cf6-c9d16bfc6c42"), 69, new Guid("e882674d-64e6-9206-9663-a19447030f30"), new Guid("992aae9b-bc81-3de0-3dd2-7269635706c9") },
                    { new Guid("43e11e42-34e9-7449-7bad-ed2a97105bfe"), 69, new Guid("1609cd27-5c3c-819a-48fd-54fee970ed71"), new Guid("a4883564-6048-79b5-4548-c8292f81db2b") },
                    { new Guid("64567513-bbe0-5c8d-16cf-b636f5f01cac"), 69, new Guid("1609cd27-5c3c-819a-48fd-54fee970ed71"), new Guid("b461ef5d-b6bb-5863-0790-d710877fec22") },
                    { new Guid("8f1fb514-2219-5dd6-a12b-b8a48076139f"), 69, new Guid("e63449ad-5be6-8bd8-a93b-7319e6f802ec"), new Guid("676421b7-3c21-179e-122a-099feb545ec3") },
                    { new Guid("afdcfef7-5b43-9c15-4e13-e46be4c8d621"), 69, new Guid("731428c4-bec7-ad4d-da48-ffbec51dc64f"), new Guid("81c6bfcc-641c-85b3-2cf2-f43daa7ee649") },
                    { new Guid("68201f6d-8f6a-ddf9-923f-f2407841d020"), 69, new Guid("1322bf58-a6a4-1a50-089b-6693d5a14179"), new Guid("c23b8ada-45ec-ec83-4709-664395811ff4") },
                    { new Guid("9d5376ce-e8a2-08ef-388e-4c1f1b7ea2dd"), 69, new Guid("1322bf58-a6a4-1a50-089b-6693d5a14179"), new Guid("ece316c8-3378-85d5-7cd4-37249ad57554") },
                    { new Guid("a7547256-1494-d8b9-a9e4-6ac45d918b62"), 69, new Guid("1322bf58-a6a4-1a50-089b-6693d5a14179"), new Guid("5bc9738a-3586-e36c-0b33-8cac50e7a114") },
                    { new Guid("116951f0-07e9-53d3-ea8e-f134e3ac5a21"), 69, new Guid("3fda1ea6-9435-8df2-c112-d0394e4df5bb"), new Guid("cb8581e7-5051-d676-3c6e-a89cca1642a9") },
                    { new Guid("fd9a54ab-a3e7-0b62-71a0-1e174de9b98a"), 69, new Guid("3fda1ea6-9435-8df2-c112-d0394e4df5bb"), new Guid("4f8b815a-4c20-a93f-8f0f-04d87947124b") },
                    { new Guid("26abac0b-7148-5955-ef3a-d927e5c6e7a0"), 69, new Guid("e0c43a00-c720-7bd2-9b0e-9f57ee8da68e"), new Guid("cfebbbe6-d9e2-6900-1a92-955d503da84b") },
                    { new Guid("d639440f-adaa-3853-1e11-909fe20b12d3"), 69, new Guid("34d5cb8f-035b-0b10-c2f3-521f70d30137"), new Guid("5bc9738a-3586-e36c-0b33-8cac50e7a114") },
                    { new Guid("86c918d9-d7f2-ddb7-ea30-7d5931a840fd"), 69, new Guid("5e8b02ca-a523-2492-6be1-aec1277bb786"), new Guid("cfebbbe6-d9e2-6900-1a92-955d503da84b") },
                    { new Guid("46d64dc7-e0b2-f690-f19d-b58ef9cc40b5"), 69, new Guid("5e8b02ca-a523-2492-6be1-aec1277bb786"), new Guid("bb909bf9-f076-53ef-44ff-495aca31c29a") },
                    { new Guid("f4bc99d0-1937-feb8-7c59-8c255206c6f5"), 69, new Guid("bab44829-2c30-7b6c-2471-5b9618905b64"), new Guid("a4883564-6048-79b5-4548-c8292f81db2b") },
                    { new Guid("91b63f7e-c176-ca59-d305-7806d9e0d364"), 69, new Guid("1322bf58-a6a4-1a50-089b-6693d5a14179"), new Guid("cb8581e7-5051-d676-3c6e-a89cca1642a9") },
                    { new Guid("5331a275-4354-fa85-b69a-f398763f3336"), 69, new Guid("0d713589-0c14-4551-b453-bedf591bb133"), new Guid("a79aa2ed-5ff4-2c65-725e-368a13aad6c5") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("0b2fa32f-e811-b34d-c46e-db2915f13bfb"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("0d98c453-d8e3-2a72-a378-5e44e8892f38"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("10e888bf-2345-cace-ef8e-8f1be73d4564"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("11222f37-1d70-0765-c165-43a5bce8ebdf"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("116951f0-07e9-53d3-ea8e-f134e3ac5a21"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("151eee9b-8209-c0c8-f8ea-fca989402c1f"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("1ce0f278-650d-e9ac-d232-fb5d45c929b5"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("236176e7-0359-0514-669d-b9817e61a8b1"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("2379187b-d6a7-fac0-7fa7-1b9a5ff3e9b0"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("25a810ce-393f-47ed-2a4d-3046ae6048f6"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("26abac0b-7148-5955-ef3a-d927e5c6e7a0"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("27ae558b-4fe8-189e-f57e-3b77b989825c"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("2ce8c6fa-6560-7908-a8fc-5e91049dd356"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("3902bbe1-c4b7-76ad-ae7c-007f2c2f7aa1"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("43e11e42-34e9-7449-7bad-ed2a97105bfe"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("46d64dc7-e0b2-f690-f19d-b58ef9cc40b5"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("4dc2ddff-fa13-846d-a507-8241d8589ee6"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("5331a275-4354-fa85-b69a-f398763f3336"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("59e49247-2dc2-ec4f-5799-74e0e328f375"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("5bf3c083-771c-0372-49bb-43dda95f1aec"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("60be6b88-025f-7a5b-6d2b-b091448e7afb"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("64567513-bbe0-5c8d-16cf-b636f5f01cac"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("68201f6d-8f6a-ddf9-923f-f2407841d020"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("7db59c9f-6709-6a5d-031b-04a64c438922"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("7dfce24a-b72a-49ca-7f90-fa7c1158f323"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("84fe903d-0362-a232-d3b2-21e31a251f0a"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("86bbc070-3ea3-d37f-0e46-23f15e16ed9a"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("86c918d9-d7f2-ddb7-ea30-7d5931a840fd"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("8a50bd00-096b-0b07-b5e3-ab0abfcacb5f"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("8bd64741-6ce7-4160-1e30-15d4d9581cfc"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("8f1fb514-2219-5dd6-a12b-b8a48076139f"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("91b63f7e-c176-ca59-d305-7806d9e0d364"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("98da886e-3f08-b1dd-f973-388c69ede945"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("9d5376ce-e8a2-08ef-388e-4c1f1b7ea2dd"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("9e475305-22ce-f2ca-cad0-22cdf849e603"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("a18d2a77-c030-da29-3f29-88f461281b3f"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("a7015bf6-f4aa-33d8-cbc9-abc0741d8ec4"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("a7547256-1494-d8b9-a9e4-6ac45d918b62"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("a92626f3-64e3-0555-be23-6439e6ba7747"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("ad4e37ad-7482-9fba-5c9f-63fa3f89b842"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("afdcfef7-5b43-9c15-4e13-e46be4c8d621"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("c640e266-22b4-9913-2cdd-dc9d8a591d45"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("d639440f-adaa-3853-1e11-909fe20b12d3"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("e019bd2e-27b0-8a62-a9ec-15b0e133c9f4"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("e8f6f26a-e29c-4d1b-714f-6bfb49295062"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("f4bc99d0-1937-feb8-7c59-8c255206c6f5"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("faafb5d7-cda8-8cce-af1a-bca66ce7c4a8"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("fb17e24c-7195-2519-2cf6-c9d16bfc6c42"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("fbea7e2d-ebdf-38b0-e502-1869eac44d0c"));

            migrationBuilder.DeleteData(
                table: "OrderDetails",
                keyColumn: "Id",
                keyValue: new Guid("fd9a54ab-a3e7-0b62-71a0-1e174de9b98a"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("07e3de08-45fa-909b-13bd-6e1a1e9580a2"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("0da52614-1535-924a-7677-8a03dc649c6e"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("10d303d4-8923-1388-486f-ac95686343db"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("19c9834a-f210-5081-58d1-498ed2a89d48"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("1e189349-f401-d4f2-45cc-3085131bb79b"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("3257b4ba-5fc0-e0df-4d2d-f4b228e9ac88"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("39fed7d2-bcd4-077e-ddfc-9f7ae48b58e9"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("3a3addc1-0a36-fd88-b60a-f719f9dfdf65"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("3d7c4b0f-93be-4373-898e-c6acfc2da0e1"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("4c437910-d035-873d-1ac0-11631e00fc92"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("78421bd6-9784-b36c-30e5-5c8dc18793e3"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("846d2bb8-f6f4-b2c3-f9cb-1b43a308f334"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("8f03bd8b-68ba-aed6-9d9c-487b351b5412"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("91f85c06-7c06-8d12-073a-f3d5f9a397e6"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("95353346-39bd-7592-cccd-1dc834368a7e"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("9f18aef8-88b5-39c1-7217-925fd8d810ba"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("9f26a5d9-e4b5-6ee8-5cd6-7279b3eb5f0a"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("b17a50fd-acd6-d63a-df0c-b3200f78ab0e"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("b1aed86a-c94a-f86c-b62a-6eace9752f28"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("bd506805-036d-3671-0538-d8d12d694fec"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("d1fbe8ad-acbf-578b-4ff2-87a5c660627f"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("dee28f49-7426-6ea7-a8c5-f88868d5eb19"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("f02e3512-5268-2093-0ddd-f7b66938399a"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("08671406-da93-ae29-788e-e0a99a5e373a"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("0fc3151e-68e7-828b-cc00-c64929ddb0b1"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("130e3904-2d03-8992-e3f4-90d306aedeb0"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("1cd04874-964b-8299-4192-e6a3f58978a0"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("21126ff6-e1ce-29af-f931-854208bbffd9"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("22103fbf-32c2-d579-e86b-c0b03dc1f0f9"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("4b62c635-b681-1e61-863a-5275afc8eb89"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("52e36235-98c1-61fb-9cdc-69a66dc91794"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("575897e3-a571-24dc-7e44-bca282519d3e"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("696630a3-f998-c36e-2b07-13dc562114d3"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("6ca3a999-d370-cfe1-ea26-7d22da61b6aa"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("79feb4f7-8bed-934f-4aee-5ec70f7da364"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("84a6d2a5-56c2-677c-ae5c-0ef72b7df197"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("93a3725d-c055-ddbd-08fd-7d9ba167693f"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("9f1e6777-de9f-936b-bff3-27c83e69b096"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("a2a96c5c-bf9c-cf1e-5b9d-2a2b93847162"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("c651c8cf-fb12-6264-9186-0bfb763e3581"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("d2379191-7d30-efc7-bc9e-8bc9c312644e"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("deb3562d-0087-5e3e-9415-ea062df917e8"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("fc556c66-c305-e207-c7d1-307b80ea3e93"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("0234abd0-395e-31a9-db35-1a72cbefa280"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("2730253b-2607-c546-2d1d-0943043dc960"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("29bb4c3e-e60a-ec96-7dcb-d2807990e67c"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("2a287412-7dc6-1cc7-d695-abf913f0b177"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("2a33b051-84a4-a527-1cb1-2f4b057251dc"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("4e7bc7a7-9a78-a7a0-6377-9f8ad2dd11a5"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("4f0cb75d-51c9-28ef-e63c-f8c0774d7d07"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("5f6eb272-e57a-3232-d795-d6b63c544b8c"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("6610b40d-8d2b-b7bd-5c77-f601f38ce94e"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("737162ae-221d-1a0c-a009-f3b527a7b5ac"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("8149b3fb-b0da-aa96-2bd3-f50c89ae21b4"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("8cdbf83f-28f4-6be9-0f09-4d2f0d22686c"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("999c0db4-728d-73b1-8fda-217a30580f95"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("ac477294-5684-9685-1d3d-67f7165bff0c"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c3f318d6-7f4d-3918-ad52-fe829b0b253d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c84b0e4d-0a29-c2be-214b-16e5cbd819bd"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("d82e2668-b0dc-6ff3-bc59-80139142ce6f"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("e7f1a9e0-19d2-6370-b3d1-c68a7836200b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("f1c451ef-cd0c-6e8a-7577-9bdbcfbd618f"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("09e1f6a4-1d91-b141-a426-b040be26397a"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("1c5d8ef1-bc50-74b1-ac03-7bab477c0ec1"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("38a7e26a-4c55-334b-82cf-c322f31b5206"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("4f8da9b7-6dfa-7869-5def-de318d9efe95"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("501e5bf1-0a44-9d1d-481c-455371c5c8b1"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("602a59f3-6aae-3d93-15eb-e922f7448061"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("767704d1-46da-8d3b-a963-37b57a4f8e0a"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("7c385e28-8d6e-15ca-33c4-3c66415a1010"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("a3335abb-4b2f-1756-8e5c-cae6e3d628b9"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("c73d2854-a2b8-0e9d-72ba-5b35f7627050"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("dd670812-e9d3-41af-b570-ca87596954d3"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("dd8087a5-6ef5-7959-aa44-6580b8f8117c"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("e53814c3-7e6c-1db8-aadc-b1a5a2de463a"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("e6fcce4a-8829-86d0-9a1a-a7f2f85381fb"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("e8381bd8-4ee0-e69c-97d6-efb60fc6c3b5"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("e9dbcd27-0c24-2d4f-2b24-ac0ef0175ca6"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("fed24806-3a93-2c77-3377-383c163ae85d"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("01f9697b-ea02-8bcc-073a-0f817261a877"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("06a67d5f-888b-a6fe-ea33-dff4eee07f79"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("09c95b30-2b12-210b-5e83-d44143b8602c"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("0f72b503-5fff-42c5-da9c-a680dbc31894"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("2d9fcbea-cb98-48f0-be57-461a1dc5c683"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("3562eceb-3acf-e655-af30-73ce06e9f790"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("37e09732-c3a8-54b1-b96d-be24d899478f"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("460bbfbb-a698-695c-ebc9-f514a15297c5"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("4a3b8bd6-d35e-0142-830c-b21bda586218"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("621f153e-eaf5-1f41-24ad-2891085d7f0e"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("692a9301-d046-a62e-9270-5a00a56fe60a"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("87feca68-3256-44ca-8c8b-81db8b86c88c"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8b00dff9-8f45-6aab-326a-9c9b26bef747"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("8b2813fa-cb4a-a873-374a-15e8aba9e5d3"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("9865fa07-979f-63fa-e7f3-ecce7fc013d1"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("a021927a-0df3-ccaa-11a0-00d6c4e6648d"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("cf502e15-6283-ed7e-be55-967d980826bb"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("d18490b1-6c24-67f0-3078-6cab0d35bcb7"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("de9afa0b-03da-072f-5faa-6642d01f4072"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("e1835bbb-29b7-77ee-e3ff-8dff540ae0eb"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("0623c3d4-53eb-3ccd-d696-b7662e4db573"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("0d713589-0c14-4551-b453-bedf591bb133"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("0d946eb1-4efc-8152-30e9-92f1c78bd59f"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("1322bf58-a6a4-1a50-089b-6693d5a14179"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("1609cd27-5c3c-819a-48fd-54fee970ed71"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("1d465b1f-808a-ab40-619e-a00104cf6fda"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("24f536ff-2257-47dc-fa46-15e593d2e40f"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("2b4e74a1-82a3-9769-9f0c-66a7b062f546"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("2b7e7d26-bf34-d80d-6d88-585cb213092b"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("2f1420aa-8f7e-eab6-15d2-699f1d7a8407"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("34d5cb8f-035b-0b10-c2f3-521f70d30137"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("3c511e3a-43b4-2de1-0d8c-fe2e7edccbee"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("3e3e3448-c127-755f-7948-7ab0ea298659"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("3fda1ea6-9435-8df2-c112-d0394e4df5bb"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("40d5f042-8c2f-57e6-b324-1589000c8886"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("5e8b02ca-a523-2492-6be1-aec1277bb786"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("731428c4-bec7-ad4d-da48-ffbec51dc64f"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("7d7ee00b-83b4-083a-1558-4729605002ab"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("7e76fc2c-544e-57dc-e49a-b29de1f4bacf"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("7eb3b38c-0562-b033-515f-88c6ced4332c"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("bab44829-2c30-7b6c-2471-5b9618905b64"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("d12c1603-ffa0-41af-0a16-6f10dfee3396"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("e0c43a00-c720-7bd2-9b0e-9f57ee8da68e"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("e63449ad-5be6-8bd8-a93b-7319e6f802ec"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("e7cbfd7f-abe7-9e1a-5541-0460102d3b5c"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("e882674d-64e6-9206-9663-a19447030f30"));

            migrationBuilder.DeleteData(
                table: "Orders",
                keyColumn: "Id",
                keyValue: new Guid("f31004d8-eb8c-0902-5e33-3e6641005012"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("0117b9be-469a-ed5b-3788-99a8aa8989e3"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("0eebb743-07b8-f025-36e5-2823c748ed82"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("12272521-34b5-511a-73bf-bde7d179544f"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("327f83fe-bdcc-8f1d-eb76-2531d70a3cd2"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("89a2ce03-f644-a9c8-cb6d-bb02b9a8bdaa"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("ae165182-f74c-cf5e-e6b6-9546059c79ab"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("c8357dc2-8c20-f678-18ae-6acf0804a81a"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("d3896182-174d-b8ba-35bf-cdfcf4083146"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("de6d8034-3aec-3962-81b7-b8a37530f50b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("0b9b5165-5f88-d78b-ce2a-8c75a6cc64b3"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("0e8d34ce-6ef8-bfad-a75b-886f3b1a93f0"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("20f11321-afa2-7f99-ec6a-48ad50e286b2"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("323fc2b0-3e52-6e9d-0f89-4345c445f907"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("4f8b815a-4c20-a93f-8f0f-04d87947124b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("517145b8-0f50-fa45-f3f7-bae62a5ffd13"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("5b6e563b-2f59-bf16-89dc-a0647f95edd2"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("5bc9738a-3586-e36c-0b33-8cac50e7a114"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("676421b7-3c21-179e-122a-099feb545ec3"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("6efdc076-1252-bad3-7a9c-be5bfda6156d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("7a53feba-b9b3-a03e-2ea9-35a4c32cf77d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("7d465ccc-87f8-66d1-4732-5c65aad55951"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("7e2f9e6b-c757-3869-63ef-97ff09d0cd20"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("7e5ddb00-98bd-fc1a-c380-ec6e90db3c4f"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("81c6bfcc-641c-85b3-2cf2-f43daa7ee649"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("992aae9b-bc81-3de0-3dd2-7269635706c9"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("9b1a54bf-87c2-e2e9-f6e4-9740a1fe41c1"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("a4883564-6048-79b5-4548-c8292f81db2b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("a79aa2ed-5ff4-2c65-725e-368a13aad6c5"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("aee312cf-1f22-b37d-9419-05b6f3ee7d88"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("b1f37e21-2d19-d222-1e0f-d23644ac3216"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("b22ac294-c14f-f51f-6173-59387aa0eb22"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("b461ef5d-b6bb-5863-0790-d710877fec22"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("bb909bf9-f076-53ef-44ff-495aca31c29a"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c23b8ada-45ec-ec83-4709-664395811ff4"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("c803b517-9a6e-3367-751a-a6bf4313dcdb"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("cb8581e7-5051-d676-3c6e-a89cca1642a9"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("cfebbbe6-d9e2-6900-1a92-955d503da84b"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("e2eaf27d-9cd2-6dbd-a73b-549aaaea146d"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("ece316c8-3378-85d5-7cd4-37249ad57554"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("ef5f5eea-54c0-ae5f-388d-b4670bb13f2b"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("0775e4c9-5f72-f699-ce46-2e1834fe6039"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("1576ef56-f34c-f0a0-ad29-887b032cc059"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("1bbda07c-89f1-f4ef-607a-1977a8168074"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("4f97da60-e073-9bcf-1344-278f499e19bf"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("71e8bff3-6db1-83ae-c578-4a857b88bd14"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("75b46c44-9b11-e0ef-58ee-c2efaa0ce6ef"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("75e8b596-b7e8-1b04-089d-6b72b4dca91c"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("7de35c6c-6128-572a-485c-2f46c1c85273"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("7e1a4c29-fe7a-14a2-fef4-178c7e5a24c9"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("ecd43c2c-bd7c-4206-55fe-8dd10590a391"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("eef44370-6d29-b1fb-25ca-07b26e644236"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("1a9f0107-2b38-cf06-db18-c2d4cf03f652"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("2f0d38a8-231c-b74e-baaf-65cc0ede4ae6"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("300842b6-f73f-6c1e-40e6-29f8f9b3ae8e"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("3c780d06-9eee-bb51-99ec-2b5260d48927"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("edaef557-a7e0-4263-aab9-4693496804f8"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("0521bb1e-a48d-fa47-0a04-d039339c6c61"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("06810705-56c5-2841-08a0-b6a9fb65e25e"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("0a90722b-4af9-ea37-eaf1-eb75cb4b1dbf"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("1557fab4-7e0b-7c4d-9a64-daa473306434"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("18c0e958-9922-5d45-d175-8c15fb17292e"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("1de1b04f-d06a-9197-2e58-d0353aa140cc"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("27dc472c-0295-1a8f-9fa6-2bcf1dea256a"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("2c76d6fc-edda-4033-40a3-21df54fe4536"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("3f139fa5-875a-33d2-aabd-582fd2ebb942"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("45a88052-16c9-e615-393c-9aaf2b5b802d"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("4e855dfc-552e-7ba6-2ae5-559f3a3aa535"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("5ad907d8-d545-dfcb-eca4-f41d31066708"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("5b131988-4db4-058e-ae63-231e09c967e1"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("77392d92-a0f5-f311-377b-3cea2facf3eb"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("8c4012dc-78de-45ae-65e9-f3916dbd8e68"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("9debb735-9d76-161c-ab78-304e42c55a45"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("9fa29937-f0ec-bc35-e300-156b89cafed1"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("c03695a0-d0ba-cd7d-c8b1-b1fce868607d"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("d7f2b07c-fd6a-e29d-27dc-243cfa1c228a"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("ee7a66d8-3414-cfa9-eff1-9d5084272344"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("f071a7f8-18d3-181b-71c7-11e6bef00685"));

            migrationBuilder.DeleteData(
                table: "Staffs",
                keyColumn: "Id",
                keyValue: new Guid("fab5d732-c6f1-d7c5-79d3-3c5809b46e0a"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("026dd299-4389-0132-dc21-8f487bfcd58d"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("1525b69a-80b7-3c07-ab8b-8950d1b491e9"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("1a79cb28-a6b9-1c80-87ea-16ac3613b707"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("344b2122-0d80-5d59-c7bb-dd21523188ae"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("3f4e0c83-a151-703b-9794-f70d948147f4"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("4b5810ab-d617-cc88-d764-ba900e5caa32"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("4ce2fc23-4a5b-97c3-5a39-3c29dd29d03a"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("58b61a1b-8674-a6c8-372c-a69f65023fcf"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("62794e50-f1b4-84a4-9eb3-3e0f2165a111"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("693af881-eddb-a025-96d5-b187f6c8af90"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("77099fae-559e-3a16-4c8b-97521d98abbb"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("7b4c13c1-b403-fa9d-6828-206dbd83c851"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("9daee96b-95b1-d142-8c3a-84e7453f96e7"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("c4f19d18-7154-7f18-bce6-4ff52eb592f6"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("e0e8e101-53d0-90d5-0cdc-71bcc8589b72"));

            migrationBuilder.DeleteData(
                table: "Parts",
                keyColumn: "Id",
                keyValue: new Guid("f0ae5df0-206e-29a8-2536-c525d2648e58"));

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Orders");

            migrationBuilder.InsertData(
                table: "Parts",
                columns: new[] { "Id", "CreateDate", "Name", "NameLeader" },
                values: new object[,]
                {
                    { new Guid("8d1a5116-082e-786d-c51a-004842a4f743"), new DateTime(2022, 8, 22, 23, 37, 14, 90, DateTimeKind.Local).AddTicks(9728), "Zena6", "Elyssa55" },
                    { new Guid("45d697c4-dd92-c266-249d-7b2522d95405"), new DateTime(2022, 8, 12, 9, 30, 47, 44, DateTimeKind.Local).AddTicks(9965), "Shyanne_Legros", "Madison_Haley" },
                    { new Guid("802c4c29-235d-1382-a9dd-e6c2dc754817"), new DateTime(2022, 8, 7, 14, 31, 10, 667, DateTimeKind.Local).AddTicks(9841), "Dwight.Haag1", "Mose.Gerlach" },
                    { new Guid("60a2f32b-01ee-c7eb-6890-3a42c679f812"), new DateTime(2022, 8, 6, 6, 21, 20, 745, DateTimeKind.Local).AddTicks(7896), "Eugenia14", "Imelda.Hamill37" },
                    { new Guid("e5593121-ceb7-1f00-f78e-7148efe4d715"), new DateTime(2022, 8, 18, 5, 3, 33, 810, DateTimeKind.Local).AddTicks(7310), "Mohamed.Reinger", "Frankie_Reinger" },
                    { new Guid("f53df199-020a-5341-a3e0-b96d83d96b41"), new DateTime(2022, 8, 13, 5, 46, 14, 747, DateTimeKind.Local).AddTicks(5155), "Fannie.Corkery90", "Jose.Gerhold59" },
                    { new Guid("2787ed82-4670-1d19-ddc3-29ec8b2ff54b"), new DateTime(2022, 8, 23, 22, 0, 12, 402, DateTimeKind.Local).AddTicks(3300), "Leann.Kub", "Jayme.Adams" },
                    { new Guid("8d9a7156-d169-f7c2-c9bf-661cb8482769"), new DateTime(2022, 8, 15, 4, 48, 25, 984, DateTimeKind.Local).AddTicks(1117), "Jana.Maggio", "Clemmie77" },
                    { new Guid("8a8b4e83-105a-6b0e-b01a-d57f9073f167"), new DateTime(2022, 8, 6, 11, 15, 46, 62, DateTimeKind.Local).AddTicks(6538), "Mae_Bernier0", "Easter_Lynch" },
                    { new Guid("e29e5fe3-fe78-cb5e-e583-376c6eae57b0"), new DateTime(2022, 8, 22, 15, 3, 50, 633, DateTimeKind.Local).AddTicks(7773), "Therese_Kerluke", "Rogelio_Moore" },
                    { new Guid("92ac021c-7937-6a0a-372d-aecccea2d620"), new DateTime(2022, 8, 17, 3, 26, 27, 317, DateTimeKind.Local).AddTicks(8495), "Osvaldo.Lang66", "Beaulah.Wolf" },
                    { new Guid("2dafe3c0-a279-987e-2107-257e24ae1034"), new DateTime(2022, 8, 16, 15, 16, 20, 882, DateTimeKind.Local).AddTicks(3335), "Percival_Dickinson18", "Trisha5" },
                    { new Guid("c916e51d-7541-f313-d3dc-353f64db448b"), new DateTime(2022, 8, 16, 20, 7, 3, 842, DateTimeKind.Local).AddTicks(2865), "Alvis.Feil", "Hyman.Kulas87" },
                    { new Guid("abda5ba0-e1b8-ad90-02e6-79e8b928cbad"), new DateTime(2022, 8, 27, 22, 42, 19, 164, DateTimeKind.Local).AddTicks(7272), "Lauren.Murazik", "Tierra_Huel38" },
                    { new Guid("2224bab3-fbc1-d35b-120f-b791fdc09be5"), new DateTime(2022, 8, 28, 1, 21, 8, 307, DateTimeKind.Local).AddTicks(2515), "Maiya.Abernathy", "Assunta23" },
                    { new Guid("934ffda7-2981-8675-882e-d688e0257d8c"), new DateTime(2022, 9, 5, 1, 4, 40, 32, DateTimeKind.Local).AddTicks(7885), "Zita15", "Kacey_Daugherty" },
                    { new Guid("52b8db66-8bb3-d9b9-2c19-fd0c4091e542"), new DateTime(2022, 8, 25, 15, 28, 26, 33, DateTimeKind.Local).AddTicks(5806), "Oren.Walter", "Zachary.Ledner" },
                    { new Guid("54441296-7f5f-8b1d-c61a-4dbc33d4a21c"), new DateTime(2022, 8, 13, 19, 25, 28, 23, DateTimeKind.Local).AddTicks(5557), "Abbey_Schoen71", "Kelvin.Zieme75" },
                    { new Guid("8e6ffe4d-89ab-1d0d-7974-85cd99e0807a"), new DateTime(2022, 8, 28, 4, 38, 58, 616, DateTimeKind.Local).AddTicks(1972), "Porter.Kling", "Alberto_Jaskolski" },
                    { new Guid("f0c4af24-6d8e-d39f-cccf-8d568ca23226"), new DateTime(2022, 8, 19, 2, 45, 56, 46, DateTimeKind.Local).AddTicks(6664), "Garry.Larkin", "Macy_Flatley" },
                    { new Guid("b42e3f0d-5d13-c155-6986-da482ceb06e5"), new DateTime(2022, 8, 26, 2, 19, 17, 339, DateTimeKind.Local).AddTicks(3443), "Mortimer.Hagenes82", "Elyse23" },
                    { new Guid("3e84ffc7-0fb2-c5c4-812e-711fb9713894"), new DateTime(2022, 9, 3, 13, 42, 58, 127, DateTimeKind.Local).AddTicks(5264), "Zena_Willms", "Gaylord16" },
                    { new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"), new DateTime(2022, 8, 15, 19, 26, 42, 916, DateTimeKind.Local).AddTicks(9736), "Simone52", "Marlen.Cartwright49" },
                    { new Guid("190a9a8e-6ab3-3c92-b5a2-2b93ba3c25bb"), new DateTime(2022, 8, 11, 20, 36, 0, 243, DateTimeKind.Local).AddTicks(4101), "Otto67", "Elmira74" },
                    { new Guid("4698230e-e5c9-2b80-539b-7bd29e469c5f"), new DateTime(2022, 8, 27, 22, 34, 24, 9, DateTimeKind.Local).AddTicks(8309), "Hildegard17", "Faye.Abbott45" },
                    { new Guid("f8342ea6-d3c8-4288-5cfc-b139ae092c02"), new DateTime(2022, 8, 9, 5, 8, 12, 894, DateTimeKind.Local).AddTicks(3914), "Darius_Murazik91", "Darrion5" },
                    { new Guid("67c24ff5-7a90-8e66-2d76-c71dbb821837"), new DateTime(2022, 8, 16, 21, 29, 50, 77, DateTimeKind.Local).AddTicks(7134), "Easton98", "Ethel51" },
                    { new Guid("bbeade3d-2f8e-be2a-c4ee-94915f9d03ea"), new DateTime(2022, 8, 21, 8, 13, 59, 733, DateTimeKind.Local).AddTicks(7545), "Shanie_Casper", "Herbert.Kuhic58" },
                    { new Guid("18ad7730-abae-2ff3-291c-4d098d623787"), new DateTime(2022, 8, 6, 13, 4, 35, 870, DateTimeKind.Local).AddTicks(7310), "Rosario_Dicki98", "Jaydon.Tremblay" },
                    { new Guid("f29200bd-2d91-1ef1-fd90-c060176f0d18"), new DateTime(2022, 8, 20, 11, 36, 59, 99, DateTimeKind.Local).AddTicks(4025), "Dax80", "Bernice.Daugherty" },
                    { new Guid("2deeb484-ae82-07ac-87ee-f29d378b1f18"), new DateTime(2022, 8, 13, 11, 52, 58, 425, DateTimeKind.Local).AddTicks(6821), "Amos_Corwin61", "Merle30" },
                    { new Guid("cdd65641-25c3-1c01-0d79-02f17be21358"), new DateTime(2022, 8, 7, 17, 15, 23, 538, DateTimeKind.Local).AddTicks(3040), "Garett24", "Sid95" },
                    { new Guid("30ca3495-b39b-05c1-eb6d-5631b92acd9b"), new DateTime(2022, 8, 16, 1, 31, 51, 496, DateTimeKind.Local).AddTicks(5614), "Gardner_Langworth", "Pansy_Kiehn" },
                    { new Guid("ad117f58-44b5-b5ca-e32e-f037ec72c640"), new DateTime(2022, 8, 15, 12, 7, 5, 748, DateTimeKind.Local).AddTicks(1033), "Adele.Thiel", "Neoma95" },
                    { new Guid("f54e5991-8125-c329-cdee-c5e5ab778280"), new DateTime(2022, 8, 10, 18, 26, 26, 223, DateTimeKind.Local).AddTicks(9759), "Vicky96", "Esther.Schumm" },
                    { new Guid("715901aa-987a-0673-33da-522121bec10a"), new DateTime(2022, 8, 28, 13, 31, 46, 625, DateTimeKind.Local).AddTicks(6363), "Alaina_Kemmer", "Bryana_Upton78" },
                    { new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"), new DateTime(2022, 8, 15, 17, 7, 8, 530, DateTimeKind.Local).AddTicks(9706), "Emmitt.Bosco", "Carrie11" },
                    { new Guid("8cea1109-a4d9-15df-3571-97b9eddc511f"), new DateTime(2022, 8, 17, 15, 23, 24, 806, DateTimeKind.Local).AddTicks(854), "Jamel.Barton", "Leilani79" },
                    { new Guid("0ae57fc6-ab6c-1770-e443-34af5836288b"), new DateTime(2022, 8, 18, 6, 44, 29, 800, DateTimeKind.Local).AddTicks(9395), "Aurelie.Heidenreich", "Martina.Friesen" },
                    { new Guid("bfb1409a-1b30-1ba9-2b65-fb8b469511f6"), new DateTime(2022, 8, 13, 11, 33, 28, 682, DateTimeKind.Local).AddTicks(4790), "Hobart_Herzog57", "Mavis21" },
                    { new Guid("d20a4b2a-d920-80c4-e396-19ef9327f24c"), new DateTime(2022, 8, 7, 7, 16, 23, 339, DateTimeKind.Local).AddTicks(7466), "Velda14", "Sam_Herman" },
                    { new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), new DateTime(2022, 8, 24, 21, 23, 3, 157, DateTimeKind.Local).AddTicks(477), "Adolf.Tillman", "Tiara.Hartmann46" },
                    { new Guid("6178377b-00c3-b8ad-1483-23b0e32b5c48"), new DateTime(2022, 9, 1, 10, 35, 24, 457, DateTimeKind.Local).AddTicks(7630), "Dameon1", "Jammie.Rohan" },
                    { new Guid("ec39f26f-c3ba-926e-27ef-852e77817ae9"), new DateTime(2022, 8, 9, 9, 58, 11, 348, DateTimeKind.Local).AddTicks(2893), "Hudson67", "Theodore_Metz47" },
                    { new Guid("1388baaf-6ce5-dfc4-530e-74cb59bfb41d"), new DateTime(2022, 8, 24, 13, 15, 18, 622, DateTimeKind.Local).AddTicks(3044), "Paige13", "Kiera_Gerhold3" },
                    { new Guid("5a106410-72c4-2306-13b3-0a1409186a97"), new DateTime(2022, 8, 31, 3, 41, 11, 900, DateTimeKind.Local).AddTicks(7003), "Darwin50", "Genesis43" },
                    { new Guid("54cbeff1-bc4e-b8bc-085c-5fcef84901c5"), new DateTime(2022, 8, 6, 16, 12, 43, 750, DateTimeKind.Local).AddTicks(7696), "Assunta.Bruen", "Hillard_Franecki81" },
                    { new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), new DateTime(2022, 8, 13, 11, 32, 57, 485, DateTimeKind.Local).AddTicks(5852), "Alek_Okuneva51", "Camron28" },
                    { new Guid("fc49c059-de46-c66f-a1f4-ee1737a1dbef"), new DateTime(2022, 8, 24, 21, 46, 9, 889, DateTimeKind.Local).AddTicks(962), "Mariela_Fritsch74", "Josh_Maggio69" },
                    { new Guid("6ffde2a4-e3e3-c753-2e7d-5013521f25aa"), new DateTime(2022, 8, 28, 7, 34, 51, 323, DateTimeKind.Local).AddTicks(1560), "Samara.Lehner", "Orland85" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("25c1263a-d432-5147-b5b6-fd2ee76f8c5b"), "Candice Haag", 11.974104023992133m },
                    { new Guid("2161ea44-bda9-09c5-626f-ffd1c3377ed7"), "Bartholome Gislason", 6.854459464016586m },
                    { new Guid("cc981adb-bedb-0273-335d-6f9af93d14d3"), "Kip Ledner", 5.782784355703175m },
                    { new Guid("e516289b-2834-3265-b7ec-aef411012bdb"), "Elody Reichert", 5.5724533351941285m },
                    { new Guid("23ade046-012d-7f1c-4aa0-73562214b462"), "Brycen Waters", 9.11960815038514m },
                    { new Guid("6fa27318-76f8-bc4f-e774-5e0a0b72d8f1"), "Brendan Hintz", 10.170824643769687m },
                    { new Guid("9e946255-a266-9c1c-c43d-c35e86e51871"), "Lempi Hirthe", 10.301515736757555m },
                    { new Guid("d4e80b03-af95-918b-2308-fae38ce2c309"), "Genevieve Kreiger", 11.801525263954666m },
                    { new Guid("b5160fee-9310-8c33-ac62-7b663cd0e930"), "Johnnie Kohler", 7.255580472413254m },
                    { new Guid("13793282-54c6-a960-73fe-74712bac9cd7"), "Kira Considine", 11.943449931658548m },
                    { new Guid("6b614d2f-5210-4165-1861-37dc095ddf14"), "Megane Gibson", 8.042435658184083m },
                    { new Guid("5e01695b-cfe4-8bdc-8007-d57c539f9494"), "Cooper Smitham", 8.811768222512572m },
                    { new Guid("51aa6206-2a1b-8574-7109-5cbc9d0cb5d2"), "Robyn Wintheiser", 10.194327846725626m },
                    { new Guid("02d67cc5-e4a4-221a-d105-59123387a71d"), "Celestine Schimmel", 11.829264494976616m },
                    { new Guid("69261de2-46c8-a164-6032-b0047c1aa7c7"), "Sophia Ernser", 8.515269352363083m },
                    { new Guid("07016d05-80a3-f718-2935-f6eea5cdf14e"), "Therese Brakus", 11.131832661168571m },
                    { new Guid("4240bc0e-f0b9-0020-dcf7-8cbf7fedafff"), "Jaquan Konopelski", 6.802938533855109m },
                    { new Guid("4fafe883-6c3a-446a-0a37-8bcbc0281522"), "Ima Witting", 11.837328693288065m },
                    { new Guid("80821748-ff3b-759d-7600-f6cd1e6ed01b"), "Fleta Collins", 10.575152990210405m },
                    { new Guid("ac4fd606-c816-2c51-cb97-6acb0c3ba898"), "Aubree Rice", 5.02070508525739661m },
                    { new Guid("f79c4f74-43f4-6a14-6c5d-ef0ceac436be"), "Tad Steuber", 10.578348150280465m },
                    { new Guid("d3eab76c-ddd1-350b-ea92-75d5d11ac2e9"), "Aubree Swift", 10.779473792658874m },
                    { new Guid("6383759e-1eb1-42b0-8ff3-e65177c1b02c"), "Scarlett Collier", 6.948101698396777m },
                    { new Guid("48fe92b0-ef9b-5039-c05d-fd3e65d7af4b"), "Stanton Ferry", 5.800622163713267m },
                    { new Guid("2ab5cdca-1799-a324-71e3-9bcaab100e57"), "Ashlee Boyle", 10.763853995950827m },
                    { new Guid("c742eab8-7988-a282-91de-90534583c6ea"), "Marcelina Hauck", 7.16908034922978m },
                    { new Guid("3b718030-4722-6868-a6f4-11a170b5b4c2"), "Forrest Brown", 5.2659659577840783m },
                    { new Guid("bf50bdb0-921b-0544-4ea7-424f95f450fc"), "Adella Will", 7.356237646823858m },
                    { new Guid("94b44eeb-7b80-be4c-507f-ebcfc4c0a9e9"), "Salma Kub", 6.134790584507769m },
                    { new Guid("f8dfce2a-c6d2-e49f-b749-f6b182ae8a64"), "Carson Considine", 7.887421412341025m },
                    { new Guid("d3ce43d0-86f4-d863-7d8f-eb0afca5b631"), "Hermann Hermiston", 11.549059447622419m },
                    { new Guid("1f8a4088-c807-d1da-71e4-62af9eed2a86"), "Emmie Lueilwitz", 5.248110795509122m },
                    { new Guid("7bc8922f-cd77-ec58-7807-c3b9d048d473"), "Brent Hodkiewicz", 7.003957866692894m },
                    { new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598"), "Ramiro Ritchie", 10.678763792700488m },
                    { new Guid("dcaf5213-c961-2bae-9266-ca7a4140818b"), "Erick Miller", 7.313395625591925m },
                    { new Guid("ed5c88d6-cf0b-e7a6-b1da-6aaf30b22431"), "Rey Harber", 9.66494587700113m },
                    { new Guid("e1bb0da4-51fd-b627-99c7-1c0faf74f05d"), "Jacquelyn West", 7.324054987320703m },
                    { new Guid("2c8873d3-0915-8721-252c-6ac8e77a6c72"), "Madie Balistreri", 11.460922270296571m },
                    { new Guid("1823ff21-532b-5cf5-44f6-75ad79ea6111"), "Kurtis Barrows", 8.871999637629837m },
                    { new Guid("cadcd8a6-8a01-ff2f-3594-4201ce60e94e"), "Reggie White", 9.044022539651031m },
                    { new Guid("c3617645-dae9-cf74-e1df-5425d4567ce4"), "Millie Grant", 10.277579801752034m },
                    { new Guid("14eddb72-921e-0b40-3645-e2984f8dfb07"), "Darien Bechtelar", 8.344893156245767m },
                    { new Guid("845097ca-477a-c4a3-703f-98acf1d2314a"), "Lori Bednar", 9.189967535990278m },
                    { new Guid("22ec42e5-d991-d18e-d9b8-271713e2c2c2"), "Kian Sipes", 5.984066877506706m },
                    { new Guid("3d754e21-32d6-bcfa-3123-945fc99919a4"), "Garnet Bahringer", 6.525232125783912m },
                    { new Guid("604863e4-0320-0b09-817c-07d7f1b8df30"), "Shanel Klein", 5.4586785074596662m },
                    { new Guid("8330229c-8d98-fdc9-c7aa-8c09b9e49d12"), "Lizzie Crona", 11.040450718738352m },
                    { new Guid("dbba385f-3b3f-dd9e-86c5-ea073272417d"), "Heidi Pfannerstill", 11.163618724869389m },
                    { new Guid("39a0519e-a73f-06d1-9672-6b967a564156"), "Emely McClure", 6.639064277354189m },
                    { new Guid("caa1fe61-404b-6aab-3824-39d8b25191cd"), "Rosie Howe", 7.646243567413761m }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreateDate", "Email", "Password", "UserName" },
                values: new object[,]
                {
                    { new Guid("62bacc24-92af-2457-cfc1-8901035b8d54"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jon57@hotmail.com", "Piy5B63n_h", "Johnnie.Willms74" },
                    { new Guid("97d69928-dae5-c4ff-46b6-493847444096"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rosalee.Effertz85@hotmail.com", "pgH_2MsLS0", "Dixie.Hand20" },
                    { new Guid("419b6b26-8c6d-1ccc-fbe7-3cf93e594ade"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kip41@gmail.com", "su_aCT5NW8", "Haley71" },
                    { new Guid("570ba0d7-214c-9d3d-601a-7c9141c09ee5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alison_Cartwright@yahoo.com", "KqgNJGQyk_", "Jazmyn_Bergstrom80" },
                    { new Guid("9ee19294-5777-af49-8bd4-5338ce965509"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Piper_Kuhic28@yahoo.com", "Ig6INx6Jny", "Devin.Glover47" },
                    { new Guid("f5a1ac76-08c4-4b03-0aa3-08c4fbbafff5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alejandra_White81@gmail.com", "JTjivdEqiE", "Haley.Osinski18" },
                    { new Guid("f5b58a27-6b3b-1d35-5cf0-c5c70a1d0d84"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Vance7@hotmail.com", "fL1kvxnhBL", "Everett_Flatley48" },
                    { new Guid("4e38f433-caac-573e-4646-7df396ed495f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ulices_Rowe@yahoo.com", "sOJ0RnA4xU", "Quinton_Fay59" },
                    { new Guid("9063a2f2-b2e8-fb0c-eafb-1ea697155f37"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Herta_Johnston@hotmail.com", "i1fG9qgwX1", "Kianna91" },
                    { new Guid("edb64a32-5259-abac-d0e3-fcc82b2e920f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Justen55@yahoo.com", "H8RqEHEF9F", "Hilma61" },
                    { new Guid("8f1cab08-e6d7-7fba-260e-6ae892375b70"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Saul.Spinka@hotmail.com", "0mYq90Kc7t", "Vida.Hudson65" },
                    { new Guid("68fdcb8a-8c50-a950-38dc-2ec486f5ec36"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alize.Block@gmail.com", "bLkumYrR8l", "Rodrick_Lesch" },
                    { new Guid("b6fcb092-ac8c-f934-e8db-d25e9ab4d170"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Anabel7@hotmail.com", "_47SUX6dju", "Tatyana.Gorczany19" },
                    { new Guid("677c530a-3d2d-f2f7-47c8-e1a01d801fc6"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ola_Upton22@gmail.com", "dsmzUsSSCB", "Arno_Osinski36" },
                    { new Guid("db3323c8-0d80-f637-2bae-93f36458eaea"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Merle94@gmail.com", "sLGavRKGnq", "Orlando_Rowe66" },
                    { new Guid("efcca2ee-7acf-09f6-5dcc-8fe04200d997"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Michel.Jacobs35@yahoo.com", "jStwoKEbQn", "Cielo.Toy" },
                    { new Guid("de7a58f1-b785-ec87-b1ea-2d1bd1b12a22"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Regan.Howe@yahoo.com", "cIKks77FbV", "Helga71" },
                    { new Guid("689e82f2-b006-3f1f-ac57-d17dbfc75b5b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bill.Bauch5@gmail.com", "Nrcf6QwCh7", "Judson.Emmerich" },
                    { new Guid("477576f6-398d-2e8d-60ad-8724cb1e355c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Camille.Rau@hotmail.com", "ZeGQLdjzDy", "Carli60" },
                    { new Guid("eb0cfd23-c79b-a4dc-7517-bf0a0892d4e4"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ora_Rempel84@yahoo.com", "W_1eZtOY47", "Allison_Tillman70" }
                });

            migrationBuilder.InsertData(
                table: "Staffs",
                columns: new[] { "Id", "Adress", "CreateDate", "Name", "NamePart", "PartId", "Phone" },
                values: new object[,]
                {
                    { new Guid("eece8354-25ec-4cd2-c00d-f56eed3b0225"), "59156 Heathcote Roads", new DateTime(2022, 8, 20, 16, 57, 55, 763, DateTimeKind.Local).AddTicks(4194), "Frank Treutel", "Felipe Volkman", new Guid("2deeb484-ae82-07ac-87ee-f29d378b1f18"), "944-772-7795" },
                    { new Guid("737a5e3b-fc31-080b-d386-725d2e47f4c9"), "342 Harvey Knoll", new DateTime(2022, 7, 30, 13, 45, 37, 246, DateTimeKind.Local).AddTicks(4381), "Katheryn Kshlerin", "Mandy Lesch", new Guid("802c4c29-235d-1382-a9dd-e6c2dc754817"), "(917) 362-1543" },
                    { new Guid("9038bc44-07b5-7d03-f25e-24555f29aff4"), "962 Cortney Landing", new DateTime(2022, 7, 25, 0, 30, 6, 660, DateTimeKind.Local).AddTicks(2075), "Maia Botsford", "Alexandrea Kiehn", new Guid("60a2f32b-01ee-c7eb-6890-3a42c679f812"), "1-809-387-8877 x8145" },
                    { new Guid("dc92c1ac-22b7-9fd3-2696-61fd96d2801b"), "620 Lavonne Ramp", new DateTime(2022, 8, 15, 2, 5, 53, 967, DateTimeKind.Local).AddTicks(771), "Mathias Botsford", "Sabryna Pagac", new Guid("e5593121-ceb7-1f00-f78e-7148efe4d715"), "1-802-321-3435 x5900" },
                    { new Guid("596e114a-dcf4-28b6-d40e-60f7ecd2c3e8"), "530 Domenick Knoll", new DateTime(2022, 7, 13, 23, 25, 53, 958, DateTimeKind.Local).AddTicks(3912), "Guido Champlin", "Destinee Jacobson", new Guid("e5593121-ceb7-1f00-f78e-7148efe4d715"), "(604) 436-9000" },
                    { new Guid("6585182d-2c5a-11bb-a70d-22318d15b950"), "4172 Jane Flats", new DateTime(2022, 8, 20, 0, 57, 17, 491, DateTimeKind.Local).AddTicks(326), "Harmon Waelchi", "Remington Wolf", new Guid("f53df199-020a-5341-a3e0-b96d83d96b41"), "581-953-0660" },
                    { new Guid("9b50b98c-25b6-12bb-5cd4-fd740442f74f"), "428 Claudine Stream", new DateTime(2022, 7, 27, 2, 15, 54, 480, DateTimeKind.Local).AddTicks(2957), "Ulices Rowe", "Adell Collier", new Guid("2787ed82-4670-1d19-ddc3-29ec8b2ff54b"), "(852) 284-7078" },
                    { new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d"), "08312 Gianni Island", new DateTime(2022, 8, 10, 20, 29, 58, 748, DateTimeKind.Local).AddTicks(3681), "Arvid Rowe", "August Sauer", new Guid("e29e5fe3-fe78-cb5e-e583-376c6eae57b0"), "368.773.8560" },
                    { new Guid("6148b7c1-313d-0b71-99ee-aed730fb76ee"), "574 Weber Street", new DateTime(2022, 7, 26, 19, 18, 18, 671, DateTimeKind.Local).AddTicks(3077), "Janis Bergnaum", "Norris Jacobs", new Guid("e29e5fe3-fe78-cb5e-e583-376c6eae57b0"), "1-576-308-2617 x011" },
                    { new Guid("e7b7328a-7a58-2d20-e6e9-ae675adfc3f9"), "4034 Cassin Meadows", new DateTime(2022, 7, 31, 7, 33, 43, 902, DateTimeKind.Local).AddTicks(9221), "Yesenia D'Amore", "Marilou Buckridge", new Guid("92ac021c-7937-6a0a-372d-aecccea2d620"), "(465) 765-8008 x0200" },
                    { new Guid("03c19da2-646d-e11e-cdd1-5e329bb32962"), "8219 Ratke Heights", new DateTime(2022, 7, 21, 20, 48, 39, 769, DateTimeKind.Local).AddTicks(4457), "Monty Harvey", "Macie Bashirian", new Guid("f8342ea6-d3c8-4288-5cfc-b139ae092c02"), "470.424.4715" },
                    { new Guid("1a8b10a0-df5e-4b1e-fa34-ddb35be0eb8b"), "74955 Konopelski Vista", new DateTime(2022, 7, 29, 19, 22, 54, 410, DateTimeKind.Local).AddTicks(6692), "Bartholome Mitchell", "Mateo Kuhn", new Guid("c916e51d-7541-f313-d3dc-353f64db448b"), "297-930-2705 x89770" },
                    { new Guid("a0fdb218-16c6-056f-cc8c-cf32a6134249"), "2287 Madie Grove", new DateTime(2022, 8, 22, 4, 29, 54, 262, DateTimeKind.Local).AddTicks(8832), "Cesar Stamm", "Pearline Jacobson", new Guid("c916e51d-7541-f313-d3dc-353f64db448b"), "724.977.2551" },
                    { new Guid("66dcd09b-a68e-fa1a-e5f5-242fe8f40fbe"), "54247 Urban Stravenue", new DateTime(2022, 8, 5, 3, 19, 53, 909, DateTimeKind.Local).AddTicks(2007), "Sydnee Miller", "Milford Orn", new Guid("2224bab3-fbc1-d35b-120f-b791fdc09be5"), "(894) 344-5366" },
                    { new Guid("0e02ac36-74c6-9f0b-0d15-005d5d84dfbe"), "7409 Howell Manor", new DateTime(2022, 8, 21, 9, 59, 48, 544, DateTimeKind.Local).AddTicks(2216), "Emie Champlin", "Rosemarie Crist", new Guid("2224bab3-fbc1-d35b-120f-b791fdc09be5"), "(900) 529-3046" },
                    { new Guid("d918a0f1-d04a-4402-5035-c1b95e4e8a48"), "08912 Terry Squares", new DateTime(2022, 8, 30, 18, 23, 56, 579, DateTimeKind.Local).AddTicks(6055), "Leonor Gutmann", "Lilliana McLaughlin", new Guid("52b8db66-8bb3-d9b9-2c19-fd0c4091e542"), "1-334-549-6542 x6760" },
                    { new Guid("97a802c2-c94f-bed8-925d-31478f9b50e8"), "78824 Moen Lakes", new DateTime(2022, 9, 5, 10, 56, 22, 71, DateTimeKind.Local).AddTicks(7026), "Ebony Welch", "Alfreda O'Reilly", new Guid("54441296-7f5f-8b1d-c61a-4dbc33d4a21c"), "(416) 384-5186" },
                    { new Guid("00e54572-9329-cc20-8146-45548f7f4fcc"), "99191 Stefan Path", new DateTime(2022, 9, 5, 17, 34, 14, 568, DateTimeKind.Local).AddTicks(9028), "Kariane Bergstrom", "Johnny Shanahan", new Guid("54441296-7f5f-8b1d-c61a-4dbc33d4a21c"), "(741) 414-3429 x5427" },
                    { new Guid("8b3f7c2d-70e0-90b6-5263-889b49b96b64"), "98469 Janelle Lodge", new DateTime(2022, 8, 3, 5, 27, 59, 265, DateTimeKind.Local).AddTicks(8689), "Marco DuBuque", "Sienna McGlynn", new Guid("8e6ffe4d-89ab-1d0d-7974-85cd99e0807a"), "839.515.7620 x718" },
                    { new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf"), "0646 Micaela Courts", new DateTime(2022, 7, 7, 8, 20, 10, 59, DateTimeKind.Local).AddTicks(7685), "Kasandra Hartmann", "Rubye Ryan", new Guid("8e6ffe4d-89ab-1d0d-7974-85cd99e0807a"), "1-945-978-3247 x978" },
                    { new Guid("fdd2cb5d-621f-f1cf-4a18-78301b15a936"), "0993 Marvin Land", new DateTime(2022, 8, 24, 3, 45, 15, 952, DateTimeKind.Local).AddTicks(1628), "Jewell Bednar", "Telly Hackett", new Guid("b42e3f0d-5d13-c155-6986-da482ceb06e5"), "272-243-7732" },
                    { new Guid("08644456-3ebe-f008-7ce7-c6e9cb0c334a"), "6911 Sarah Stream", new DateTime(2022, 8, 24, 11, 9, 4, 567, DateTimeKind.Local).AddTicks(240), "Lizzie Yost", "Brandi Paucek", new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"), "(846) 947-2651" },
                    { new Guid("a32b4e75-4941-9679-163a-1a57f6dd702a"), "6839 Sylvia Burgs", new DateTime(2022, 7, 21, 15, 39, 37, 903, DateTimeKind.Local).AddTicks(8051), "Casey Beahan", "Alayna King", new Guid("4698230e-e5c9-2b80-539b-7bd29e469c5f"), "1-672-765-8797" },
                    { new Guid("2a738e5d-dca9-82bd-17e9-9ef7db1b7d98"), "2933 Gottlieb Turnpike", new DateTime(2022, 7, 6, 9, 4, 36, 439, DateTimeKind.Local).AddTicks(4733), "Ramon Emmerich", "Tracey Howe", new Guid("bbeade3d-2f8e-be2a-c4ee-94915f9d03ea"), "1-358-374-1837 x4299" },
                    { new Guid("292d1131-2381-b342-3a05-72ccf4e3d147"), "527 Alda Parkway", new DateTime(2022, 7, 20, 21, 45, 53, 96, DateTimeKind.Local).AddTicks(5902), "Emily Bins", "Alessia Rolfson", new Guid("bbeade3d-2f8e-be2a-c4ee-94915f9d03ea"), "1-765-751-1384" },
                    { new Guid("4c4d04a1-b098-8ce0-652c-d96d36dbbe44"), "8266 Oren Shoal", new DateTime(2022, 8, 5, 14, 49, 45, 979, DateTimeKind.Local).AddTicks(7786), "Germaine Langworth", "Elena Corkery", new Guid("67c24ff5-7a90-8e66-2d76-c71dbb821837"), "(877) 284-2723 x1855" },
                    { new Guid("dc304c87-09f7-8fab-b2f9-bd99b6da2aea"), "528 Gorczany Extensions", new DateTime(2022, 7, 23, 5, 40, 31, 0, DateTimeKind.Local).AddTicks(738), "Dayton Thiel", "Ahmad Torp", new Guid("cdd65641-25c3-1c01-0d79-02f17be21358"), "815.804.3337" },
                    { new Guid("ecc1fb02-1a2e-902c-309d-8f348ae34023"), "424 Kerluke Locks", new DateTime(2022, 9, 2, 2, 52, 1, 366, DateTimeKind.Local).AddTicks(4312), "Keith Walker", "Abbey Crona", new Guid("30ca3495-b39b-05c1-eb6d-5631b92acd9b"), "980.835.7259 x801" },
                    { new Guid("64735d54-7642-72c8-659b-7616a68a3338"), "62904 Gerson Turnpike", new DateTime(2022, 8, 30, 2, 25, 6, 219, DateTimeKind.Local).AddTicks(8098), "Ervin Waelchi", "Gordon Goyette", new Guid("30ca3495-b39b-05c1-eb6d-5631b92acd9b"), "1-623-440-4795 x60624" },
                    { new Guid("363bc0cb-083e-5630-71ba-dabbacd706dd"), "81802 Schimmel Lights", new DateTime(2022, 8, 18, 16, 55, 55, 258, DateTimeKind.Local).AddTicks(9030), "Virginia Kassulke", "Gardner Schulist", new Guid("ad117f58-44b5-b5ca-e32e-f037ec72c640"), "(777) 790-4182 x931" },
                    { new Guid("17a6f792-66c6-b278-b806-19485cacee54"), "52518 Mann Shores", new DateTime(2022, 7, 21, 12, 41, 27, 652, DateTimeKind.Local).AddTicks(6315), "Lindsey Bauch", "Marta Hills", new Guid("715901aa-987a-0673-33da-522121bec10a"), "(309) 861-1482" },
                    { new Guid("2ef232d7-920f-4a24-11a3-4e576177ccfa"), "62359 Wendell Rest", new DateTime(2022, 8, 19, 17, 36, 36, 224, DateTimeKind.Local).AddTicks(6624), "Mayra Nicolas", "Hattie Miller", new Guid("715901aa-987a-0673-33da-522121bec10a"), "482.946.0825" },
                    { new Guid("428ac028-9d86-4ced-8c8a-a85d4b9c1e3d"), "384 Stroman Garden", new DateTime(2022, 8, 9, 11, 20, 56, 751, DateTimeKind.Local).AddTicks(8156), "Lauriane Oberbrunner", "Jasper McCullough", new Guid("715901aa-987a-0673-33da-522121bec10a"), "1-225-713-6813 x587" },
                    { new Guid("3cbf7731-ffcf-0075-6676-bde35fddb8d1"), "11648 Boehm Mill", new DateTime(2022, 8, 4, 2, 31, 34, 954, DateTimeKind.Local).AddTicks(1328), "Sandy Kunde", "Christiana Hodkiewicz", new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"), "1-294-424-7611" },
                    { new Guid("d8035464-71c8-ac5e-234a-9b805a11a569"), "77065 Jerad Knoll", new DateTime(2022, 8, 19, 4, 52, 53, 768, DateTimeKind.Local).AddTicks(6179), "Xzavier Haag", "Aletha Reinger", new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"), "960.852.3408 x505" },
                    { new Guid("37323f33-86c8-3f82-842a-a4d548beabb8"), "860 Morissette Fort", new DateTime(2022, 8, 27, 21, 57, 43, 924, DateTimeKind.Local).AddTicks(7607), "Patricia Metz", "Tamara Medhurst", new Guid("464ddc26-00fd-c301-7a7f-cf52535651de"), "630.223.1136 x64585" },
                    { new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b"), "4921 Casimir Flat", new DateTime(2022, 9, 2, 20, 16, 3, 424, DateTimeKind.Local).AddTicks(5139), "Dorthy Jacobs", "Keagan Stark", new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"), "970-270-8912 x91242" },
                    { new Guid("cd4ac6da-0caa-def4-be85-c2d738cf916f"), "461 Abbott Freeway", new DateTime(2022, 8, 3, 8, 56, 35, 759, DateTimeKind.Local).AddTicks(4813), "Kevon Sawayn", "Eddie Bernhard", new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), "(355) 482-1510 x342" },
                    { new Guid("a417997b-6a8d-1047-8b38-49481bd044cb"), "585 Fiona Causeway", new DateTime(2022, 7, 13, 6, 32, 24, 870, DateTimeKind.Local).AddTicks(2907), "April Grady", "Orin Gusikowski", new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), "367-556-3595" },
                    { new Guid("69f34ae5-db6d-2a23-2ac4-8bc2d0a17470"), "337 Jaskolski Bypass", new DateTime(2022, 7, 19, 3, 10, 40, 118, DateTimeKind.Local).AddTicks(5611), "Gretchen Bernhard", "Torrance Kling", new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), "(486) 606-8555 x3487" },
                    { new Guid("707740bf-6987-5dc0-87db-230cd35bed46"), "7131 Ryan Views", new DateTime(2022, 7, 21, 8, 39, 30, 773, DateTimeKind.Local).AddTicks(7308), "Destini Luettgen", "Carolyne Price", new Guid("6178377b-00c3-b8ad-1483-23b0e32b5c48"), "281.843.5762" },
                    { new Guid("23d0f4ff-61f4-cf86-f9c3-c9d01567ba63"), "6898 McDermott Walk", new DateTime(2022, 8, 5, 23, 44, 40, 562, DateTimeKind.Local).AddTicks(8221), "Nicolette Lesch", "Efrain Romaguera", new Guid("1388baaf-6ce5-dfc4-530e-74cb59bfb41d"), "(971) 801-7153 x142" },
                    { new Guid("f777c83e-2104-c344-3f79-db4a3b634b4b"), "4632 Tierra Dale", new DateTime(2022, 8, 7, 13, 58, 13, 656, DateTimeKind.Local).AddTicks(9806), "Edward Orn", "Jacklyn Lindgren", new Guid("1388baaf-6ce5-dfc4-530e-74cb59bfb41d"), "(371) 553-9230 x71280" },
                    { new Guid("5ed3fbd8-5d8a-5141-0d80-b6bd5ee3961b"), "586 Rusty Meadow", new DateTime(2022, 8, 25, 14, 7, 13, 239, DateTimeKind.Local).AddTicks(1678), "Leonard Hammes", "Graham Emmerich", new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), "891-756-6737" },
                    { new Guid("19298057-71d1-e643-d8d2-75a075a133ce"), "635 Christy Mission", new DateTime(2022, 7, 22, 2, 21, 5, 25, DateTimeKind.Local).AddTicks(1011), "Elnora Predovic", "Dianna Kunze", new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), "764.797.7742" },
                    { new Guid("70176971-a6cc-265a-0259-d18af3babc70"), "57259 Batz Greens", new DateTime(2022, 7, 15, 23, 32, 12, 110, DateTimeKind.Local).AddTicks(1121), "Eleazar Kuhlman", "Berneice Raynor", new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), "1-568-513-6957" },
                    { new Guid("c757d324-c682-9d14-f213-adffd86f17fc"), "4309 Dayton Court", new DateTime(2022, 8, 4, 8, 33, 58, 871, DateTimeKind.Local).AddTicks(449), "Frida Daniel", "Abigail Volkman", new Guid("7dbc5691-c915-02bd-d283-3bcfc5f64d5c"), "208.870.5789" },
                    { new Guid("2c850a60-85fc-7d04-b9f7-32159895f8e5"), "515 Burnice Lake", new DateTime(2022, 7, 31, 15, 8, 38, 963, DateTimeKind.Local).AddTicks(6559), "Jaron Morissette", "Lance Muller", new Guid("6ffde2a4-e3e3-c753-2e7d-5013521f25aa"), "236.529.2910 x659" },
                    { new Guid("13208737-53bc-80c8-483b-8e63d9d042c8"), "8565 Halvorson Lakes", new DateTime(2022, 9, 4, 22, 36, 10, 84, DateTimeKind.Local).AddTicks(9346), "Rollin Kozey", "Chasity Marks", new Guid("d0a1560c-6a00-a6bc-d944-7a9722cd3d72"), "905-580-8532 x353" },
                    { new Guid("3cdf8fd0-1f92-3e5b-f04d-c9f45530bcec"), "596 Lynch Court", new DateTime(2022, 9, 3, 14, 48, 35, 111, DateTimeKind.Local).AddTicks(1628), "Tavares Price", "Tanya Sporer", new Guid("070e8ea4-4a22-03db-2a95-553d2bd4cb83"), "701-575-2154" }
                });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "Id", "NamePart", "NameStaff", "StaffId" },
                values: new object[,]
                {
                    { new Guid("d961269f-e5d1-1541-26ed-f97767e65a6c"), "Dameon1", "Guido Champlin", new Guid("eece8354-25ec-4cd2-c00d-f56eed3b0225") },
                    { new Guid("a090ff59-c55b-00a2-aeb1-9e6f8e7437f1"), "Otto67", "Dayton Thiel", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("5f9fa83f-6cbd-c5b8-05e1-4ee1b7c3c701"), "Shyanne_Legros", "Janis Bergnaum", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"), "Maiya.Abernathy", "Janis Bergnaum", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("4edec551-e708-3c41-5a2e-54b4f90c978d"), "Easton98", "Sandy Kunde", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("6c54248f-902e-27b7-25a9-1628050db7bd"), "Darwin50", "Guido Champlin", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("425dd829-9b9e-201e-bb22-87b28388a9a6"), "Hildegard17", "Ulices Rowe", new Guid("6148b7c1-313d-0b71-99ee-aed730fb76ee") },
                    { new Guid("257238c6-a427-b0ed-12ca-d989926219e1"), "Hudson67", "Kevon Sawayn", new Guid("6148b7c1-313d-0b71-99ee-aed730fb76ee") },
                    { new Guid("babc03e8-adfe-ab67-cccd-3d43ae9f1a47"), "Hobart_Herzog57", "Maia Botsford", new Guid("03c19da2-646d-e11e-cdd1-5e329bb32962") },
                    { new Guid("8fae5f78-cf7d-4cc2-ceb0-31b87178284d"), "Darius_Murazik91", "Emily Bins", new Guid("03c19da2-646d-e11e-cdd1-5e329bb32962") },
                    { new Guid("4a3110a4-0c2f-34de-523e-e0b900790540"), "Zena6", "Lauriane Oberbrunner", new Guid("66dcd09b-a68e-fa1a-e5f5-242fe8f40fbe") },
                    { new Guid("24cba960-243f-8eed-f954-388350f7a42e"), "Hobart_Herzog57", "Jewell Bednar", new Guid("66dcd09b-a68e-fa1a-e5f5-242fe8f40fbe") },
                    { new Guid("78ac5e27-766b-cf6d-c472-46f74a23448f"), "Dameon1", "Germaine Langworth", new Guid("0e02ac36-74c6-9f0b-0d15-005d5d84dfbe") },
                    { new Guid("7b4c3eaf-b74f-164b-c658-902665aafbeb"), "Hildegard17", "Sydnee Miller", new Guid("d918a0f1-d04a-4402-5035-c1b95e4e8a48") },
                    { new Guid("f30dd851-b816-40fd-e9df-97223f7668da"), "Eugenia14", "Cesar Stamm", new Guid("d918a0f1-d04a-4402-5035-c1b95e4e8a48") },
                    { new Guid("7243e63d-9bfc-0972-fa64-5c9468bfc3d1"), "Maiya.Abernathy", "Arvid Rowe", new Guid("97a802c2-c94f-bed8-925d-31478f9b50e8") },
                    { new Guid("6bf84cec-7d37-7772-db15-54951c7b8b5f"), "Hobart_Herzog57", "Maia Botsford", new Guid("00e54572-9329-cc20-8146-45548f7f4fcc") },
                    { new Guid("058753b3-3837-9aea-f0f7-0511c2d29d4b"), "Dax80", "Jewell Bednar", new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf") },
                    { new Guid("1c644315-1e5b-46c1-cd53-c75e7d46f2bc"), "Percival_Dickinson18", "Xzavier Haag", new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf") },
                    { new Guid("b6d1a298-a50a-157c-7422-0dfaed645f2b"), "Adolf.Tillman", "Guido Champlin", new Guid("577da76d-f4db-a6ce-9b91-6a6a356538cf") },
                    { new Guid("4ec824fd-fc0f-b47c-44eb-e0622c8d4d06"), "Emmitt.Bosco", "Leonor Gutmann", new Guid("fdd2cb5d-621f-f1cf-4a18-78301b15a936") },
                    { new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), "Porter.Kling", "Emily Bins", new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b") },
                    { new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"), "Zena_Willms", "April Grady", new Guid("f3a96b5f-40f3-ab13-f348-2b6f5ce4080d") },
                    { new Guid("6a5205f3-feb9-d175-962f-22a9ed849b66"), "Mariela_Fritsch74", "Lindsey Bauch", new Guid("9b50b98c-25b6-12bb-5cd4-fd740442f74f") },
                    { new Guid("e580e679-ec05-fecd-c5ea-0ca425b9776d"), "Abbey_Schoen71", "Gretchen Bernhard", new Guid("6585182d-2c5a-11bb-a70d-22318d15b950") },
                    { new Guid("109571f6-d42a-79e4-b7d3-aed716fb7ae2"), "Eugenia14", "Tavares Price", new Guid("596e114a-dcf4-28b6-d40e-60f7ecd2c3e8") },
                    { new Guid("b0130de5-0167-51f9-9652-76057f1adc7a"), "Amos_Corwin61", "Edward Orn", new Guid("eece8354-25ec-4cd2-c00d-f56eed3b0225") },
                    { new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"), "Alvis.Feil", "Virginia Kassulke", new Guid("ecc1fb02-1a2e-902c-309d-8f348ae34023") },
                    { new Guid("2c3e6a90-25dc-0f65-d57a-4000c4b96044"), "Shyanne_Legros", "Sydnee Miller", new Guid("ecc1fb02-1a2e-902c-309d-8f348ae34023") },
                    { new Guid("923ca8a6-1d68-fb7a-37ab-d9eac6a9193d"), "Dax80", "Lindsey Bauch", new Guid("64735d54-7642-72c8-659b-7616a68a3338") },
                    { new Guid("329a58e6-03ae-1f52-cf69-6f0bc15af361"), "Assunta.Bruen", "Ulices Rowe", new Guid("363bc0cb-083e-5630-71ba-dabbacd706dd") },
                    { new Guid("161d9192-c183-9e3c-bd02-4935c8b84557"), "Mariela_Fritsch74", "Elnora Predovic", new Guid("17a6f792-66c6-b278-b806-19485cacee54") },
                    { new Guid("822b7a9e-e1c5-1495-ca67-b2aaae095cbc"), "Fannie.Corkery90", "Jaron Morissette", new Guid("cd4ac6da-0caa-def4-be85-c2d738cf916f") },
                    { new Guid("2ff2b15a-2163-a874-8980-e3b467a12a14"), "Otto67", "Katheryn Kshlerin", new Guid("cd4ac6da-0caa-def4-be85-c2d738cf916f") },
                    { new Guid("a53c05f1-dee7-fe26-1ae6-e4ce7e13bb49"), "Velda14", "Jewell Bednar", new Guid("707740bf-6987-5dc0-87db-230cd35bed46") },
                    { new Guid("43856184-28f8-18f8-ce8f-30a4c88688e4"), "Vicky96", "Xzavier Haag", new Guid("f777c83e-2104-c344-3f79-db4a3b634b4b") },
                    { new Guid("39692b63-4874-aae8-4bd7-4c9ee31c61b3"), "Hobart_Herzog57", "Mayra Nicolas", new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b") },
                    { new Guid("4feda6f9-d3fc-5c87-7c3a-39b6ab33f4d8"), "Samara.Lehner", "Gretchen Bernhard", new Guid("5ed3fbd8-5d8a-5141-0d80-b6bd5ee3961b") },
                    { new Guid("a45dbb54-3345-1d1a-a0dc-6524630df8c6"), "Otto67", "Lizzie Yost", new Guid("19298057-71d1-e643-d8d2-75a075a133ce") },
                    { new Guid("7d680ada-8fdb-c6f1-d85f-9aa4bd8fc81b"), "Vicky96", "Katheryn Kshlerin", new Guid("19298057-71d1-e643-d8d2-75a075a133ce") },
                    { new Guid("0b66a772-f8ba-2a8f-7cff-611e863dff14"), "Alaina_Kemmer", "Monty Harvey", new Guid("c757d324-c682-9d14-f213-adffd86f17fc") },
                    { new Guid("df2d60b1-6e5a-e140-735c-92b88fc72a7c"), "Garett24", "Rollin Kozey", new Guid("4c4d04a1-b098-8ce0-652c-d96d36dbbe44") },
                    { new Guid("b0b20bef-ee68-9076-8e33-83226a277b8f"), "Hudson67", "Kasandra Hartmann", new Guid("2a738e5d-dca9-82bd-17e9-9ef7db1b7d98") },
                    { new Guid("5919fa5c-f41a-e47a-2bd2-355a426c0d49"), "Aurelie.Heidenreich", "Harmon Waelchi", new Guid("2a738e5d-dca9-82bd-17e9-9ef7db1b7d98") },
                    { new Guid("0f358ba3-8a3c-2bcb-a2ad-c3eae579db79"), "Amos_Corwin61", "Rollin Kozey", new Guid("737a5e3b-fc31-080b-d386-725d2e47f4c9") },
                    { new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"), "Velda14", "Lauriane Oberbrunner", new Guid("dc92c1ac-22b7-9fd3-2696-61fd96d2801b") },
                    { new Guid("ee544476-56a0-2e56-5df0-8c40fad4d07e"), "Jamel.Barton", "Germaine Langworth", new Guid("dc92c1ac-22b7-9fd3-2696-61fd96d2801b") },
                    { new Guid("8b847645-fafd-0e79-daf8-b34e6bcf2b8a"), "Fannie.Corkery90", "Janis Bergnaum", new Guid("596e114a-dcf4-28b6-d40e-60f7ecd2c3e8") },
                    { new Guid("c79beded-a660-c08a-7f24-8228409e7cdb"), "Amos_Corwin61", "Bartholome Mitchell", new Guid("19298057-71d1-e643-d8d2-75a075a133ce") },
                    { new Guid("efaa2963-c4a9-6831-acfb-445bd3c1c265"), "Adele.Thiel", "Xzavier Haag", new Guid("747019e2-cb5b-77f3-1ad8-3b2c3f8fae8b") }
                });

            migrationBuilder.InsertData(
                table: "OrderDetails",
                columns: new[] { "Id", "Amount", "OrderId", "ProductId" },
                values: new object[,]
                {
                    { new Guid("c5e39430-006c-b382-bd2d-0d179530843f"), 55, new Guid("d961269f-e5d1-1541-26ed-f97767e65a6c"), new Guid("d3ce43d0-86f4-d863-7d8f-eb0afca5b631") },
                    { new Guid("2451a1ea-2ca6-989a-1619-5dbfa600a5e6"), 55, new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"), new Guid("1823ff21-532b-5cf5-44f6-75ad79ea6111") },
                    { new Guid("9311a9c1-74fb-04b9-d2f8-54cb72b86c25"), 55, new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"), new Guid("e516289b-2834-3265-b7ec-aef411012bdb") },
                    { new Guid("0033b837-ed58-3444-aa5d-bbdb28c9fd16"), 55, new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"), new Guid("6383759e-1eb1-42b0-8ff3-e65177c1b02c") },
                    { new Guid("4c1476e0-6657-1ee2-028d-c3b5a6689056"), 55, new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"), new Guid("f8dfce2a-c6d2-e49f-b749-f6b182ae8a64") },
                    { new Guid("19d7e30b-dbf0-af2a-b42d-9dc3a77aa9dc"), 55, new Guid("d6dd602c-d3ec-81aa-e139-d82f05c04d2b"), new Guid("4fafe883-6c3a-446a-0a37-8bcbc0281522") },
                    { new Guid("6d4d1ce0-20b5-b8ae-e4cf-47f4f93e0f05"), 55, new Guid("4edec551-e708-3c41-5a2e-54b4f90c978d"), new Guid("604863e4-0320-0b09-817c-07d7f1b8df30") },
                    { new Guid("15e621a7-86f2-c279-c4c3-736df6448436"), 55, new Guid("6c54248f-902e-27b7-25a9-1628050db7bd"), new Guid("02d67cc5-e4a4-221a-d105-59123387a71d") },
                    { new Guid("9f7f5db1-f5c6-acb4-3f3e-fc5f4d5112a8"), 55, new Guid("257238c6-a427-b0ed-12ca-d989926219e1"), new Guid("bf50bdb0-921b-0544-4ea7-424f95f450fc") },
                    { new Guid("2b5e417f-d76f-7058-7d73-8c63d0848b47"), 55, new Guid("8fae5f78-cf7d-4cc2-ceb0-31b87178284d"), new Guid("69261de2-46c8-a164-6032-b0047c1aa7c7") },
                    { new Guid("75ac8f04-4930-a8bd-ac67-f648d2ee6a79"), 55, new Guid("8fae5f78-cf7d-4cc2-ceb0-31b87178284d"), new Guid("ed5c88d6-cf0b-e7a6-b1da-6aaf30b22431") },
                    { new Guid("0d03c172-57c2-b2cc-c38b-d33e4cc112d6"), 55, new Guid("24cba960-243f-8eed-f954-388350f7a42e"), new Guid("2161ea44-bda9-09c5-626f-ffd1c3377ed7") },
                    { new Guid("afef892e-6ddf-f78b-af7d-256318f2d6f4"), 55, new Guid("24cba960-243f-8eed-f954-388350f7a42e"), new Guid("2161ea44-bda9-09c5-626f-ffd1c3377ed7") },
                    { new Guid("10c0ffe8-0a72-566b-4e8b-b4d62f38f7ae"), 55, new Guid("7b4c3eaf-b74f-164b-c658-902665aafbeb"), new Guid("14eddb72-921e-0b40-3645-e2984f8dfb07") },
                    { new Guid("f210fadd-f385-8e63-1874-352b279a41cc"), 55, new Guid("7243e63d-9bfc-0972-fa64-5c9468bfc3d1"), new Guid("2ab5cdca-1799-a324-71e3-9bcaab100e57") },
                    { new Guid("1c8fd4bd-0d36-9d5a-50eb-b21429d20ffe"), 55, new Guid("6bf84cec-7d37-7772-db15-54951c7b8b5f"), new Guid("6b614d2f-5210-4165-1861-37dc095ddf14") },
                    { new Guid("04379b6b-6a0c-2892-d4b1-207e9ab6c37d"), 55, new Guid("1c644315-1e5b-46c1-cd53-c75e7d46f2bc"), new Guid("cc981adb-bedb-0273-335d-6f9af93d14d3") },
                    { new Guid("4579be77-2e22-bfe4-c327-8bce2175ebb2"), 55, new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598") },
                    { new Guid("4e9f6d20-27be-efe9-c156-835a0e4e15ac"), 55, new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), new Guid("13793282-54c6-a960-73fe-74712bac9cd7") },
                    { new Guid("a623a55c-3dd5-4c1f-7f11-bb6ca667b836"), 55, new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), new Guid("1f8a4088-c807-d1da-71e4-62af9eed2a86") },
                    { new Guid("1af762e8-90a8-2c01-4a41-2835ee468117"), 55, new Guid("f71328f2-cb20-c5d7-041f-34ee9d835473"), new Guid("cc981adb-bedb-0273-335d-6f9af93d14d3") },
                    { new Guid("0de819ca-d82a-87f6-1df1-28da1954769d"), 55, new Guid("39692b63-4874-aae8-4bd7-4c9ee31c61b3"), new Guid("3b718030-4722-6868-a6f4-11a170b5b4c2") },
                    { new Guid("3bd7d25a-be7f-80d0-3bec-1795cc75f20c"), 55, new Guid("1e42216e-323c-b55c-f08f-5ddea6fcfe8a"), new Guid("6b614d2f-5210-4165-1861-37dc095ddf14") },
                    { new Guid("64db8e8f-26f4-2a11-36e1-4b86ae78416b"), 55, new Guid("109571f6-d42a-79e4-b7d3-aed716fb7ae2"), new Guid("7bc8922f-cd77-ec58-7807-c3b9d048d473") },
                    { new Guid("01bd0096-a253-d246-e7a9-4889f1bc529d"), 55, new Guid("8b847645-fafd-0e79-daf8-b34e6bcf2b8a"), new Guid("845097ca-477a-c4a3-703f-98acf1d2314a") },
                    { new Guid("7b154df2-5d18-c7bd-386f-6c2cefed9e03"), 55, new Guid("8b847645-fafd-0e79-daf8-b34e6bcf2b8a"), new Guid("39a0519e-a73f-06d1-9672-6b967a564156") },
                    { new Guid("5659a1a6-b334-2387-dd84-2e2eaea9eed0"), 55, new Guid("d961269f-e5d1-1541-26ed-f97767e65a6c"), new Guid("ed5c88d6-cf0b-e7a6-b1da-6aaf30b22431") },
                    { new Guid("c216df5c-50d4-6ae0-1679-79b5b02aa593"), 55, new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"), new Guid("604863e4-0320-0b09-817c-07d7f1b8df30") },
                    { new Guid("9a9ef423-04f9-02c4-75e8-6ab60d5b9411"), 55, new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"), new Guid("c742eab8-7988-a282-91de-90534583c6ea") },
                    { new Guid("75e6655f-0438-1983-781f-e478a55348a7"), 55, new Guid("c29c2675-e2d4-49cb-9ea9-8eadbc4836cd"), new Guid("d4e80b03-af95-918b-2308-fae38ce2c309") },
                    { new Guid("8701add4-505c-9919-7618-2e2fc0a7f5fb"), 55, new Guid("329a58e6-03ae-1f52-cf69-6f0bc15af361"), new Guid("604863e4-0320-0b09-817c-07d7f1b8df30") },
                    { new Guid("f55c3dcc-ad71-e092-4232-34fa5b32e24b"), 55, new Guid("161d9192-c183-9e3c-bd02-4935c8b84557"), new Guid("02d67cc5-e4a4-221a-d105-59123387a71d") },
                    { new Guid("800421ec-e40a-9dc9-5d2a-d8d5f0e19577"), 55, new Guid("2ff2b15a-2163-a874-8980-e3b467a12a14"), new Guid("bf50bdb0-921b-0544-4ea7-424f95f450fc") },
                    { new Guid("3b4d37d3-351d-e266-05e7-8667fbddcd36"), 55, new Guid("2ff2b15a-2163-a874-8980-e3b467a12a14"), new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598") },
                    { new Guid("2a5def9e-80bb-94c5-3a0d-90e3076e38f0"), 55, new Guid("43856184-28f8-18f8-ce8f-30a4c88688e4"), new Guid("69261de2-46c8-a164-6032-b0047c1aa7c7") },
                    { new Guid("8a58b8cf-3719-0edc-126b-e63fa1467158"), 55, new Guid("43856184-28f8-18f8-ce8f-30a4c88688e4"), new Guid("13793282-54c6-a960-73fe-74712bac9cd7") },
                    { new Guid("47ed86d7-99be-2d57-bc27-dd0192295684"), 55, new Guid("39692b63-4874-aae8-4bd7-4c9ee31c61b3"), new Guid("c742eab8-7988-a282-91de-90534583c6ea") },
                    { new Guid("e3f14653-6a68-3761-a001-72f6fb664fbb"), 55, new Guid("4feda6f9-d3fc-5c87-7c3a-39b6ab33f4d8"), new Guid("b4190bc7-f3f1-df1b-4d1d-d21f11de6598") },
                    { new Guid("c70de7c4-9bb6-f439-52f8-0a911c2a5884"), 55, new Guid("a45dbb54-3345-1d1a-a0dc-6524630df8c6"), new Guid("94b44eeb-7b80-be4c-507f-ebcfc4c0a9e9") },
                    { new Guid("82151266-f23f-9b0c-4656-5e7c8c53c114"), 55, new Guid("7d680ada-8fdb-c6f1-d85f-9aa4bd8fc81b"), new Guid("6b614d2f-5210-4165-1861-37dc095ddf14") },
                    { new Guid("f5a44759-d677-4998-987b-b8d39f75e7fc"), 55, new Guid("df2d60b1-6e5a-e140-735c-92b88fc72a7c"), new Guid("6fa27318-76f8-bc4f-e774-5e0a0b72d8f1") },
                    { new Guid("c5d1b821-d605-3534-ac50-c9dc0410bfc9"), 55, new Guid("df2d60b1-6e5a-e140-735c-92b88fc72a7c"), new Guid("5e01695b-cfe4-8bdc-8007-d57c539f9494") },
                    { new Guid("2fddf619-b341-7243-2cd7-ed95836cbc9d"), 55, new Guid("5919fa5c-f41a-e47a-2bd2-355a426c0d49"), new Guid("3b718030-4722-6868-a6f4-11a170b5b4c2") },
                    { new Guid("5a7343f1-db54-6580-2425-2e7dba02c8bb"), 55, new Guid("5919fa5c-f41a-e47a-2bd2-355a426c0d49"), new Guid("1823ff21-532b-5cf5-44f6-75ad79ea6111") },
                    { new Guid("9cd98795-6c5c-0169-98fe-3b68f6b91e92"), 55, new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"), new Guid("39a0519e-a73f-06d1-9672-6b967a564156") },
                    { new Guid("0e1fabb1-095a-77fc-0c34-70220809648a"), 55, new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"), new Guid("f79c4f74-43f4-6a14-6c5d-ef0ceac436be") },
                    { new Guid("d6e43927-1eb1-887b-55f7-bdfa945b53f8"), 55, new Guid("c884c5a2-d93c-7297-fbd6-a0566a640aec"), new Guid("dbba385f-3b3f-dd9e-86c5-ea073272417d") },
                    { new Guid("7c73741a-aaa8-a414-2d19-e00d22fa3d86"), 55, new Guid("ee544476-56a0-2e56-5df0-8c40fad4d07e"), new Guid("48fe92b0-ef9b-5039-c05d-fd3e65d7af4b") },
                    { new Guid("52f50405-2e8b-1514-2236-3d861260a09f"), 55, new Guid("c79beded-a660-c08a-7f24-8228409e7cdb"), new Guid("07016d05-80a3-f718-2935-f6eea5cdf14e") },
                    { new Guid("12ce0b43-a71d-7039-fae1-8333c93aedc6"), 55, new Guid("efaa2963-c4a9-6831-acfb-445bd3c1c265"), new Guid("8330229c-8d98-fdc9-c7aa-8c09b9e49d12") }
                });
        }
    }
}
