﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Supplies.Data.Migrations
{
    public partial class addtotalintablestatistical : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TotalDate",
                table: "Statisticals",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalMonth",
                table: "Statisticals",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalYear",
                table: "Statisticals",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalDate",
                table: "Statisticals");

            migrationBuilder.DropColumn(
                name: "TotalMonth",
                table: "Statisticals");

            migrationBuilder.DropColumn(
                name: "TotalYear",
                table: "Statisticals");
        }
    }
}
