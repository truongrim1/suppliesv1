﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Supplies.Data.Entity
{
    public class OrderDetail
    {
        [Key]
        public Guid Id { get; set; }

        public int Amount { get; set; }

        [ForeignKey("OrderId")]
        
        public Guid OrderId { get; set; }
        
        public Order Order { get; set; }

        [ForeignKey("ProductId")]
      
        public Guid ProductId { get; set; }

        public Product Product { get; set; }




    }
}
