﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Data.Entity
{
   public class Staff
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(50)]
        public string  Name { get; set; }

        [MaxLength(50)]
        public string NamePart { get; set; }

        public string Adress { get; set; }
        [Phone,MaxLength(10),MinLength(10)]
        public string Phone { get; set; }

        public DateTime CreateDate { get; set; }

        [ForeignKey("PartId")]
        public virtual Part Part { get; set; }

        public Guid PartId { get; set; }

    }
}
