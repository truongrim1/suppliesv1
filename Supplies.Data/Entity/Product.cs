﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Supplies.Data.Entity
{
   public class Product
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(50)]
        public string  Name { get; set; }

        [Column(TypeName = "decimal(18,4)")]
        public decimal Price { get; set; }

        public int ViewCount { get; set; }

        public DateTime CreateOnDate { get; set; }

      


    }
}
