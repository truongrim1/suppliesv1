﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Supplies.Data.Entity
{
   public class Order
    {   
        [Key]
        public Guid  Id { get; set; }
        
        [MaxLength(50)]
        public string  NamePart { get; set; }

        [MaxLength(50)]
        public string  NameStaff { get; set; }

        [ForeignKey("StaffId")]
        public virtual Staff Staff { get; set; }

        public Guid StaffId { get; set; }

        public ICollection<OrderDetail> Products { get; set; }
        public DateTime CreateDate { get; set; }


    }
}
