﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Supplies.Data.Entity
{   

    public class Part
    {
        [Key]
        public Guid Id { get; set; }

        
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string NameLeader { get; set; }

        public DateTime CreateDate { get; set; }

        public ICollection<Staff> Staffs { get; set; } = new List<Staff>();
    }
}
