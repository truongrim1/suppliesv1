﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Supplies.Data.Entity
{
   public class Statistical
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime AccessTime { get; set; }

        public string IpAdress { get; set; }

        public int ViewCount { get; set; }

        public int TotalDate { get; set; }
        public int TotalMonth { get; set; }
        public int TotalYear { get; set; }


    }
}
